FROM openjdk:8-jdk-alpine
EXPOSE 8090
ADD target/newapp.war newapp.war 
ENTRYPOINT ["java","-jar","/newapp.war"]