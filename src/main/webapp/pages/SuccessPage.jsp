<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
    <%@ page isELIgnored = "false" %>

<!DOCTYPE html>

<head>

<meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <script src="https://kit.fontawesome.com/e38070cbc2.js" crossorigin="anonymous"></script>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<title>Success</title>
<!-- Bootstrap CSS -->
<link rel="stylesheet" type="text/css" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css" />
<link rel="stylesheet" type="text/css" href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" />
<style type="text/css">

    body
    {
        background:#f2f2f2;
    }

    .payment
	{
		border:1px solid #f2f2f2;
		height:280px;
        border-radius:20px;
        background:#fff;
	}
   .payment_header
   {
	   background:rgb(39, 161, 66);
	   padding:20px;
       border-radius:20px 20px 0px 0px;
	   
   }
   
   .check
   {
	   margin:0px auto;
	   width:50px;
	   height:50px;
	   border-radius:100%;
	   background:#fff;
	   text-align:center;
   }
   
   .check i
   {
	   vertical-align:middle;
	   line-height:50px;
	   font-size:30px;
   }

    .content 
    {
        text-align:center;
    }

    .content  h1
    {
        font-size:25px;
        padding-top:25px;
    }

    .content a
    {
        width:200px;
        height:35px;
        color:rgb(255, 255, 255);
        border-radius:30px;
        padding:5px 10px;
        background:rgb(4, 110, 160);
        transition:all ease-in-out 0.3s;
    }

    .content a:hover
    {
        text-decoration:none;
        background:#000;
    }
   
</style>

</head>
<body>

<% String s=(String)request.getAttribute("polystatus"); %>


  <div class="container">
    <div class="row">
       <div class="col-md-6 mx-auto mt-5">
          <div class="payment">
             <div class="payment_header">
                <div class="check"><i class="fa fa-check" aria-hidden="true"></i></div>
             </div>
             <div class="content">
            
                <h1> Success !</h1>
                <% if(s!=null)
                	{
                	
                	if(s.equalsIgnoreCase("Pending")){
                	
                	%>
                <p>Your request is <b >Pending</b> and is sent to underwriter for review. </p>
                <%}
                	
                	else{
                		%>
                	<p>Your request is <b>Approved</b>, policy bought successfully. </p>
                <%}} %>
                <a href="/user/homered">Go to Home</a>
             </div>
             
          </div>
       </div>
    </div>
 </div>
</body>
</html>
