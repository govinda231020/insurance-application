<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>

<!doctype html>
<html lang="en">

<head>
<link rel="stylesheet"
	href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css"
	integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm"
	crossorigin="anonymous">

<!-- Required meta tags -->
<meta charset="utf-8">
<meta name="viewport"
	content="width=device-width, initial-scale=1, shrink-to-fit=no">
<script src="https://kit.fontawesome.com/e38070cbc2.js"
	crossorigin="anonymous"></script>
	
	
	
	<script src="https://cdnjs.cloudflare.com/ajax/libs/crypto-js/3.1.2/rollups/aes.js"></script>
<script src="http://crypto-js.googlecode.com/svn/tags/3.1.2/build/rollups/md5.js"></script>
<!-- Bootstrap CSS -->
<link rel="stylesheet"
	href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css"
	integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm"
	crossorigin="anonymous">
<title>Life Insurance</title>


<style>
    .form {
      padding: 5px 30px;
    }
    
    .form-control {
      margin-bottom: 5px;
      padding-bottom: 17px;
      position: relative;
      border-style: none;
    }
    
    .form-control label {
      display: inline-block;
      margin-bottom: 5px;
    }
    
    .form-control input {
      border: 2px solid #f0f0f0;
      border-radius: 4px;
      display: block;
      font-family: inherit;
      font-size: 14px;
      padding: 10px;
      width: 100%;
    }
    
    .form-control input:focus {
      outline: 0;
      border-color: #777;
    }
    
    .form-control.success input {
      border-color: #2ecc71;
    }
    
    .form-control.error input {
      border-color: #e74c3c;
    }
    
    .form-control i {
      visibility: hidden;
      position: absolute;
      top: 50px;
      right: 20px;
    }
    
    .form-control.success i.fa-check-circle {
      color: #2ecc71;
      visibility: visible;
    }
    
    .form-control.error i.fa-exclamation-circle {
      color: #e74c3c;
      visibility: visible;
    }
    
    .form-control small {
      color: #e74c3c;
      position: absolute;
      bottom: 0;
      left: 0;
      visibility: hidden;
    }
    
    .form-control.error small {
      visibility: visible;
    }
    
    .form button {
      background-color: #44ad52;
      border: 2px solid #55ad44;
      border-radius: 4px;
      color: #fff;
      display: block;
      font-family: inherit;
      font-size: 16px;
      padding: 10px;
      margin-top: 20px;
      width: 100%;
    }
    
    /* for dropdown in navbar */
    .dropdown:hover .dropdown-menu {
      display: block;
    }
    
    
      .form-control span {

          color: red;

          font-weight: bold;

      }

      #star
      {

          color: red;

          font-weight: bold;
      }
    </style>
    
    <style>
#navname{
color:white;
float:Right;
padding-right: 20px;
font-size:20px;
}
</style>
</head>

<body>
	<nav class="navbar navbar-expand-lg navbar-dark bg-dark">
		<a class="navbar-brand" href="#">Insurance App</a>
		<button class="navbar-toggler" type="button" data-toggle="collapse"
			data-target="#navbarSupportedContent"
			aria-controls="navbarSupportedContent" aria-expanded="false"
			aria-label="Toggle navigation">
			<span class="navbar-toggler-icon"></span>
		</button>

		<div class="collapse navbar-collapse" id="navbarSupportedContent">
			<ul class="navbar-nav mr-auto">
				<li class="nav-item active"><a class="nav-link" href="/user/homered">Home<span
						class="sr-only">(current)</span></a></li>
				<div class="dropdown">
					<button class="btn btn-dark dropdown-toggle" type="button"
						id="dropdownMenuButton" data-toggle="dropdown"
						aria-haspopup="true" aria-expanded="false">Insurance
						Plans</button>
					<div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
						<a class="dropdown-item" href="/user/lifeplans">Life Insurance</a> 
						<a
							class="dropdown-item" href="/user/dentalplans">Dental Insurance</a> <a
							class="dropdown-item" href="/user/visionplans">Vision Insurance</a>
					</div>
				</div>
			</ul>
			<div id="navname">Hi ${yourname}</div>

		</div>

		<div class="mx-2">
			<!-- <button class="btn btn-success">Logout</button> -->
			<div class="dropdown">
				<button class="btn btn-success dropdown-toggle" type="button"
					data-toggle="dropdown">
					Actions <span class="caret"></span>
				</button>
				<ul class="dropdown-menu">
					<div class="dropbt">
						<li><a href="/user/myprofile" style="color: black"><b>My Profile</b></a></li>
						<li><a href="/user/myPolicies" style="color: black"><b>My Policies</b></a></li>
						<li><a href="/user/myfamily" style="color: black"><b>My Family</b></a></li>
						<li><a href="/user/contact" style="color: black"><b>Contact Us</b></a></li>
						<li><a href="/logout" style="color: black"><b>Logout</b></a></li>
					</div>
				</ul>
			</div>

			<!-- <button class="btn btn-danger" data-toggle="modal" data-target="#signupModal">SignUp</button> -->
		</div>

	</nav>


<br>
	<div class="container">
		<div class="row">
			<div
				class="col-lg-6 col-md-6 col-sm-6 container justify-content-center card">
				<h2 class="text-center">Add Details Member</h2>
				<div class="card-body">

					<form action="/user/membersAfterAdd" method="POST" id="form" class="form" onsubmit="return checkInputs()">

						<div class="form-control">
                        <label for="relation">Relation</label><span> *</span><br> <select
                            class="form-select form-select-lg mb-3" aria-label=".form-select-lg example" id="relation"
                            name="depRelation" onchange="checkrelation()" >
                            <option selected disabled value="">Select Relation</option>
                            <option value="son">Son</option>
                            <option value="daughter">Daughter</option>
                            <option value="father">Father</option>
                            <option value="mother">Mother</option>
                            <option value="husband">Husband</option>
                            <option value="wife">Wife</option>

                            <option value="brother">Brother</option>
                            <option value="sister">Sister</option>
                            
                        </select>
                        <div id="errm" style="color:#e74c3c;font-size:0.8em"></div>
                    </div>
                    <label for="username">Gender</label> <span id="star"> *</span><br>
                    <div class="form-check form-check-inline">
                        <input class="form-check-input" type="radio" name="gender" id="inlineRadio1" value="male"
                            > <label class="form-check-label" for="inlineRadio1">Male</label>
                    </div>
                    <div class="form-check form-check-inline">
                        <input class="form-check-input" type="radio" name="gender" id="inlineRadio2" value="female">
                        <label class="form-check-label" for="inlineRadio2">Female</label>
                    </div>
<!--                     <div class="form-check form-check-inline"> -->
<!--                         <input class="form-check-input" type="radio" name="gender" id="inlineRadio3" value="other"> -->
<!--                         <label class="form-check-label" for="inlineRadio2">Other</label> -->
<!--                     </div> -->
                    <div id="radioerr" style="color:#e74c3c;font-size:0.8em;"></div>
                   <br>



						<div class="form-control">

							<label for="username">FullName</label> <span> *</span><input type="text"  onchange="checkComp()"
								placeholder="Enter Fullname" id="compname" name="depFullName" />
							<i class="fas fa-check-circle"></i> <i
								class="fas fa-exclamation-circle"></i><small>Error
		message</small>
						</div>
						<div class="form-control">
							<label for="username">Year Of Birth</label><span> *</span> <input type="date" onchange="checkYear()"
								placeholder="Enter year of birth" id="minage" name="depdob" />
							<small>Error
		message</small>
						</div>

						                        <h5>Medical History</h5>
						<label for="heart">Heart Ailments</label> <span id="star">
							*</span><br>
						<div class="form-check form-check-inline">
							<input class="form-check-input" type="radio" name="heartAilments"
								id="inlineRadio1" value="no" checked> <label
								class="form-check-label" for="inlineRadio1">No</label>
						</div>
						<div class="form-check form-check-inline">
							<input class="form-check-input" type="radio" name="heartAilments"
								id="inlineRadio2" value="low"> <label
								class="form-check-label" for="inlineRadio2">Low</label>
						</div>
						<div class="form-check form-check-inline">
							<input class="form-check-input" type="radio" name="heartAilments"
								id="inlineRadio3" value="moderate"> <label
								class="form-check-label" for="inlineRadio3">Moderate</label>
						</div>
						<div class="form-check form-check-inline">
							<input class="form-check-input" type="radio" name="heartAilments"
								id="inlineRadio4" value="severe"> <label
								class="form-check-label" for="inlineRadio4">Severe</label>
						</div>
						<div id="radioerr"></div>
						<br> <label for="diabetes">Diabetes</label> <span id="star">
							*</span><br>
						<div class="form-check form-check-inline">
							<input class="form-check-input" type="radio" name="diabetes"
								id="inlineRadio5" value="no" checked> <label
								class="form-check-label" for="inlineRadio5">No</label>
						</div>
						<div class="form-check form-check-inline">
							<input class="form-check-input" type="radio" name="diabetes"
								id="inlineRadio6" value="low"> <label
								class="form-check-label" for="inlineRadio6">Low</label>
						</div>
						<div class="form-check form-check-inline"> 
							<input class="form-check-input" type="radio" name="diabetes"
								id="inlineRadio7" value="moderate"> <label
								class="form-check-label" for="inlineRadio7">Moderate</label>
						</div>
						<div class="form-check form-check-inline">
							<input class="form-check-input" type="radio" name="diabetes"
								id="inlineRadio8" value="severe"> <label
								class="form-check-label" for="inlineRadio8">Severe</label>
						</div>
						

						<div id="radioerr"></div>
						<br> <label for="tooth">Tooth Decay</label> <span id="star">
							*</span><br>
						<div class="form-check form-check-inline">
							<input class="form-check-input" type="radio" name="toothDecay"
								id="inlineRadio11" value="no" checked> <label
								class="form-check-label" for="inlineRadio11">No</label>
						</div>
						<div class="form-check form-check-inline">
							<input class="form-check-input" type="radio" name="toothDecay"
								id="inlineRadio12" value="yes"> <label
								class="form-check-label" for="inlineRadio12">Yes</label>
						</div>

						<div id="radioerr"></div>
						<br> <label for="covid">Covid</label> <span id="star">
							*</span><br>
						<div class="form-check form-check-inline">
							<input class="form-check-input" type="radio" name="covid"
								id="inlineRadio13" value="no" checked> <label
								class="form-check-label" for="inlineRadio13">No</label>
						</div>
						<div class="form-check form-check-inline">
							<input class="form-check-input" type="radio" name="covid"
								id="inlineRadio14" value="yes"> <label
								class="form-check-label" for="inlineRadio14">Yes</label>
						</div>

						<div id="radioerr"></div>
						<br> <label for="cancer">Cancer</label> <span id="star">
							*</span><br>
						<div class="form-check form-check-inline">
							<input class="form-check-input" type="radio" name="cancer"
								id="inlineRadio15" value="no" checked> <label
								class="form-check-label" for="inlineRadio15">No</label>
						</div>
						<div class="form-check form-check-inline">
							<input class="form-check-input" type="radio" name="cancer"
								id="inlineRadio16" value="low"> <label
								class="form-check-label" for="inlineRadio16">Low</label>
						</div>
						<div class="form-check form-check-inline">
							<input class="form-check-input" type="radio" name="cancer"
								id="inlineRadio17" value="moderate"> <label
								class="form-check-label" for="inlineRadio17">Moderate</label>
						</div><div class="form-check form-check-inline">
							<input class="form-check-input" type="radio" name="cancer"
								id="inlineRadio18" value="severe"> <label
								class="form-check-label" for="inlineRadio18">Severe</label>
						</div>
						<div id="radioerr"></div>
						<br> <label for="cataract">Cataract</label> <span id="star">
							*</span><br>
						<div class="form-check form-check-inline">
							<input class="form-check-input" type="radio" name="cataract"
								id="inlineRadio19" value="no" checked> <label
								class="form-check-label" for="inlineRadio19">No</label>
						</div>
						<div class="form-check form-check-inline">
							<input class="form-check-input" type="radio" name="cataract"
								id="inlineRadio20" value="yes"> <label
								class="form-check-label" for="inlineRadio20">Yes</label>
						</div>
						
						<br>


						<button type="submit" class="btn btn-primary">Add Member</button>
					</form>

				</div>
			</div>
		</div>
	</div>
	<br>


<script>
		const form = document.getElementById('form');
		

		//for particular field check on TAB
		
		
		 function checkrelation() {
            var option = document.getElementById("relation").value;


            if(option == "son")
                   {
                        
                    //document.getElementById("errm").innerHTML = "Hiiiiiiiiiiii Hiiiiiiii";
                        document.getElementById("inlineRadio1").checked = true;
                        document.getElementById("inlineRadio1").disabled = false;
                        document.getElementById("inlineRadio2").disabled = true;
                        document.getElementById("inlineRadio3").disabled = true;
                   }
                        

                if(option == "daughter")
                  {
                        //document.getElementById("enroll1").style.display="none";
                        
                        document.getElementById("inlineRadio2").checked = true;
                        document.getElementById("inlineRadio1").disabled = true;
                        document.getElementById("inlineRadio2").disabled = false;
                        document.getElementById("inlineRadio3").disabled = true;
                    }
                if(option == "father")
                  {
                        document.getElementById("inlineRadio1").checked = true;
                        document.getElementById("inlineRadio1").disabled = false;
                        document.getElementById("inlineRadio2").disabled = true;
                        document.getElementById("inlineRadio3").disabled = true;
                  }

                  if(option == "mother")
                  {
                        document.getElementById("inlineRadio2").checked = true;
                        document.getElementById("inlineRadio1").disabled = true;
                        document.getElementById("inlineRadio2").disabled = false;
                        document.getElementById("inlineRadio3").disabled = true;
                  }


                  if(option == "husband")
                  {
                        document.getElementById("inlineRadio1").checked = true;
                        document.getElementById("inlineRadio1").disabled = false;
                        document.getElementById("inlineRadio2").disabled = true;
                        document.getElementById("inlineRadio3").disabled = true;
                  }


                  if(option == "wife")
                  {
                        document.getElementById("inlineRadio2").checked = true;
                        document.getElementById("inlineRadio1").disabled = true;
                        document.getElementById("inlineRadio2").disabled = false;
                        document.getElementById("inlineRadio3").disabled = true;
                  }


                  if(option == "brother")
                  {
                        document.getElementById("inlineRadio1").checked = true;
                        document.getElementById("inlineRadio1").disabled = false;
                        document.getElementById("inlineRadio2").disabled = true;
                        document.getElementById("inlineRadio3").disabled = true;
                  }

                  if(option == "sister")
                  {
                        document.getElementById("inlineRadio2").checked = true;
                        document.getElementById("inlineRadio1").disabled = true;
                        document.getElementById("inlineRadio2").disabled = false;
                        document.getElementById("inlineRadio3").disabled = true;
                  }

                

                 
                  
                }
		
		


//Company check
        const compname = document.getElementById('compname');
        function checkComp()
        {
            const compnameValue = compname.value.trim();
            if (compnameValue === '') {
				setErrorFor(compname, 'Name cannot be blank');

			} else if (compnameValue.length < 3) {
				setErrorFor(compname, 'Not valid');
			} else {
				setSuccessFor(compname);

			}
        }
        
      //check age
      
//         const minage = document.getElementById('minage');
//         function checkYear() {
//         	const minagevalue = minage.value.trim();

// 			if (minagevalue === '') {
// 				setErrorFor(minage, 'dob cannot be blank');
				

// 			} else if (minagevalue<1920 || minagevalue>(new Date().getFullYear()) - 5) {
// 				setErrorFor(minage, 'Enter valid Year');
				

// 			} else {
// 				setSuccessFor(minage);

// 			}
// 		}
        
        
        function checkYear() {
        	const minagevalue = minage.value.trim();

			var parts = minagevalue.split("/");
			var day = parseInt(parts[2], 10);
			var month = parseInt(parts[1], 10);
			var year = parseInt(parts[0], 10);

			var monthLength = [31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31];
			// Adjust for leap years
			if (year % 400 == 0 || (year % 100 != 0 && year % 4 == 0))
				monthLength[1] = 29;
			// Check the ranges of month and year
			// if (yobvalue === '') {
			// 			setErrorFor(yob, 'year cannot be blank');

			// 		}
			if (minagevalue === '') {
				setErrorFor(minage, 'Year cannot be blank');}
			
			else if (year > new Date().getFullYear() - 80 && year < new Date().getFullYear() - 5) {
				
				setSuccessFor(minage);
			}
			else {
				setErrorFor(minage, 'Invalid date of birth');

			}

		}
        
        
        
       
        
        
        
        function checkInputs() 
		{
			// trim to remove the whitespaces

			var flag = 0;
			
			
			var gen = document.getElementsByName("gender").value;
            if(document.getElementById("inlineRadio1").checked == false && document.getElementById("inlineRadio1").checked == false && document.getElementById("inlineRadio1").checked == false)
            {
                document.getElementById("radioerr").innerHTML = "Please select gender!";
                flag=0;
            }
            else{
                document.getElementById("radioerr").innerHTML = "";
                flag=1;
            }



    var option = document.getElementById("relation").value;

    if(option === '')
            {
                document.getElementById("errm").innerHTML = "Please select reletion!";
                flag=0;
            }
            else{
                document.getElementById("errm").innerHTML = "";
                
            }





         
			
			
			
	
// //Company check

        
             const compnameValue = compname.value.trim();
            if (compnameValue === '') {
				setErrorFor(compname, 'Name cannot be blank');
                flag = 0;
			} else if (compnameValue.length < 3) {
				setErrorFor(compname, 'name not valid');
                flag = 0;
			} else {
				setSuccessFor(compname);
                //flag=1;
			}
            
            
            const minagevalue = minage.value.trim();

			var parts = minagevalue.split("/");
			var day = parseInt(parts[2], 10);
			var month = parseInt(parts[1], 10);
			var year = parseInt(parts[0], 10);

			var monthLength = [31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31];
			// Adjust for leap years
			if (year % 400 == 0 || (year % 100 != 0 && year % 4 == 0))
				monthLength[1] = 29;
			// Check the ranges of month and year
			// if (yobvalue === '') {
			// 			setErrorFor(yob, 'year cannot be blank');

			// 		}
			if (minagevalue === '') {
				setErrorFor(minage, 'Year cannot be blank');
				flag=0;}
			
			else  if (year > new Date().getFullYear() - 80 && year < new Date().getFullYear() - 5) {
				
				setSuccessFor(minage);
			}
			else {
				setErrorFor(minage, 'Invalid date of birth');
					flag=0;
			}

			return Boolean(flag);

		}

		function setErrorFor(input, message) {
			const formControl = input.parentElement;
			const small = formControl.querySelector('small');
			formControl.className = 'form-control error';
			small.innerText = message;
		}

		function setSuccessFor(input) {
			const formControl = input.parentElement;
			formControl.className = 'form-control success';
		}

</script>


	<!-- Optional JavaScript -->
	<!-- jQuery first, then Popper.js, then Bootstrap JS -->
	<script src="https://code.jquery.com/jquery-3.2.1.slim.min.js"
		integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN"
		crossorigin="anonymous"></script>
	<script
		src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js"
		integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q"
		crossorigin="anonymous"></script>
	<script
		src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js"
		integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl"
		crossorigin="anonymous"></script>

</body>

</html>