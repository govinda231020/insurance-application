<%@page import="com.impetus.insuranceapp.model.InsurancePlan"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!doctype html>
<html lang="en">

<head>
<link rel="stylesheet"
	href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css"
	integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm"
	crossorigin="anonymous">

<!-- Required meta tags -->
<meta charset="utf-8">
<meta name="viewport"
	content="width=device-width, initial-scale=1, shrink-to-fit=no">
<script src="https://kit.fontawesome.com/e38070cbc2.js"
	crossorigin="anonymous"></script>
<!-- Bootstrap CSS -->
<link rel="stylesheet"
	href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css"
	integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm"
	crossorigin="anonymous">
<style>
.form {
	padding: 30px 40px;
}

.form-control {
	margin-bottom: 10px;
	padding-bottom: 20px;
	position: relative;
	border-style: none;
}

.form-control label {
	display: inline-block;
	margin-bottom: 5px;
}

.form-control input {
	border: 2px solid #f0f0f0;
	border-radius: 4px;
	display: block;
	font-family: inherit;
	font-size: 14px;
	padding: 10px;
	width: 100%;
}

.form-control input:focus {
	outline: 0;
	border-color: #777;
}

.form-control.success input {
	border-color: #2ecc71;
}

.form-control.error input {
	border-color: #e74c3c;
}

.form-control i {
	visibility: hidden;
	position: absolute;
	top: 50px;
	right: 20px;
}

.form-control.success i.fa-check-circle {
	color: #2ecc71;
	visibility: visible;
}

.form-control.error i.fa-exclamation-circle {
	color: #e74c3c;
	visibility: visible;
}

.form-control small {
	color: #e74c3c;
	position: absolute;
	bottom: 0;
	left: 0;
	visibility: hidden;
}

.form-control.error small {
	visibility: visible;
}

.form button {
	background-color: #8e44ad;
	border: 2px solid #8e44ad;
	border-radius: 4px;
	color: #fff;
	display: block;
	font-family: inherit;
	font-size: 16px;
	padding: 10px;
	margin-top: 20px;
	width: 100%;
}

/* for dropdown in navbar */
.dropdown:hover .dropdown-menu {
	display: block;
}

.form-control span {
	color: red;
	font-weight: bold;
}

#star {
	color: red;
	font-weight: bold;
}
</style>
<style>
/* for dropdown in navbar */
.dropdown:hover .dropdown-menu {
	display: block;
	margin-top: 0;
}

#navname {
	color: white;
	float: Right;
	padding-right: 20px;
	font-size: 20px;
}
</style>
<title>Product Details Page</title>
</head>

<body>

	<nav class="navbar navbar-expand-lg navbar-dark bg-dark">
		<a class="navbar-brand" href="#">Insurance App</a>
		<button class="navbar-toggler" type="button" data-toggle="collapse"
			data-target="#navbarSupportedContent"
			aria-controls="navbarSupportedContent" aria-expanded="false"
			aria-label="Toggle navigation">
			<span class="navbar-toggler-icon"></span>
		</button>

		<div class="collapse navbar-collapse" id="navbarSupportedContent">
			<ul class="navbar-nav mr-auto">
				<li class="nav-item active"><a class="nav-link"
					href="/user/homered">Home<span class="sr-only">(current)</span></a></li>
				<div class="dropdown">
					<button class="btn btn-dark dropdown-toggle" type="button"
						id="dropdownMenuButton" data-toggle="dropdown"
						aria-haspopup="true" aria-expanded="false">Insurance
						Plans</button>
					<div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
						<a class="dropdown-item" href="/user/lifeplans">Life Insurance</a>
						<a class="dropdown-item" href="/user/dentalplans">Dental
							Insurance</a> <a class="dropdown-item" href="/user/visionplans">Vision
							Insurance</a>
					</div>
				</div>
				
			</ul>
			
			<div id="navname">Hi ${yourname}</div>

		</div>

		<div class="mx-2">
			<!-- <button class="btn btn-success">Logout</button> -->
			<div class="dropdown">
				<button class="btn btn-success dropdown-toggle" type="button"
					data-toggle="dropdown">
					Actions <span class="caret"></span>
				</button>
				<ul class="dropdown-menu">
					<div class="dropbt">
						<li><a href="/user/myprofile" style="color: black"><b>My
									Profile</b></a></li>
						<li><a href="/user/myPolicies" style="color: black"><b>My
									Policies</b></a></li>
						<li><a href="/user/myfamily" style="color: black"><b>My
									Family</b></a></li>
						<li><a href="/user/contact" style="color: black"><b>Contact
									Us</b></a></li>
						<li><a href="/logout" style="color: black"><b>Logout</b></a></li>
					</div>
				</ul>
			</div>

			<!-- <button class="btn btn-danger" data-toggle="modal" data-target="#signupModal">SignUp</button> -->
		</div>

	</nav>
	<br>


	<div class="container">
		<div class="card">
			<div class="card-body">
				<h5 class="card-title">Your selected plan detail</h5>
				<div class="row">
					<!-- <div class="col-lg-5 col-md-5 col-sm-6">
                        <div class="white-box text-center"><img src="https://via.placeholder.com/430x600/00CED1/000000" class="img-responsive"></div>
                    </div> -->
					<div class="col-lg-7 col-md-7 col-sm-6">
						<!-- <h4 class="box-title mt-2">Product description</h4> -->
						<%
						InsurancePlan ins = (InsurancePlan) request.getAttribute("insurdata");
						%>
						<table class="table table-striped table-bordered">
							<thead class="table-dark">
								<tr>
									<th>Insurance Name</th>
									<th>Insurance Plan Detail</th>
									<th>Insurance Type</th>
									<th>Base Price</th>
									<th>Coverage Amount</th>
									<th>Minimum Age</th>
									<th>Maximum Age</th>
								</tr>
							</thead>
							<tbody>
								<%
								InsurancePlan plan = (InsurancePlan) request.getAttribute("ins");
								%>
								<tr>
									<td><%=plan.getInsuranceCompName()%></td>
									<td><%=plan.getInsPlanDetail()%></td>
									<td><%=plan.getInsuranceType()%></td>
									<td><%=plan.getBasePrice()%></td>
									<td><%=plan.getCoverageAmount()%></td>
									<td><%=plan.getMinAge()%></td>
									<td><%=plan.getCoverageAge()%></td>
								</tr>

							</tbody>
						</table>

						<p>Cover against Uncertainties. It is one of the most
							prominent and crucial benefits of insurance. ... Cash Flow
							Management. The uncertainty of paying for the losses incurred out
							of pocket has a significant impact on cash flow management. ...
							Investment Opportunities.</p>


						<%
						String mss = (String) request.getAttribute("notvalidmsg");
						%>

						<%
						if (mss != null) {
						%>
						<div id="updatem">
							<h6 style="color: red; text-align: center"><%=mss%></h6>
						</div>
						<%
						}
						%>



						<h5 class="box-title mt-3">Key Highlights</h5>
						<ul>
							<li>Insurance provides security against risk and uncertainty</li>
							<li>It enables the insured to concentrate on his work
								without fear of loss due to risk and uncertainty</li>
							<li>It inculcates regular savings habit, as in the case of
								life insurance</li>
							<li>The insurance policy can be mortgaged and funds raised
								in case of financial requirements</li>
							<li>Insurance policies, especially pension plans provide for
								income security during old age</li>
							<li>The insured gets tax benefits for the amount of premium
								paid</li>
							<li>Insurance of goods may be a mandatory requirement in
								certain contracts</li>

						</ul>
						<form action="/user/getpriceorgrp" method="post">
							<!-- if group kind of insurance-->
							<div class="form-check form-check-inline">

								<input class="form-check-input" type="radio"
									onclick="function1()" name="PolicyType" id="inlineRadio1"
									value="Individual" checked> <label
									class="form-check-label" for="inlineRadio1">Individual</label>

							</div>

							<div class="form-check form-check-inline">

								<input class="form-check-input" type="radio"
									onclick="function2()" name="PolicyType" id="inlineRadio2"
									value="Group"> <label class="form-check-label"
									for="inlineRadio2">Group</label>

							</div>
							<button type="submit" id="bu" name="insbtn"
								class="btn btn-primary">Enter Your Details</button>


							<br>

							<script>
								function function1() {

									document.getElementById("bu").innerHTML = "Enter Your Details";

								}

								function function2() {

									document.getElementById("bu").innerHTML = "Enter Group Details";

								}
							</script>

						</form>

					</div>
				</div>
			</div>
		</div>

	</div>




	<!-- Optional JavaScript -->
	<!-- jQuery first, then Popper.js, then Bootstrap JS -->
	<script src="https://code.jquery.com/jquery-3.2.1.slim.min.js"
		integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN"
		crossorigin="anonymous"></script>
	<script
		src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js"
		integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q"
		crossorigin="anonymous"></script>
	<script
		src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js"
		integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl"
		crossorigin="anonymous"></script>
</body>

</html>