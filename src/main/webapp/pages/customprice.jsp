<%@page import="java.time.LocalDate"%>
<%@page import="com.impetus.insuranceapp.model.BeneficiaryDetails"%>
<%@page import="com.impetus.insuranceapp.model.InsurancePlan"%>
<%@page import="com.impetus.insuranceapp.repository.DepRepo"%>
<%@page import="java.util.ArrayList"%>
<%@page import="com.impetus.insuranceapp.model.DepDetails"%>
<%@page import="com.impetus.insuranceapp.model.UserProfile"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!doctype html>
<html lang="en">

<head>
<link rel="stylesheet"
	href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css"
	integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm"
	crossorigin="anonymous">

<!-- Required meta tags -->
<meta charset="utf-8">
<meta name="viewport"
	content="width=device-width, initial-scale=1, shrink-to-fit=no">
<script src="https://kit.fontawesome.com/e38070cbc2.js"
	crossorigin="anonymous"></script>
<!-- Bootstrap CSS -->
<link rel="stylesheet"
	href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css"
	integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm"
	crossorigin="anonymous">

<title>Pricing Page</title>
<script type="text/javascript">
	function disableBack() {
		window.history.forward();
	}
	setTimeout("disableBack()", 0);
	window.onunload = function() {
		null
	};
</script>
<style>
/* for dropdown in navbar */
.dropdown:hover .dropdown-menu {
	display: block;
	margin-top: 0;
}

#navname {
	color: white;
	float: Right;
	padding-right: 20px;
	font-size: 20px;
}
</style>
</head>

<body>

	<nav class="navbar navbar-expand-lg navbar-dark bg-dark">
		<a class="navbar-brand" href="#">Insurance App</a>
		<button class="navbar-toggler" type="button" data-toggle="collapse"
			data-target="#navbarSupportedContent"
			aria-controls="navbarSupportedContent" aria-expanded="false"
			aria-label="Toggle navigation">
			<span class="navbar-toggler-icon"></span>
		</button>

		<div class="collapse navbar-collapse" id="navbarSupportedContent">
			<ul class="navbar-nav mr-auto">
				<li class="nav-item active"><a class="nav-link"
					href="/user/homered">Home<span class="sr-only">(current)</span></a></li>
				<div class="dropdown">
					<button class="btn btn-dark dropdown-toggle" type="button"
						id="dropdownMenuButton" data-toggle="dropdown"
						aria-haspopup="true" aria-expanded="false">Insurance
						Plans</button>
					<div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
						<a class="dropdown-item" href="/user/lifeplans">Life Insurance</a>
						<a class="dropdown-item" href="/user/dentalplans">Dental
							Insurance</a> <a class="dropdown-item" href="/user/visionplans">Vision
							Insurance</a>
					</div>
				</div>
			</ul>
			<div id="navname">Hi ${yourname}</div>

		</div>

		<div class="mx-2">
			<!-- <button class="btn btn-success">Logout</button> -->
			<div class="dropdown">
				<button class="btn btn-success dropdown-toggle" type="button"
					data-toggle="dropdown">
					Actions <span class="caret"></span>
				</button>
				<ul class="dropdown-menu">
					<div class="dropbt">
						<li><a href="/user/myprofile" style="color: black"><b>My
									Profile</b></a></li>
						<li><a href="/user/myPolicies" style="color: black"><b>My
									Policies</b></a></li>
						<li><a href="/user/myfamily" style="color: black"><b>My
									Family</b></a></li>
						<li><a href="/user/contact" style="color: black"><b>Contact
									Us</b></a></li>
						<li><a href="/logout" style="color: black"><b>Logout</b></a></li>
					</div>
				</ul>
			</div>

			<!-- <button class="btn btn-danger" data-toggle="modal" data-target="#signupModal">SignUp</button> -->
		</div>

	</nav>
	<br>

	<%
	UserProfile u = (UserProfile) request.getAttribute("user");
	Long customPrice = (Long) request.getAttribute("cusprice");
	Integer members = (Integer) request.getAttribute("mem");
	%>

	<div class="container">
		<h5>Your Details</h5>
		<table class="table table-striped table-bordered">
			<thead class="table">
				<tr>
					<th>First Name</th>
					<th>Last Name</th>
					<th>Email</th>
					<th>Mobile No</th>
					<th>Age</th>
				</tr>
			</thead>
			<tbody>
				<tr>
					<td><%=u.getFirstName()%></td>
					<td><%=u.getLastName()%></td>
					<td><%=u.getEmail()%></td>
					<td><%=u.getMobileNo()%></td>
					
					<%
					LocalDate currentdate1 = LocalDate.now();
					int yearr = currentdate1.getYear() -u.getYob();
					%>
					
					<td><%=yearr%></td>
				</tr>

			</tbody>
		</table>
	</div>
	<!-- Changes on 10/01/2022 -->
	<div class="container">
		<h5>Insurance Plan Details</h5>
		<%
		InsurancePlan ins = (InsurancePlan) request.getAttribute("insurdata");
		%>
		<table class="table table-striped table-bordered">
			<thead class="table">
				<tr>
					<th>Insurance Name</th>
					<th>Insurance Plan Detail</th>
					<th>Insurance Type</th>
					<th>Base Price</th>
					<th>Coverage Amount</th>
				</tr>
			</thead>
			<tbody>
				<%
				InsurancePlan plan = (InsurancePlan) request.getAttribute("ins");
				%>
				<tr>
					<td><%=plan.getInsuranceCompName()%></td>
					<td><%=plan.getInsPlanDetail()%></td>
					<td><%=plan.getInsuranceType()%></td>
					<td><%=plan.getBasePrice()%></td>
					<td><%=plan.getCoverageAmount()%></td>
				</tr>

			</tbody>
		</table>

	</div>

	<%
	if (members > 0) {
		DepRepo depRepo = (DepRepo) request.getAttribute("deprep");
		ArrayList<Long> depDet = (ArrayList<Long>) request.getAttribute("dependents");
	%>
	<div class="container">
		<h5>Dependents Included</h5>
		<table class="table table-striped table-bordered">
			<thead class="table">
				<tr>
					<th>Full Name</th>
					<th>Relation</th>
					<th>Age</th>
				</tr>
			</thead>
			<tbody>
				<%
				if (depDet.size() > 0 || depDet != null) {

					for (Long j : depDet) {

						DepDetails i = (DepDetails) depRepo.getById(j);
				%>
				<tr>
					<td><%=i.getDepFullName()%></td>
					<td><%=i.getDepRelation()%></td>
					<%
					LocalDate currentdate = LocalDate.now();
					int year = currentdate.getYear() -i.getDepyob();
					%>
					
					<td><%=year%></td>
				</tr>
				<%
				}
				}
				%>
			</tbody>
		</table>
		<%
		}
		%>
	</div>
	<% BeneficiaryDetails ben=(BeneficiaryDetails) request.getAttribute("ben");
 %>
	<div class="container">
		<h5>Beneficiary Detail</h5>
		<table class="table table-striped table-bordered">
			<thead class="table">
				<tr>
					<th>Name</th>
					<th>Relation</th>
					<th>Email</th>
					<th>Mobile No</th>
				</tr>
			</thead>
			<tbody>
				<tr>
					<td><%=ben.getBenName()%></td>
					<td><%=ben.getBenRelation()%></td>
					<td><%=ben.getEmailAddress()%></td>
					<td><%=ben.getBenMobNo()%></td>


				</tr>

			</tbody>
		</table>
	</div>

	<% Integer years =Integer.parseInt(request.getParameter("years")); %>
	<div class="container">
		<div class="card-deck mb-3 text-center">
			<div class="card mb-12 box-shadow">
				<div class="card-header">
					<h4 class="my-0 font-weight-normal">Customized Annual Premium</h4>
				</div>
				<div class="card-body">
					<h1 class="card-title pricing-card-title"><%=customPrice%><small
							class="text-muted"> INR/Year</small>
					</h1>
					<ul class="list-unstyled mt-3 mb-4">
						<%if(members == 0) 
						   {%>
						    <h5><li>No addtional members included</li></h5>
						 <%}
						  else
							{ %>
						<h5><li><%=members%> addtional members included</li></h5>
						  <%} %>
						<h5><li>Selected Policy Tenure is <%=years%> Years</li> </h5>
						<p>Additional Charges Applied By Number of Dependents and Age of Primary Subscriber</p>
					</ul>
					<form action="/user/buy" method="post">
						<button type="submit" class="btn btn-lg  btn-outline-success">Buy
							Now</button>
					</form>
				</div>
			</div>
		</div>
	</div>
	<!-- <div class="card mb-4 box-shadow">
                <div class="card-header">
                    <h4 class="my-0 font-weight-normal">Pro</h4>
                </div>
                <div class="card-body">
                    <h1 class="card-title pricing-card-title">$15 <small class="text-muted">/ mo</small></h1>
                    <ul class="list-unstyled mt-3 mb-4">
                        <li>20 users included</li>
                        <li>10 GB of storage</li>
                        <li>Priority email support</li>
                        <li>Help center access</li>
                    </ul>
                    <button type="button" class="btn btn-lg btn-block btn-primary">Get started</button>
                </div>
            </div> -->
	<!-- <div class="card mb-6 box-shadow">
                <div class="card-header">
                    <h4 class="my-0 font-weight-normal">Enterprise</h4>
                </div>
                <div class="card-body">
                    <h1 class="card-title pricing-card-title">$29 <small class="text-muted">/ mo</small></h1>
                    <ul class="list-unstyled mt-3 mb-4">
                        <li>30 users included</li>
                        <li>15 GB of storage</li>
                        <li>Phone and email support</li>
                        <li>Help center access</li>
                    </ul>
                    <button type="button" class="btn btn-lg btn-block btn-primary">Contact us</button>
                </div>
            </div>
        </div> -->



	<script src="https://code.jquery.com/jquery-3.2.1.slim.min.js"
		integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN"
		crossorigin="anonymous">
		
	</script>
	<script
		src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js"
		integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q"
		crossorigin="anonymous"></script>
	<script
		src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js"
		integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl"
		crossorigin="anonymous"></script>
</body>

</html>