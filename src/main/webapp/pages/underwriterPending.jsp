<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>

<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>

<!doctype html>
<html lang="en">

<head>
<!-- Required meta tags -->
<meta charset="utf-8">
<meta name="viewport"
	content="width=device-width, initial-scale=1, shrink-to-fit=no">

<!-- Bootstrap CSS -->
<link rel="stylesheet"
	href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css"
	integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm"
	crossorigin="anonymous">

<title>Life Insurance</title>
<style>
#navname{
color:white;
float:Right;
padding-right: 20px;
font-size:20px;
}
</style>
</head>

<body>
	<nav class="navbar navbar-expand-lg navbar-dark bg-dark">
		<a class="navbar-brand" href="#">Insurance App</a>
		<button class="navbar-toggler" type="button" data-toggle="collapse"
			data-target="#navbarSupportedContent"
			aria-controls="navbarSupportedContent" aria-expanded="false"
			aria-label="Toggle navigation">
			<span class="navbar-toggler-icon"></span>
		</button>

		<div class="collapse navbar-collapse" id="navbarSupportedContent">
			<ul class="navbar-nav mr-auto">
				<li class="nav-item active"><a class="nav-link" href="/underwriter/homepage">Home</a>
				</li>
				
			</ul>
			<div id="navname">Hi Underwriter</div>	
			
		</div>
		
		<div class="mx-2">
            <!-- <button class="btn btn-success">Logout</button> -->
            <div class="dropdown">
               <a href="/logout"> <button class="btn btn-success">Logout</button></a>
                  
            </div>

            
        </div>

	</nav>

	<br>
	<h3 style="text-align:center;">All pending requests</h3>
	<br>
	
	
	
<% String msss=(String)request.getAttribute("statuschangesmsg"); %>

<% if(msss != null)
	{
	%>
<div id="updatem"><h5 style="color:green ;text-align:center"><%=msss %></h5></div>
<%
}

%> 
	<div class="container">

		<div class="row"></div>
		<table class="table table-striped table-bordered">
			<thead class="table-dark">
				<tr>
					<th>Buy Date</th>
					<th>Policy Id</th>
					<th>Policy Name</th>
					<th>Policy Type</th>
					<th>Annual Premium</th>
					<th>Policy End Date</th>
					<th>Policy Status</th>
					<th>Actions</th>
				</tr>
			</thead>
			<tbody>

				<c:forEach var="poly" items="${m}">
					<tr>
						<td>${poly.date}</td>
						<td>${poly.policyId}</td>
						<td> ${poly.getInsurance().getInsuranceCompName()}</td>
						<td> ${poly.getInsurance().getInsuranceType()}</td>
						<td>${poly.policyCost}</td>
						<td>${poly.policyEndDate}</td>
						<td>${poly.policyStatus}</td>
						<td>
							<div class="dropdown">
								<button class="btn btn-secondary dropdown-toggle" type="button"
									id="dropdownMenuButton" data-toggle="dropdown"
									aria-haspopup="true" aria-expanded="false">Action</button>
								<div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
									<form action="/underwriter/approved" method="post">
										<button type="submit" class="dropdown-item" id="edititem"
											name="button" value="${poly.policyId}">Approve</button>
									</form>
									<form action="/underwriter/reject" method="post">
										<button type="submit" class="dropdown-item" id="edititem"
											name="button" value="${poly.policyId}">Reject</button>
									</form>
									<form action="/underwriter/allPolyDet" method="post">
									<button name="insid" value="${poly.policyId}"
										class="dropdown-item">View Details</button>
								</form>
								</div>
								
							</div>
						</td>

					</tr>
				</c:forEach>

			</tbody>
		</table>

	</div>







	<!-- Optional JavaScript -->
	<!-- jQuery first, then Popper.js, then Bootstrap JS -->
	<script src="https://code.jquery.com/jquery-3.2.1.slim.min.js"
		integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN"
		crossorigin="anonymous"></script>
	<script
		src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js"
		integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q"
		crossorigin="anonymous"></script>
	<script
		src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js"
		integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl"
		crossorigin="anonymous"></script>

</body>

</html>
