<%@page import="com.impetus.insuranceapp.repository.UserProfileRepo"%>
<%@page import="com.impetus.insuranceapp.model.UserProfile"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>

<!doctype html>
<html lang="en">

<head>
<!-- Required meta tags -->
<meta charset="utf-8">
<meta name="viewport"
	content="width=device-width, initial-scale=1, shrink-to-fit=no">

<!-- Bootstrap CSS -->
<link rel="stylesheet"
	href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css"
	integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm"
	crossorigin="anonymous">

<title>Life Insurance</title>
<style>
#navname{
color:white;
float:Right;
padding-right: 20px;
font-size:20px;
}
</style>
</head>

<body>
	<nav class="navbar navbar-expand-lg navbar-dark bg-dark">
		<a class="navbar-brand" href="#">Insurance App</a>
		<button class="navbar-toggler" type="button" data-toggle="collapse"
			data-target="#navbarSupportedContent"
			aria-controls="navbarSupportedContent" aria-expanded="false"
			aria-label="Toggle navigation">
			<span class="navbar-toggler-icon"></span>
		</button>

		<div class="collapse navbar-collapse" id="navbarSupportedContent">
			<ul class="navbar-nav mr-auto">
				<li class="nav-item active"><a class="nav-link" href="/admin/homepage">Home</a>
				</li>
				<li class="nav-item active"><a class="nav-link" name="allplan" value="planvalue" href="/admin/allplans">All Plans</a>
				</li>
			</ul>
			<div id="navname">Hi Admin</div>	
			
		</div>
		
		<div class="mx-2">
            <!-- <button class="btn btn-success">Logout</button> -->
            <div class="dropdown">
               <a href="/logout"> <button class="btn btn-success">Logout</button></a>
                  
            </div>

            
        </div>

	</nav>
	
	<br><br>
	<% UserProfileRepo userRepo = (UserProfileRepo)request.getAttribute("userRepo");%>
<div class="container">
		
		<div class="row">

		</div>
		<table class="table table-striped table-bordered">
			<thead class="table-dark">
				<tr>
					<th>Buy Date</th>
					<th>Policy Id</th>
					<th>Full Name</th>
					<th>Annual Premium</th>
					<th>Policy End Date</th>
					<th>Approval Type</th>
					<th>Policy Status</th>
					<th>Action</th>
				</tr>
			</thead>
			<tbody>
				
					<c:forEach var="poly" items="${m}">
						<tr>
							<td>${poly.date}</td>
							<td>${poly.policyId}</td>
							<td>${userRepo.getById(poly.user.getUserId()).getFirstName()} ${userRepo.getById(poly.user.getUserId()).getLastName()}</td>
							<td>${poly.policyCost}</td>
							<td>${poly.policyEndDate}</td>
							<td>${poly.approval}</td>
							<td>${poly.policyStatus}</td>
							<td><form action="/admin/allPolyDet" method="post">
									<button name="insid" value="${poly.policyId}"
										class="btn btn-success btn-sm">View Details</button></form></td>
							
<!-- 							<form action = "insplansdet" method="post"> -->
<%-- 							<td><button name="insid" value="${plan.insuranceId}" class="btn btn-success">Proceed</button></td> --%>
<!-- 							</form> -->
						</tr>
					</c:forEach>
			
			</tbody>
		</table>

	</div>	
	<br>
    
	
		
				
					
			

	<!-- Optional JavaScript -->
	<!-- jQuery first, then Popper.js, then Bootstrap JS -->
	<script src="https://code.jquery.com/jquery-3.2.1.slim.min.js"
		integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN"
		crossorigin="anonymous"></script>
	<script
		src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js"
		integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q"
		crossorigin="anonymous"></script>
	<script
		src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js"
		integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl"
		crossorigin="anonymous"></script>

</body>

</html>
    	