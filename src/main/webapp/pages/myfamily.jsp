<%@page import="java.util.ArrayList"%>
<%@page import="java.time.LocalDate"%>
<%@page import="com.impetus.insuranceapp.model.DepDetails"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>

<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>


<!doctype html>
<html lang="en">

<head>
<!-- Required meta tags -->
<meta charset="utf-8">
<meta name="viewport"
	content="width=device-width, initial-scale=1, shrink-to-fit=no">

<!-- Bootstrap CSS -->
<link rel="stylesheet"
	href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css"
	integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm"
	crossorigin="anonymous">

<title>All Insurance</title>
<style>
#addM {
	margin-top: 20px;
	margin-bottom: 20px;
	text-align: center;
}
</style>
<style>
/* for dropdown in navbar */
.dropdown:hover .dropdown-menu {
	display: block;
	margin-top: 0;
}

#navname {
	color: white;
	float: Right;
	padding-right: 20px;
	font-size: 20px;
}
</style>
</head>

<body>
	<nav class="navbar navbar-expand-lg navbar-dark bg-dark">
		<a class="navbar-brand" href="#">Insurance App</a>
		<button class="navbar-toggler" type="button" data-toggle="collapse"
			data-target="#navbarSupportedContent"
			aria-controls="navbarSupportedContent" aria-expanded="false"
			aria-label="Toggle navigation">
			<span class="navbar-toggler-icon"></span>
		</button>

		<div class="collapse navbar-collapse" id="navbarSupportedContent">
			<ul class="navbar-nav mr-auto">
				<li class="nav-item active"><a class="nav-link"
					href="/user/homered">Home<span class="sr-only">(current)</span></a></li>
				<div class="dropdown">
					<button class="btn btn-dark dropdown-toggle" type="button"
						id="dropdownMenuButton" data-toggle="dropdown"
						aria-haspopup="true" aria-expanded="false">Insurance
						Plans</button>
					<div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
						<a class="dropdown-item" href="/user/lifeplans">Life Insurance</a>
						<a class="dropdown-item" href="/user/dentalplans">Dental
							Insurance</a> <a class="dropdown-item" href="/user/visionplans">Vision
							Insurance</a>
					</div>
				</div>
			</ul>
			<div id="navname">Hi ${yourname}</div>

		</div>

		<div class="mx-2">
			<!-- <button class="btn btn-success">Logout</button> -->
			<div class="dropdown">
				<button class="btn btn-success dropdown-toggle" type="button"
					data-toggle="dropdown">
					Actions <span class="caret"></span>
				</button>
				<ul class="dropdown-menu">
					<div class="dropbt">
						<li><a href="/user/myprofile" style="color: black"><b>My
									Profile</b></a></li>
						<li><a href="/user/myPolicies" style="color: black"><b>My
									Policies</b></a></li>
						<li><a href="/user/myfamily" style="color: black"><b>My
									Family</b></a></li>
						<li><a href="/user/contact" style="color: black"><b>Contact
									Us</b></a></li>
						<li><a href="/logout" style="color: black"><b>Logout</b></a></li>
					</div>
				</ul>
			</div>

			<!-- <button class="btn btn-danger" data-toggle="modal" data-target="#signupModal">SignUp</button> -->
		</div>

	</nav>


	<div id="addM">
		<a href="/user/useraddmember"><button type="button"
				class="btn btn-primary">Add Member</button></a>
	</div>

	<%
	String mss = (String) session.getAttribute("updatedepmsg");
	%>

	<%
	if (mss != null) {
	%>
	<div id="updatem">
		<h5 style="color: green; text-align: center">
			Medical history of
			<%=mss%>
			updated successfully
		</h5>
	</div>
	<%
	}
	session.removeAttribute("updatedepmsg");
	%>



	<%
	String msss = (String) session.getAttribute("deletedepmsg");
	%>

	<%
	if (msss != null) {
	%>
	<div id="updatem">
		<h5 style="color: red; text-align: center">
			Dependent
			<%=msss%></h5>
	</div>
	<%
	}
	session.removeAttribute("deletedepmsg");
	%>
	<div class="container">

		<div class="row"></div>
		<table class="table table-striped table-bordered">
			<thead class="table-dark">
				<tr>

					<th>Name</th>
					<th>Relation</th>
					<th>Age</th>

					<th>Actions</th>
				</tr>
			</thead>
			<tbody>


				<%
				ArrayList<DepDetails> dependents = (ArrayList<DepDetails>) request.getAttribute("dep");
				if (dependents != null || dependents.size() > 0) {
					for (DepDetails plan : dependents) {
				%>

				<tr>
					<td><%=plan.getDepFullName()%></td>
					<td><%=plan.getDepRelation()%></td>
					<%
					LocalDate currentdate = LocalDate.now();
					int dyear = currentdate.getYear() - plan.getDepyob();
					%>

					<td><%=dyear%></td>

					<td>
						<div class="dropdown">
							<button class="btn btn-primary dropdown-toggle" type="button"
								id="dropdownMenuButton" data-toggle="dropdown"
								aria-haspopup="true" aria-expanded="false">Action</button>
							<div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
								<form action="/user/depUpdateDetail" method="post">
									<button type="submit" class="dropdown-item" id="edititem"
										name="button" value="<%=plan.getDepId()%>">Update
										Medical History</button>
								</form>
								<form action="/user/depDeleteDetail" method="post">
									<button type="submit" class="dropdown-item" id="edititem"
										name="button" value="<%=plan.getDepId()%>">Delete</button>
								</form>

							</div>
						</div>
					</td>
				</tr>
				<%
				}
				}
				%>



			</tbody>
		</table>

	</div>





	<!-- Optional JavaScript -->
	<!-- jQuery first, then Popper.js, then Bootstrap JS -->
	<script src="https://code.jquery.com/jquery-3.2.1.slim.min.js"
		integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN"
		crossorigin="anonymous"></script>
	<script
		src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js"
		integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q"
		crossorigin="anonymous"></script>
	<script
		src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js"
		integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl"
		crossorigin="anonymous"></script>

</body>

</html>