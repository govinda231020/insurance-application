const form = document.getElementById('form');
const username = document.getElementById('username');
const email = document.getElementById('email');
const password = document.getElementById('password');
const password2 = document.getElementById('password2');


const lastname = document.getElementById('lastname');

const mobile=document.getElementById('mobile');

const yob=document.getElementById('yob');

const address=document.getElementById('address');

function checkInputs() {
	// trim to remove the whitespaces
    var flag=0;
	const usernameValue = username.value.trim();
	const emailValue = email.value.trim();
	const passwordValue = password.value.trim();
	const password2Value = password2.value.trim();

const lastnameValue=lastname.value.trim();
if(lastnameValue === '') {
    setErrorFor(lastname, 'Lastname cannot be blank');
    flag=0;
} else {
    setSuccessFor(lastname);
    flag=1;
}








// const gender =document.getElementById('inlineRadio1');
// if(document.getElementById('inlineRadio1').checked || document.getElementById('inlineRadio2').checked) {
// 	flag=1;
//   }
// else {
// 	document.getElementById("radioerr").innerHTML = "Please select one !" ;
//         document.getElementById("err").style.color = "red";
// 		flag=0;
// }






	
	if(usernameValue === '') {
		setErrorFor(username, 'Username cannot be blank');
        flag=0;
	} else {
		setSuccessFor(username);
        flag=1;
	}


	
	if(emailValue === '') {
		setErrorFor(email, 'Email cannot be blank');
        flag= 0;
	} else if (!isEmail(emailValue)) {
		setErrorFor(email, 'Not a valid email');
        flag= 0;
	} else {
		setSuccessFor(email);
        flag= 1;
	}
	
	if(passwordValue === '') {
		setErrorFor(password, 'Password cannot be blank');
        flag= 0;
	} else {
		setSuccessFor(password);
        flag= 1;
	}
	
	if(password2Value === '') {
		setErrorFor(password2, 'Password cannot be blank');
        flag= 0;
	} else if(passwordValue !== password2Value) {
		setErrorFor(password2, 'Passwords does not match');
        flag= 0;
	} else{
		setSuccessFor(password2);
        flag= 1;
	}


	const mobilevalue=mobile.value.trim();

if (/^\d{10}$/.test(mobilevalue)) {
    setSuccessFor(mobile);
    flag=1;
}
else{
	setErrorFor(mobile, 'Please enter valid number!');
    flag=0;
}




const yobvalue=yob.value.trim();

if(yobvalue ==='' ){
	setErrorFor(yob, 'year cannot be blank');
    flag=0;
}
else if(yobvalue<1960 || yobvalue> (new Date().getFullYear())-5)
{
	setErrorFor(yob, 'Enter valid Year');
    flag=0;
}
else{
	setSuccessFor(yob);
    flag=1;
}


const addressValue=address.value.trim();
if(addressValue === '') {
	setErrorFor(address, 'Address cannot be blank');
	flag=0;
} else {
	setSuccessFor(address);
	flag=1;
}



return Boolean(flag);


}

function setErrorFor(input, message) {
	const formControl = input.parentElement;
	const small = formControl.querySelector('small');
	formControl.className = 'form-control error';
	small.innerText = message;
}

function setSuccessFor(input) {
	const formControl = input.parentElement;
	formControl.className = 'form-control success';
}
	
function isEmail(email) {
	return /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/.test(email);
}




// const ff=0;

// form.addEventListener('submit', e => {
// 	e.preventDefault();
	
// 	checkInputs();
// 	if (ff===1)
// 	{
// 		(this).submit();
// 	}
// });






