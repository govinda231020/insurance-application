<%@page import="java.time.format.DateTimeFormatter"%>
<%@page import="com.impetus.insuranceapp.repository.DepRepo"%>
<%@page import="java.util.ArrayList"%>
<%@page import="java.time.LocalDate"%>
<%@page import="com.impetus.insuranceapp.model.DepDetails"%>
<%@page import="com.impetus.insuranceapp.model.UserProfile"%>
<%@page import="com.impetus.insuranceapp.model.PolicyDetail"%>
<%@page import="com.impetus.insuranceapp.model.InsurancePlan"%>
<%@page import="com.impetus.insuranceapp.model.MedicalHistory"%>
<%@page import="com.impetus.insuranceapp.model.BeneficiaryDetails"%>

<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>

<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>

<!doctype html>
<html lang="en">

<head>
<!-- Required meta tags -->
<meta charset="utf-8">
<meta name="viewport"
	content="width=device-width, initial-scale=1, shrink-to-fit=no">

<!-- Bootstrap CSS -->
<link rel="stylesheet"
	href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css"
	integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm"
	crossorigin="anonymous">

<title>Life Insurance</title>
<style>
#navname {
	color: white;
	float: Right;
	padding-right: 20px;
	font-size: 20px;
}
</style>
<style>
#navname {
	color: white;
	float: Right;
	padding-right: 20px;
	font-size: 20px;
}
</style>
</head>

<body>
	<nav class="navbar navbar-expand-lg navbar-dark bg-dark">
		<a class="navbar-brand" href="#">Insurance App</a>
		<button class="navbar-toggler" type="button" data-toggle="collapse"
			data-target="#navbarSupportedContent"
			aria-controls="navbarSupportedContent" aria-expanded="false"
			aria-label="Toggle navigation">
			<span class="navbar-toggler-icon"></span>
		</button>

		<div class="collapse navbar-collapse" id="navbarSupportedContent">
			<ul class="navbar-nav mr-auto">
				<li class="nav-item active"><a class="nav-link"
					href="/user/homered">Home<span class="sr-only">(current)</span></a></li>
				<div class="dropdown">
					<button class="btn btn-dark dropdown-toggle" type="button"
						id="dropdownMenuButton" data-toggle="dropdown"
						aria-haspopup="true" aria-expanded="false">Insurance
						Plans</button>
					<div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
						<a class="dropdown-item" href="/user/lifeplans">Life Insurance</a>
						<a class="dropdown-item" href="/user/dentalplans">Dental
							Insurance</a> <a class="dropdown-item" href="/user/visionplans">Vision
							Insurance</a>
					</div>
				</div>
			</ul>
			<div id="navname">Hi ${yourname}</div>

		</div>

		<div class="mx-2">
			<!-- <button class="btn btn-success">Logout</button> -->
			<div class="dropdown">
				<button class="btn btn-success dropdown-toggle" type="button"
					data-toggle="dropdown">
					Actions <span class="caret"></span>
				</button>
				<ul class="dropdown-menu">
					<div class="dropbt">
						<li><a href="/user/myprofile" style="color: black"><b>My
									Profile</b></a></li>
						<li><a href="/user/myPolicies" style="color: black"><b>My
									Policies</b></a></li>
						<li><a href="/user/myfamily" style="color: black"><b>My
									Family</b></a></li>
						<li><a href="/user/contact" style="color: black"><b>Contact
									Us</b></a></li>
						<li><a href="/logout" style="color: black"><b>Logout</b></a></li>
					</div>
				</ul>
			</div>

			<!-- <button class="btn btn-danger" data-toggle="modal" data-target="#signupModal">SignUp</button> -->
		</div>

	</nav>


	<br>

	<%
	UserProfile u = (UserProfile) request.getAttribute("user");
	PolicyDetail pod = (PolicyDetail) request.getAttribute("pod");
	InsurancePlan ins = (InsurancePlan) request.getAttribute("ins");
	MedicalHistory med = (MedicalHistory) request.getAttribute("medhis");
	ArrayList<Long> arr = (ArrayList<Long>) request.getAttribute("arr");
	BeneficiaryDetails ben = (BeneficiaryDetails) request.getAttribute("ben");
	%>

	<div class="container">
		<h5>Policy Details</h5>
		<table class="table table-striped table-bordered">
			<thead class="table ">
				<tr>
					<th>Policy Id</th>
					<th>Policy Buy Date</th>
					<th>Policy End Date</th>
					<th>Policy Tenure(In Years)</th>
				</tr>
			</thead>
			<tbody>
				<tr>
					<td><%=pod.getPolicyId()%></td>
					<td><%=pod.getDate()%></td>
					<td><%=pod.getPolicyEndDate()%></td>

					<%
					Integer y1 = Integer.parseInt(pod.getDate().substring(0, 4));
					Integer y2 = Integer.parseInt(pod.getPolicyEndDate().substring(0, 4));
					%>

					<td><%=y2 - y1%></td>

				</tr>

			</tbody>
		</table>
	</div>
	<br>

	<div class="container">

		<h5>Primary Holder of Policy</h5>
		<table class="table table-striped table-bordered ">
			<thead class="table">
				<tr>
					<th>First Name</th>
					<th>Last Name</th>
					<th>Email</th>
					<th>Age</th>
					<th>Mobile No</th>
				</tr>
			</thead>
			<tbody>
				<tr>
					<td><%=u.getFirstName()%></td>
					<td><%=u.getLastName()%></td>
					<td><%=u.getEmail()%></td>

					<%
					LocalDate currentdate = LocalDate.now();
					int year = currentdate.getYear() - u.getYob();
					%>

					<td><%=year%></td>


					<td><%=u.getMobileNo()%></td>

				</tr>

			</tbody>
		</table>
	</div>


	<br>
	<div class="container">
		<h5>Insurance Plan</h5>
		<table class="table table-striped table-bordered">
			<thead class="table ">
				<tr>
					<th>Insurance Name</th>
					<th>Insurance Type</th>
					<th>Base Price</th>
					<th>Annual Premium</th>
					<th>Maximum Age</th>
				</tr>
			</thead>
			<tbody>
				<tr>
					<td><%=ins.getInsuranceCompName()%></td>
					<td><%=ins.getInsuranceType()%></td>
					<td><%=ins.getBasePrice() %></td>
					<td><%=pod.getPolicyCost()%></td>
					<td><%=ins.getCoverageAge()%></td>

				</tr>

			</tbody>
		</table>
	</div>
	<br>
	<div class="container">
		<h5>Medical History</h5>
		<table class="table table-striped table-bordered">
			<thead class="table">
				<tr>
					<th>Heart Ailments</th>
					<th>Diabetes</th>
					<th>Tooth Decay</th>
					<th>Covid</th>
					<th>Cancer</th>
					<th>Cataract</th>
				</tr>
			</thead>
			<tbody>
				<tr>
					<td><%=med.getHeartAilments()%></td>
					<td><%=med.getDiabetes()%></td>
					<td><%=med.getToothDecay()%></td>
					<td><%=med.getCovid()%></td>
					<td><%=med.getCancer()%></td>
					<td><%=med.getCataract()%></td>

				</tr>

			</tbody>
		</table>
	</div>
	<br>

	<%
	if (arr.size() > 0) {
		DepRepo depRepo = (DepRepo) request.getAttribute("deprepo");
	%>
	<div class="container">
		<h5>Dependents Included</h5>
		<table class="table table-striped table-bordered">
			<thead class="table">
				<tr>
					<th>Full Name</th>
					<th>Relation</th>
					<th>Age</th>
				</tr>
			</thead>
			<tbody>
				<%
				for (Long j : arr) {

					DepDetails i = (DepDetails) depRepo.getById(j);
				%>
				<tr>
					<td><%=i.getDepFullName()%></td>
					<td><%=i.getDepRelation()%></td>
					<%
					LocalDate currenttdate = LocalDate.now();
					int dyear = currentdate.getYear() - i.getDepyob();
					%>

					<td><%=dyear%></td>
				</tr>
				<%
				}
				%>
			</tbody>
		</table>
	</div>
	<%
	}
	%>


	<div class="container">
		<h5>Beneficiary Detail</h5>
		<table class="table table-striped table-bordered">
			<thead class="table">
				<tr>
					<th>Name</th>
					<th>Relation</th>
					<th>Email</th>
					<th>Mobile No</th>
				</tr>
			</thead>
			<tbody>
				<tr>
					<td><%=ben.getBenName()%></td>
					<td><%=ben.getBenRelation()%></td>
					<td><%=ben.getEmailAddress()%></td>
					<td><%=ben.getBenMobNo()%></td>


				</tr>

			</tbody>
		</table>
	</div>
	<br>




	<!-- Optional JavaScript -->
	<!-- jQuery first, then Popper.js, then Bootstrap JS -->
	<script src="https://code.jquery.com/jquery-3.2.1.slim.min.js"
		integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN"
		crossorigin="anonymous"></script>
	<script
		src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js"
		integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q"
		crossorigin="anonymous"></script>
	<script
		src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js"
		integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl"
		crossorigin="anonymous"></script>

</body>

</html>