<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
    <%@ page isELIgnored = "false" %>

<!doctype html>
<html lang="en">

    <head>
        <link rel="stylesheet"
            href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css"
            integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm"
            crossorigin="anonymous">
        
        <!-- Required meta tags -->
        <meta charset="utf-8">
        <meta name="viewport"
            content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <script src="https://kit.fontawesome.com/e38070cbc2.js"
            crossorigin="anonymous"></script>
            
            
            
            <script src="https://cdnjs.cloudflare.com/ajax/libs/crypto-js/3.1.2/rollups/aes.js"></script>
        <script src="http://crypto-js.googlecode.com/svn/tags/3.1.2/build/rollups/md5.js"></script>
        <!-- Bootstrap CSS -->
        <link rel="stylesheet"
            href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css"
            integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm"
            crossorigin="anonymous">
        <style>
        .form {
            padding: 5px 40px;
        }
        
        .form-control {
            margin-bottom: 5px;
            padding-bottom: 16px;
            position: relative;
            border-style: none;
        }
        
        .form-control label {
            display: inline-block;
            margin-bottom: 5px;
        }
        
        .form-control input {
            
            border: 2px solid #f0f0f0;
            border-radius: 4px;
            display: block;
            font-family: inherit;
            font-size: 14px;
            padding: 10px;
            width: 100%;
        }
        
        .form-control input:focus {
            outline: 0;
            border-color: #777;
        }
        
        .form-control.success input {
            border-color: #2ecc71;
        }
        
        .form-control.error input {
            border-color: #e74c3c;
        }
        
        .form-control i {
            visibility: hidden;
            position: absolute;
            top: 50px;
            right: 20px;
        }
        
        .form-control.success i.fa-check-circle {
            color: #2ecc71;
            visibility: visible;
        }
        
        .form-control.error i.fa-exclamation-circle {
            color: #e74c3c;
            visibility: visible;
        }
        
        .form-control small {
            color: #e74c3c;
            position: absolute;
            bottom: 0;
            left: 0;
            visibility: hidden;
        }
        
        .form-control.error small {
            visibility: visible;
        }
        
        .form button {
            background-color: #8e44ad;
            border: 2px solid #8e44ad;
            border-radius: 4px;
            color: #fff;
            display: block;
            font-family: inherit;
            font-size: 16px;
            padding: 10px;
            margin-top: 20px;
            width: 100%;
        }
        
        /* for dropdown in navbar */
        .dropdown:hover .dropdown-menu {
            display: block;
        }
        
        .form-control span {
            color: red;
            font-weight: bold;
        }
        
        #star {
            color: red;
            font-weight: bold;
        }
        
        
          #msgerr{
                color:red;
                font-size: 20px;
                font-weight: bold;
                text-align:center;
                }
                
                #regsuccess{
                
                color:green;
                text-align:center;
                }
        </style>
    
    <script type="text/javascript">
        function disableBack() { window.history.forward(); }
        setTimeout("disableBack()", 0);
        window.onunload = function () { null };
</script> 

    <title>Log in Page</title>
</head>

<body>


      

    <nav class="navbar navbar-expand-lg navbar-dark bg-dark">
        <a class="navbar-brand" href="#">Insurance App</a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent"
            aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>

        <div class="collapse navbar-collapse" id="navbarSupportedContent">
            <ul class="navbar-nav mr-auto">
                <li class="nav-item active">
                    <a class="nav-link" href="guest">Home<span class="sr-only">(current)</span></a>
                </li>
                <!-- <li class="nav-item">
                    <a class="nav-link" href="#">My Policies</a>
                </li>
                
                <li class="nav-item">
                    <a class="nav-link" href="#">My Profile</a>
          
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="#">Contact Us</a>
                </li> -->
            </ul>
            
            <div class="mx-2">	
				<a href="login">
					<button class="btn btn-success">Login</button>
				</a>
				<a href="signup">
					<button class="btn btn-success">SignUp</button>
				</a>
			</div>

        </div>
    </nav>
    <br>
 <div id="regsuccess"><h2>${successmsg}</h2></div>
    



    <div class="container">
        <div class="row">
            <div class="col-lg-6 col-md-6 col-sm-6 container justify-content-center card">
                <h2 class="text-center">Log In</h2>
                <div class="card-body">
					<div id="msgerr">${message}</div>
					 <div id="msgerr">${Passmessage}</div>
                    <form action="homelogin" method="POST"  id="form" class="form" onsubmit="return checkInputs()">
                        
                      

                        <div class="form-control">
                            <label for="username">Email</label> <span> *</span><input type="email"
                                onChange="checkemail()" placeholder="Enter email" id="email" name="email" /> <i
                                class="fas fa-check-circle"></i> <i class="fas fa-exclamation-circle"></i> <small>Error
                                message</small>
                        </div>
                        

                        <div class="form-control">
                            <label for="username">Password</label> <span> *</span> <input type="password"
                                onChange="loginFunction()" placeholder="Enter password" id="password" value=""
                                name="password" /> <i class="fas fa-check-circle"></i>
                            <i class="fas fa-exclamation-circle"></i> <small>Error
                                message</small>
                        </div>
                        
                        
                
                        <button type="submit" class="btn btn-primary" >Login</button>
                       <p style="text-align:center;">Don't have an account? <a href="signup" >Sign up</a></p>
                    </form>
                
                </div>
            </div>
        </div>
    </div>
    
    
       <script src="https://cdnjs.cloudflare.com/ajax/libs/crypto-js/3.1.2/rollups/aes.js"></script>
<script src="http://crypto-js.googlecode.com/svn/tags/3.1.2/build/rollups/md5.js"></script>
    <script>
    const email = document.getElementById('email');
    const password = document.getElementById('password');

    function checkemail() {
			const emailValue = email.value.trim();
			if (emailValue === '') {
				setErrorFor(email, 'Email cannot be blank');

			} else if (!isEmail(emailValue)) {
				setErrorFor(email, 'Not a valid email');

			} else {
				setSuccessFor(email);
			}
		}

    function loginFunction()
    {
    	//const password = document.getElementById('password');
const passwordValue = password.value.trim();
        if (passwordValue === '') {
				setErrorFor(password, 'Password cannot be blank');
				//flag = 0;
			} else {
				setSuccessFor(password);
				
                
				//flag = 1;
			}  
    	//const passwordValue = password.value.trim();
    	//var hash=CryptoJS.MD5(passwordValue);
		//document.getElementById('password').value=hash;
    }


    function checkInputs(){
        var flag = 0;
        const emailValue = email.value.trim();
        const passwordValue = password.value.trim();

        if (emailValue === '') {
				setErrorFor(email, 'Email cannot be blank');
				flag = 0;
			} else if (!isEmail(emailValue)) {
				setErrorFor(email, 'Not a valid email');
				flag = 0;
			} else {
				setSuccessFor(email);
				flag = 1;
			}
        
        
        

            if (passwordValue === '') {
				setErrorFor(password, 'Password cannot be blank');
				flag = 0;
			} else {
				setSuccessFor(password);
                const passwordValue = password.value.trim();
    	var hash=CryptoJS.MD5(passwordValue);
		document.getElementById('password').value=hash;
				//flag = 1;
			}  
            return Boolean(flag);

    }
    function setErrorFor(input, message) {
			const formControl = input.parentElement;
			const small = formControl.querySelector('small');
			formControl.className = 'form-control error';
			small.innerText = message;
		}

		function setSuccessFor(input) {
			const formControl = input.parentElement;
			formControl.className = 'form-control success';
		}


    function isEmail(email) {
			return /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/
					.test(email);
		}
    
    </script>
    <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js"
        integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN"
        crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js"
        integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q"
        crossorigin="anonymous"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js"
        integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl"
        crossorigin="anonymous"></script>
</body>

</html>