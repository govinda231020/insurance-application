<%@page import="com.impetus.insuranceapp.model.InsurancePlan"%>
<%@page import="java.util.ArrayList"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>


<!doctype html>
<html lang="en">

<head>
<!-- Required meta tags -->
<meta charset="utf-8">
<meta name="viewport"
	content="width=device-width, initial-scale=1, shrink-to-fit=no">

<!-- Bootstrap CSS -->
<link rel="stylesheet"
	href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css"
	integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm"
	crossorigin="anonymous">

<title>All Insurance</title>
<style>
#edititem:hover {
	background-color: rgb(172, 172, 172);
}
</style>
<style>
#navname {
	color: white;
	float: Right;
	padding-right: 20px;
	font-size: 20px;
}
</style>
<style>
#addM {
	margin-top: 20px;
	margin-bottom: 20px;
	text-align: center;
}
</style>
</head>

<body>
	<nav class="navbar navbar-expand-lg navbar-dark bg-dark">
		<a class="navbar-brand" href="#">Insurance App</a>
		<button class="navbar-toggler" type="button" data-toggle="collapse"
			data-target="#navbarSupportedContent"
			aria-controls="navbarSupportedContent" aria-expanded="false"
			aria-label="Toggle navigation">
			<span class="navbar-toggler-icon"></span>
		</button>

		<div class="collapse navbar-collapse" id="navbarSupportedContent">
			<ul class="navbar-nav mr-auto">
				<li class="nav-item active"><a class="nav-link"
					href="/admin/homepage">Home</a></li>
				<li class="nav-item active"><a class="nav-link" name="allplan"
					value="planvalue" href="/admin/allplans">All Plans</a></li>
			</ul>
			<div id="navname">Hi Admin</div>

		</div>

		<div class="mx-2">
			<!-- <button class="btn btn-success">Logout</button> -->
			<div class="dropdown">
				<a href="/logout">
					<button class="btn btn-success">Logout</button>
				</a>

			</div>


		</div>

	</nav>


	<!--     <a href="/admin/AddPlan"><button type="button" class="btn btn-primary">Add New Plan</button></a> -->
	<div id="addM">
		<a href="/admin/AddPlan"><button type="button"
				class="btn btn-primary">Add New Plan</button></a>
	</div>

	<%
	String msss = (String) session.getAttribute("insupdatemsg");
	%>

	<%
	if (msss != null) {
	%>
	<div id="updatem">
		<h5 style="color: green; text-align: center"><%=msss%></h5>
	</div>
	<%
	}
	session.removeAttribute("insupdatemsg");
	%>


	<%
	String mss = (String) request.getAttribute("insDeletemsg");
	%>

	<%
	if (mss != null) {
	%>
	<div id="updatem">
		<h5 style="color: red; text-align: center"><%=mss%></h5>
	</div>
	<%
	}
	%>





	<%
	ArrayList<InsurancePlan> insplans = (ArrayList<InsurancePlan>) request.getAttribute("insplans");
	%>





<%
	String emsg = (String) session.getAttribute("enablemsg");
	%>

	<%
	if (emsg != null) {
	%>
	<div id="updatem">
		<h5 style="color: green; text-align: center"><%=emsg%></h5>
	</div>
	<%
	}
	session.removeAttribute("enablemsg");
	%>
	
	
	
	
	<%
	String dmsg = (String) session.getAttribute("disablemsg");
	%>

	<%
	if (dmsg != null) {
	%>
	<div id="updatem">
		<h5 style="color: red; text-align: center"><%=dmsg%></h5>
	</div>
	<%
	}
	session.removeAttribute("disablemsg");
	%>

	<div class="container">

		<div class="row"></div>
		<table class="table table-striped table-bordered">
			<thead class="table-dark">
				<tr>
					<th>Insurance Id</th>
					<th>Insurance Name</th>
					<th>Insurance Plan Detail</th>
					<th>Insurance Type</th>
					<th>Base Price</th>
					<th>Coverage Amount</th>
					<th>Minimum Age</th>
					<th>Maximum Age</th>
					<th>Is Active</th>
					<th>Actions</th>
				</tr>
			</thead>
			<tbody>

				<%
				if (insplans.size() > 0) {

					for (InsurancePlan plan : insplans) {
				%>

				<tr>
					<td><%=plan.getInsuranceId()%></td>
					<td><%=plan.getInsuranceCompName()%></td>
					<td><%=plan.getInsPlanDetail()%></td>
					<td><%=plan.getInsuranceType()%></td>
					<td><%=plan.getBasePrice()%></td>

					<td><%=plan.getInsuranceType()%></td>
					<td><%=plan.getMinAge()%></td>
					<td><%=plan.getCoverageAge()%></td>
					<td><%=plan.isActiveplan()%></td>
					<td>
						<div class="dropdown">
							<button class="btn btn-secondary dropdown-toggle" type="button"
								id="dropdownMenuButton" data-toggle="dropdown"
								aria-haspopup="true" aria-expanded="false">Action</button>
							<div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
								<form action="/admin/UpdatePlan" method="post">
									<button type="submit" class="dropdown-item" id="edititem"
										name="button" value="<%=plan.getInsuranceId()%>">Update</button>
								</form>
								<!-- 									  <form action="/admin/DeletePlan" method="post"> -->
								<%-- 									<button type="submit" class="dropdown-item" id="edititem" name="button" value="${plan.insuranceId}">Delete</button> --%>
								<!-- 									  </form> -->

								<%
								if (plan.isActiveplan()) {
								%>
								<form action="/admin/Deactivateplan" method="post">
									<button type="submit" class="dropdown-item" id="edititem"
										name="button" value="<%=plan.getInsuranceId()%>">Deactivate</button>
								</form>
								<%
								}
								else
								{
								%>
								<form action="/admin/Deactivateplan" method="post">
									<button type="submit" class="dropdown-item" id="edititem"
										name="button" value="<%=plan.getInsuranceId()%>">Activate</button>
								</form>
							<%} %>

							</div>
						</div>
					</td>

				</tr>

				<%
				}
				}
				%>
			</tbody>


		</table>

	</div>





	<!-- Optional JavaScript -->
	<!-- jQuery first, then Popper.js, then Bootstrap JS -->
	<script src="https://code.jquery.com/jquery-3.2.1.slim.min.js"
		integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN"
		crossorigin="anonymous"></script>
	<script
		src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js"
		integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q"
		crossorigin="anonymous"></script>
	<script
		src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js"
		integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl"
		crossorigin="anonymous"></script>

</body>

</html>
