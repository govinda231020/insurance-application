<%@page import="java.util.ArrayList"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@page import="com.impetus.insuranceapp.model.UserProfile"%>
<!doctype html>
<html lang="en">

<head>
<!-- Required meta tags -->
<meta charset="utf-8">
<meta name="viewport"
	content="width=device-width, initial-scale=1, shrink-to-fit=no">

<!-- Bootstrap CSS -->
<link rel="stylesheet"
	href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css"
	integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm"
	crossorigin="anonymous">

<title>admin Page</title>
<style>
#navname{
color:white;
float:Right;
padding-right: 20px;
font-size:20px;
}
</style>
</head>

<body>
	<nav class="navbar navbar-expand-lg navbar-dark bg-dark">
		<a class="navbar-brand" href="#">Insurance App</a>
		<button class="navbar-toggler" type="button" data-toggle="collapse"
			data-target="#navbarSupportedContent"
			aria-controls="navbarSupportedContent" aria-expanded="false"
			aria-label="Toggle navigation">
			<span class="navbar-toggler-icon"></span>
		</button>

		<div class="collapse navbar-collapse" id="navbarSupportedContent">
			<ul class="navbar-nav mr-auto">
				<li class="nav-item active"><a class="nav-link" href="/admin/homepage">Home</a>
				</li>
				<li class="nav-item active"><a class="nav-link" name="allplan" value="planvalue" href="/admin/allplans">All Plans</a>
				</li>
			</ul>
			<div id="navname">Hi Admin</div>	
			
		</div>
		
		<div class="mx-2">
            <!-- <button class="btn btn-success">Logout</button> -->
            <div class="dropdown">
               <a href="/logout"> <button class="btn btn-success">Logout</button></a>
                  
            </div>

            
        </div>

	</nav>
	<br><br>
	
	
	<%
	ArrayList<UserProfile> users = (ArrayList<UserProfile>) request.getAttribute("users");
	%>
	
	
	<%
	String msss = (String) session.getAttribute("enablemsg");
	%>

	<%
	if (msss != null) {
	%>
	<div id="updatem">
		<h5 style="color: green; text-align: center"><%=msss%></h5>
	</div>
	<%
	}
	session.removeAttribute("enablemsg");
	%>
	
	
	
	
	
	<%
	String dmsg = (String) session.getAttribute("disablemsg");
	%>

	<%
	if (dmsg != null) {
	%>
	<div id="updatem">
		<h5 style="color: red; text-align: center"><%=dmsg%></h5>
	</div>
	<%
	}
	session.removeAttribute("disablemsg");
	%>
	
	
	
	<%Integer count = 1; %>
	<div class="container">
		
		<div class="row">

		</div>
		<table class="table table-striped table-bordered">
			<thead class="table-dark">
				<tr>
				<th>S.No.</th>
					<th>First Name</th>
					<th>Email</th>
					<th>Mobile</th>
					<th>Is Active</th>
					<th>Actions</th>
				</tr>
			</thead>
			<tbody>
			<%
				if (users.size() > 0) {

					for (UserProfile user : users) {
				%>
				
					
						<tr>
						<td><%=count++ %></td>
							<td><%=user.getFirstName() %></td>
							<td><%=user.getEmail() %></td>
							<td><%=user.getMobileNo() %></td>
							<td><%=user.isActive() %></td>
							
							<td>
								<div class="dropdown">
									<button class="btn btn-secondary dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
									  Action
									</button>
									<div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
									
									<%
								if (!user.isActive()) {
								%>
										<form action="/admin/EnableUser" method="post">
										  <button type="submit"  class="dropdown-item" id="edititem" name="button" value= "<%=user.getUserId() %>" >Activate</button>
										  </form>
										  
										  <%}else { %>
										  <form action="/admin/DisableUser" method="post">
										<button type="submit" class="dropdown-item" id="edititem" name="button" value="<%=user.getUserId() %>" >Deactivate</button>
										  </form>
										  <%} %>
										</div>
								  </div>
							</td>
						</tr>
					
				<%
				}
				}
				%>
			</tbody>
		</table>

	</div>
		
				
					
			

	<!-- Optional JavaScript -->
	<!-- jQuery first, then Popper.js, then Bootstrap JS -->
	<script src="https://code.jquery.com/jquery-3.2.1.slim.min.js"
		integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN"
		crossorigin="anonymous"></script>
	<script
		src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js"
		integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q"
		crossorigin="anonymous"></script>
	<script
		src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js"
		integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl"
		crossorigin="anonymous"></script>

</body>

</html>
