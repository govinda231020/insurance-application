<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>

<!doctype html>
<html lang="en">

<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css"
        integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">

    <title>Guest Page</title>
    <script src="https://kit.fontawesome.com/e38070cbc2.js" crossorigin="anonymous"></script>





<style>


.form {
	padding: 30px 40px;	
}

.form-control {
	margin-bottom: 10px;
	padding-bottom: 20px;
	position: relative;
    border-style: none;
}

.form-control label {
	display: inline-block;
	margin-bottom: 5px;
}

.form-control input {
	border: 2px solid #f0f0f0;
	border-radius: 4px;
	display: block;
	font-family: inherit;
	font-size: 14px;
	padding: 10px;
	width: 100%;
}

.form-control input:focus {
	outline: 0;
	border-color: #777;
}

.form-control.success input {
	border-color: #2ecc71;
}

.form-control.error input {
	border-color: #e74c3c;
}

.form-control i {
	visibility: hidden;
	position: absolute;
	top: 50px;
	right: 20px;
}

.form-control.success i.fa-check-circle {
	color: #2ecc71;
	visibility: visible;
}

.form-control.error i.fa-exclamation-circle {
	color: #e74c3c;
	visibility: visible;
}

.form-control small {
	color: #e74c3c;
	position: absolute;
	bottom: 0;
	left: 0;
	visibility: hidden;
}

.form-control.error small {
	visibility: visible;
}

.form button {
	background-color: #8e44ad;
	border: 2px solid #8e44ad;
	border-radius: 4px;
	color: #fff;
	display: block;
	font-family: inherit;
	font-size: 16px;
	padding: 10px;
	margin-top: 20px;
	width: 100%;
}



/* for dropdown in navbar */

.dropdown:hover .dropdown-menu {
  display: block;
}

</style>


</head>

<body>

    <nav class="navbar navbar-expand-lg navbar-dark bg-dark">
        <a class="navbar-brand" href="#">Insurance App</a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent"
            aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>

        <div class="collapse navbar-collapse" id="navbarSupportedContent">
            <ul class="navbar-nav mr-auto">
                <li class="nav-item active">
                    <a class="nav-link" href="#">Home <span class="sr-only">(current)</span></a>
                </li>

                <div class="dropdown">
                    <button class="btn btn-dark dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                      Insurance plans
                    </button>
                    <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                      <a class="dropdown-item" href="#">Life Insurance</a>
                      <a class="dropdown-item" href="#">Dental Insurance</a>
                      <a class="dropdown-item" href="#">Vision Insurance</a>
                    </div>
                  </div>
                
        
            </ul>
            <div class="mx-2">
                <button class="btn btn-success" data-toggle="modal" data-target="#loginModal">Login</button>
                <button class="btn btn-success" data-toggle="modal" data-target="#signupModal">SignUp</button>
            </div>
        </div>
    </nav>

    <!--Login Modal -->
    <div class="modal fade" id="loginModal" tabindex="-1" role="dialog" aria-labelledby="loginModalLabel"
        aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="loginModalLabel">Log in!</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <form action="/govinda.saxena-master/homepage.html" >
                        <div class="form-group">
                            <label for="exampleInputEmail1">Email address</label>
                            <p id="emailerr"></p>
                            <input type="email" class="form-control" id="exampleInputEmail1"
                                aria-describedby="emailHelp" placeholder="Enter email" required>
                            <small id="emailHelp" class="form-text text-muted">We'll never share your email with anyone
                                else.</small>
                        </div>
                        <div class="form-group">
                            <label for="exampleInputPassword1">Password</label>
                            <p id="emailerr"></p>
                            <input type="password" class="form-control" id="exampleInputPassword1"
                                placeholder="Password" required>
                        </div>

                        <button type="submit" class="btn btn-primary" onclick="loginFunction()">Login</button>
                    </form>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal"  >Close</button>
                </div>
            </div>
        </div>
    </div>



    <!-- SignUp Modal -->
    <div class="modal fade" id="signupModal" tabindex="-1" role="dialog" aria-labelledby="signupModalLabel"
        aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="signupModalLabel">Sign Up!</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                   

                    <form action="home" method="POST" id="form" class="form" onsubmit="return checkInputs()">
                        <div class="form-control">
                            <label for="username">Firstname</label>
                            <input type="text" onChange="checkfirst()" placeholder="Enter first name" id="username" name="firstName"/>
                            <i class="fas fa-check-circle"></i>
                            <i class="fas fa-exclamation-circle"></i>
                            <small>Error message</small>
                        </div>
                        <div class="form-control">
                            <label for="username">Lastname</label>
                            <input type="text" onChange="checklast()" placeholder="Enter last name" id="lastname" name="lastName" />
                            <i class="fas fa-check-circle"></i>
                            <i class="fas fa-exclamation-circle"></i>
                            <small>Error message</small>
                        </div>
                        <div class="form-control">
                            <label for="username">Email</label>
                            <input type="email" onChange="checkemail()" placeholder="Enter email" id="email" name="email" />
                            <i class="fas fa-check-circle"></i>
                            <i class="fas fa-exclamation-circle"></i>
                            <small>Error message</small>
                        </div>
                        <div class="form-control">
                            <label for="username">Password</label>
                            <input type="password" onChange="checkPassword1()" placeholder="Enter password" id="password" name="password" />
                            <i class="fas fa-check-circle"></i>
                            <i class="fas fa-exclamation-circle"></i>
                            <small>Error message</small>
                        </div>
                        <div class="form-control">
                            <label for="username">Password check</label>
                            <input type="password" onChange="checkPassword2()" placeholder="Re-Enter password" id="password2" />
                            <i class="fas fa-check-circle"></i>
                            <i class="fas fa-exclamation-circle"></i>
                            <small>Error message</small>
                        </div>

                        <div class="form-control">
                            <label for="username">Mobile Number</label>
                            <input type="text" onChange="checkmob()" placeholder="Enter mobile number" id="mobile" name="mobileNo" />
                            <i class="fas fa-check-circle"></i>
                            <i class="fas fa-exclamation-circle"></i>
                            <small>Error message</small>
                        </div>

                        <div class="form-control">
                            <label for="username">Year of Birth</label>
                            <input type="number" onChange="checkYear()" min="1920"  step="1"  placeholder="Enter Year" id="yob" name="yob" />
                            <i class="fas fa-check-circle"></i>
                            <i class="fas fa-exclamation-circle"></i>
                            <small>Error message</small>
                        </div>

                        <div class="form-control">
                            <label for="username">Address</label>
                            <input type="textarea" onChange="checkaddress()" placeholder="Enter your address" id="address" name="address" />
                            <i class="fas fa-check-circle"></i>
                            <i class="fas fa-exclamation-circle"></i>
                            <small>Error message</small>
                        </div>
                        
                        <label for="username">Gender</label><br>
                            <div class="form-check form-check-inline">
                                <input class="form-check-input" type="radio" name="gender" id="inlineRadio1" value="male" checked>
                                <label class="form-check-label" for="inlineRadio1">Male</label>
                              </div>
                              <div class="form-check form-check-inline">
                                <input class="form-check-input" type="radio" name="gender" id="inlineRadio2" value="female">
                                <label class="form-check-label" for="inlineRadio2">Female</label>
                              </div>
                              <div id="radioerr"></div>
                              <br>



                            <div class="form-control">
                                <label for="username">Annual Income </label><br>
                            <select class="form-select form-select-lg mb-3" aria-label=".form-select-lg example" id="incomelist" name="annualIncome" required>
                                <option>select</option>
                                <option value="below 2 lakh">below 2 lakh</option>
                                <option value="2-5 lakh">2-5 lakh</option>
                                <option value="5-10 lakh">5-10 lakh</option>
                                <option value="above 10 lakh">above 10 lakh</option>
                              </select>
                              </div>
                               



                        <button type="submit" class="btn btn-primary">Register</button>
                    </form>


                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>
    <br>
    <!-- <br> -->

    <div class="container">
        <div class="row">
            <div class="col-md-4">
                <div class="card mb-4 box-shadow">
                    <img class="card-img-top"
                        data-src="holder.js/100px225?theme=thumb&amp;bg=55595c&amp;fg=eceeef&amp;text=Thumbnail"
                        alt="Thumbnail [100%x225]" style="height: 225px; width: 100%; display: block;"
                        src="images\Life.jpg"
                        data-holder-rendered="true">
                    <div class="card-body">
                        <h4>Life Insurance</h4>
                        <!-- <p>Your beneficiaries could use the money to help cover essential expenses, such as paying a mortgage or college tuition for your children in case you are trouble.</p> -->
                        <div class="d-flex justify-content-between align-items-center">
                            <div class="btn-group">
                                <button type="button" class="btn btn-success">View Plans</button>
                            </div>
                            
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-4">
                <div class="card mb-4 box-shadow">
                    <img class="card-img-top"
                        data-src="holder.js/100px225?theme=thumb&amp;bg=55595c&amp;fg=eceeef&amp;text=Thumbnail"
                        alt="Thumbnail [100%x225]"
                        src="images\Dental.png"
                        data-holder-rendered="true" style="height: 225px; width: 100%; display: block;">
                    <div class="card-body">
                        <h4>Dental Insurance</h4>
    
                        <!-- <p>Most dental plans cover preventive visits at little or no additional cost. You're protected from financial risk.</p> -->

                        <div class="d-flex justify-content-between align-items-center">
                            <div class="btn-group">
                                <button type="button" class="btn btn-success">View Plans</button>
                            </div>
                            
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-4">
                <div class="card mb-4 box-shadow">
                    <img class="card-img-top"
                        data-src="holder.js/100px225?theme=thumb&amp;bg=55595c&amp;fg=eceeef&amp;text=Thumbnail"
                        alt="Thumbnail [100%x225]"
                        src="images\Vision.jpg"
                        data-holder-rendered="true" style="height: 225px; width: 100%; display: block;">
                    <div class="card-body">
                        <h4>Vision Insurance</h4>
                        
                        <div class="d-flex justify-content-between align-items-center">
                            <div class="btn-group">
                                <button type="button" class="btn btn-success">View Plans</button>
                            </div>
                            
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>


    <!--life Modal-->
    <div class="modal fade" id="lifeModal" tabindex="-1" role="dialog" aria-labelledby="lifeModalLabel"
        aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="lifeModalLabel">Enter Details</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <form>
                        <div class="form-group">
                            <label for="exampleInputEmail1">Age</label>
                            <input type="number" min="5" class="form-control" id="exampleInputlifeage1"
                                aria-describedby="emailHelp" placeholder="Enter Age">
                        </div>
                        
                        <div class="form-group">
                            <label for="exampleInputPassword1">Annual Income</label><br>
                            
                            <select class="form-select" aria-label="Default select example" id="income" name="Annual Income">
                                <option value="below than 1 lakhs"><b>below than 1 lakhs</b></option>
                                <option value="1-3 lakhs">1-3 lakhs</option>
                                <option value="3-5 lakhs">3-5 lakhs</option>
                                <option value="5-10 lakhs">5-10 lakhs</option>
                                <option value="10+ lakhs">10+ lakhs</option>
                            </select>
                              
                        </div>

                        <button type="submit" class="btn btn-primary">Proceed</button>
                    </form>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>
    <!--dental Modal-->
    <div class="modal fade" id="dentalModal" tabindex="-1" role="dialog" aria-labelledby="dentalModalLabel"
        aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="dentalModalLabel">Enter Details</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <form>
                        <div class="form-group">
                            <label for="exampleInputEmail1">Age</label>
                            <input type="number" min="5" class="form-control" id="exampleInputlifeage1"
                                aria-describedby="emailHelp" placeholder="Enter Age">
                        </div>
                        
                        <div class="form-group">
                            <label for="exampleInputPassword1">Annual Income</label><br>
                            
                            <select class="form-select" aria-label="Default select example" id="income" name="Annual Income">
                                <option value="below than 1 lakhs"><b>below than 1 lakhs</b></option>
                                <option value="1-3 lakhs">1-3 lakhs</option>
                                <option value="3-5 lakhs">3-5 lakhs</option>
                                <option value="5-10 lakhs">5-10 lakhs</option>
                                <option value="10+ lakhs">10+ lakhs</option>
                            </select>
                              
                        </div>

                        <button type="submit" class="btn btn-primary">Proceed</button>
                    </form>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>
    <!--vision Modal-->
    <div class="modal fade" id="visionModal" tabindex="-1" role="dialog" aria-labelledby="visionModalLabel"
        aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="visionModalLabel">Enter Details</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <form>
                        <div class="form-group">
                            <label for="exampleInputEmail1">Age</label>
                            <input type="number" min="5" class="form-control" id="exampleInputlifeage1"
                                aria-describedby="emailHelp" placeholder="Enter Age">
                        </div>
                        
                        <div class="form-group">
                            <label for="exampleInputPassword1">Annual Income</label><br>
                            
                            <select class="form-select" aria-label="Default select example" id="income" name="Annual Income">
                                <option value="below than 1 lakhs"><b>below than 1 lakhs</b></option>
                                <option value="1-3 lakhs">1-3 lakhs</option>
                                <option value="3-5 lakhs">3-5 lakhs</option>
                                <option value="5-10 lakhs">5-10 lakhs</option>
                                <option value="10+ lakhs">10+ lakhs</option>
                            </select>
                              
                        </div>

                        <button type="submit" class="btn btn-primary">Proceed</button>
                    </form>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>
    <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js"
        integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN"
        crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js"
        integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q"
        crossorigin="anonymous"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js"
        integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl"
        crossorigin="anonymous"></script>


<!-- <script src="LoginModal.js"></script> -->

<script>

const form = document.getElementById('form');
const username = document.getElementById('username');
const email = document.getElementById('email');
const password = document.getElementById('password');
const password2 = document.getElementById('password2');


const lastname = document.getElementById('lastname');

const mobile=document.getElementById('mobile');

const yob=document.getElementById('yob');

const address=document.getElementById('address');


//for particular field check on TAB

function checkfirst(){
	const usernameValue = username.value.trim();
	if(usernameValue === '') {
		setErrorFor(username, 'Username cannot be blank');
      
	}
	else if(usernameValue.length< 3)
	{
		setErrorFor(username, 'name not valid');
	} else {
		setSuccessFor(username);
        
	}
}

function checklast()
{
	const lastnameValue=lastname.value.trim();
if(lastnameValue === '') {
    setErrorFor(lastname, 'Lastname cannot be blank');
    
} 
else if(lastnameValue.length< 3)
	{
		setErrorFor(lastname, 'lastname not valid');
	} else {
    setSuccessFor(lastname);
    
}
}

function checkemail(){
	const emailValue = email.value.trim();
	if(emailValue === '') {
		setErrorFor(email, 'Email cannot be blank');
       
	} else if (!isEmail(emailValue)) {
		setErrorFor(email, 'Not a valid email');
       
	} else {
		setSuccessFor(email);
	}
}

function checkPassword1(){
	const passwordValue = password.value.trim();
	if(passwordValue === '') {
		setErrorFor(password, 'Password cannot be blank');}
      else if(passwordValue.length< 5)
		{
			setErrorFor(password, 'Password not valid');
		}
	 else {
		setSuccessFor(password);
        
	}
}
function checkPassword2(){
	const passwordValue = password.value.trim();
	const password2Value = password2.value.trim();
	if(password2Value === '') {
		setErrorFor(password2, 'Password cannot be blank');
        
	} else if(passwordValue !== password2Value) {
		setErrorFor(password2, 'Passwords does not match');
        
	} else{
		setSuccessFor(password2);
       
	}
}

function checkmob(){
	const mobilevalue=mobile.value.trim();

if (/^\d{10}$/.test(mobilevalue)) {
    setSuccessFor(mobile);
   
}
else{
	setErrorFor(mobile, 'Please enter valid number!'); 
}
}

function checkYear(){
	const yobvalue=yob.value.trim();

if(yobvalue ==='' ){
	setErrorFor(yob, 'year cannot be blank');
   
}
else if(yobvalue<1920 || yobvalue> (new Date().getFullYear())-5)
{
	setErrorFor(yob, 'Enter valid Year');
   
}
else{
	setSuccessFor(yob);
    
}
}

function checkaddress(){
	const addressValue=address.value.trim();
if(addressValue === '') {
	setErrorFor(address, 'Address cannot be blank');
	
}
else if(addressValue.length< 5)
	{
		setErrorFor(address, 'address not valid');
	}  else {
	setSuccessFor(address);
}
}

//for submitting the form all checks will be there

function checkInputs() {
	// trim to remove the whitespaces

    var flag=0;
	const usernameValue = username.value.trim();
	const emailValue = email.value.trim();
	const passwordValue = password.value.trim();
	const password2Value = password2.value.trim();

const lastnameValue=lastname.value.trim();
if(lastnameValue === '') {
    setErrorFor(lastname, 'Lastname cannot be blank');
    flag=0;
} else {
    setSuccessFor(lastname);
    flag=1;
}


// const gender =document.getElementById('inlineRadio1');
// if(document.getElementById('inlineRadio1').checked || document.getElementById('inlineRadio2').checked) {
// 	flag=1;
//   }
// else {
// 	document.getElementById("radioerr").innerHTML = "Please select one !" ;
//         document.getElementById("err").style.color = "red";
// 		flag=0;
// }

	
	if(usernameValue === '') {
		setErrorFor(username, 'Username cannot be blank');
        flag=0;
	} else {
		setSuccessFor(username);
        flag=1;
	}

	
	if(emailValue === '') {
		setErrorFor(email, 'Email cannot be blank');
        flag= 0;
	} else if (!isEmail(emailValue)) {
		setErrorFor(email, 'Not a valid email');
        flag= 0;
	} else {
		setSuccessFor(email);
        flag= 1;
	}
	
	if(passwordValue === '') {
		setErrorFor(password, 'Password cannot be blank');
        flag= 0;
	} else {
		setSuccessFor(password);
        flag= 1;
	}
	
	
	if(password2Value === '') {
		setErrorFor(password2, 'Password cannot be blank');
        flag= 0;
	} else if(passwordValue !== password2Value) {
		setErrorFor(password2, 'Passwords does not match');
        flag= 0;
	} else{
		setSuccessFor(password2);
        flag= 1;
	}


	const mobilevalue=mobile.value.trim();

if (/^\d{10}$/.test(mobilevalue)) {
    setSuccessFor(mobile);
    flag=1;
}
else{
	setErrorFor(mobile, 'Please enter valid number!');
    flag=0;
}




const yobvalue=yob.value.trim();

if(yobvalue ==='' ){
	setErrorFor(yob, 'year cannot be blank');
    flag=0;
}
else if(yobvalue<1960 || yobvalue> (new Date().getFullYear())-5)
{
	setErrorFor(yob, 'Enter valid Year');
    flag=0;
}
else{
	setSuccessFor(yob);
    flag=1;
}


const addressValue=address.value.trim();
if(addressValue === '') {
	setErrorFor(address, 'Address cannot be blank');
	flag=0;
} else {
	setSuccessFor(address);
	flag=1;
}



return Boolean(flag);


}

function setErrorFor(input, message) {
	const formControl = input.parentElement;
	const small = formControl.querySelector('small');
	formControl.className = 'form-control error';
	small.innerText = message;
}

function setSuccessFor(input) {
	const formControl = input.parentElement;
	formControl.className = 'form-control success';
}
	
function isEmail(email) {
	return /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/.test(email);
}


</script>








        <!-- <script>

const form = document.getElementById('form');
const username = document.getElementById('username');
const email = document.getElementById('email');
const password = document.getElementById('password');
const password2 = document.getElementById('password2');


const lastname = document.getElementById('lastname');

const mobile=document.getElementById('mobile');

const yob=document.getElementById('yob');

const address=document.getElementById('address');

function checkInputs() {
	// trim to remove the whitespaces
    var flag=0;
	const usernameValue = username.value.trim();
	const emailValue = email.value.trim();
	const passwordValue = password.value.trim();
	const password2Value = password2.value.trim();

const lastnameValue=lastname.value.trim();
if(lastnameValue === '') {
    setErrorFor(lastname, 'Lastname cannot be blank');
    flag=0;
} else {
    setSuccessFor(lastname);
    flag=1;
}








// const gender =document.getElementById('inlineRadio1');
// if(document.getElementById('inlineRadio1').checked || document.getElementById('inlineRadio2').checked) {
// 	flag=1;
//   }
// else {
// 	document.getElementById("radioerr").innerHTML = "Please select one !" ;
//         document.getElementById("err").style.color = "red";
// 		flag=0;
// }






	
	if(usernameValue === '') {
		setErrorFor(username, 'Username cannot be blank');
        flag=0;
	} else {
		setSuccessFor(username);
        flag=1;
	}


	
	if(emailValue === '') {
		setErrorFor(email, 'Email cannot be blank');
        flag= 0;
	} else if (!isEmail(emailValue)) {
		setErrorFor(email, 'Not a valid email');
        flag= 0;
	} else {
		setSuccessFor(email);
        flag= 1;
	}
	
	if(passwordValue === '') {
		setErrorFor(password, 'Password cannot be blank');
        flag= 0;
	} else {
		setSuccessFor(password);
        flag= 1;
	}
	
	if(password2Value === '') {
		setErrorFor(password2, 'Password cannot be blank');
        flag= 0;
	} else if(passwordValue !== password2Value) {
		setErrorFor(password2, 'Passwords does not match');
        flag= 0;
	} else{
		setSuccessFor(password2);
        flag= 1;
	}


	const mobilevalue=mobile.value.trim();

if (/^\d{10}$/.test(mobilevalue)) {
    setSuccessFor(mobile);
    flag=1;
}
else{
	setErrorFor(mobile, 'Please enter valid number!');
    flag=0;
}




const yobvalue=yob.value.trim();

if(yobvalue ==='' ){
	setErrorFor(yob, 'year cannot be blank');
    flag=0;
}
else if(yobvalue<1960 || yobvalue> (new Date().getFullYear())-5)
{
	setErrorFor(yob, 'Enter valid Year');
    flag=0;
}
else{
	setSuccessFor(yob);
    flag=1;
}


const addressValue=address.value.trim();
if(addressValue === '') {
	setErrorFor(address, 'Address cannot be blank');
	flag=0;
} else {
	setSuccessFor(address);
	flag=1;
}



return Boolean(flag);


}

function setErrorFor(input, message) {
	const formControl = input.parentElement;
	const small = formControl.querySelector('small');
	formControl.className = 'form-control error';
	small.innerText = message;
}

function setSuccessFor(input) {
	const formControl = input.parentElement;
	formControl.className = 'form-control success';
}
	
function isEmail(email) {
	return /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/.test(email);
} -->




<!-- // const ff=0;

// form.addEventListener('submit', e => {
// 	e.preventDefault();
	
// 	checkInputs();
// 	if (ff===1)
// 	{
// 		(this).submit();
// 	}
// }); -->







<!-- 
        // </script> -->




<!-- Form validation code JavaScript -->

<!-- <script>
    function loginFunction() {
      // Get the value of the input field with id="numb"
      let x = document.getElementById("numb").value;
      // If x is Not a Number or less than one or greater than 10
      let text;
      if (isNaN(x) || x < 1 || x > 10) {
        text = "Input not valid";
      } else {
        text = "Input OK";
      }
      document.getElementById("demo").innerHTML = text;
    }
    </script> -->




</body>

</html>