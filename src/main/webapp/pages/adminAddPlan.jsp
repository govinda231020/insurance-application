
<!doctype html>
<html lang="en">

<head>
<link rel="stylesheet"
	href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css"
	integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm"
	crossorigin="anonymous">

<!-- Required meta tags -->
<meta charset="utf-8">
<meta name="viewport"
	content="width=device-width, initial-scale=1, shrink-to-fit=no">
<script src="https://kit.fontawesome.com/e38070cbc2.js"
	crossorigin="anonymous"></script>



<script
	src="https://cdnjs.cloudflare.com/ajax/libs/crypto-js/3.1.2/rollups/aes.js"></script>
<script
	src="http://crypto-js.googlecode.com/svn/tags/3.1.2/build/rollups/md5.js"></script>
<!-- Bootstrap CSS -->
<link rel="stylesheet"
	href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css"
	integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm"
	crossorigin="anonymous">
<title>Life Insurance</title>


<style>
.form {
	padding: 5px 30px;
}

.form-control {
	margin-bottom: 5px;
	padding-bottom: 17px;
	position: relative;
	border-style: none;
}

.form-control label {
	display: inline-block;
	margin-bottom: 5px;
}

.form-control input {
	border: 2px solid #f0f0f0;
	border-radius: 4px;
	display: block;
	font-family: inherit;
	font-size: 14px;
	padding: 10px;
	width: 100%;
}

.form-control input:focus {
	outline: 0;
	border-color: #777;
}

.form-control.success input {
	border-color: #2ecc71;
}

.form-control.error input {
	border-color: #e74c3c;
}

.form-control i {
	visibility: hidden;
	position: absolute;
	top: 50px;
	right: 20px;
}

.form-control.success i.fa-check-circle {
	color: #2ecc71;
	visibility: visible;
}

.form-control.error i.fa-exclamation-circle {
	color: #e74c3c;
	visibility: visible;
}

.form-control small {
	color: #e74c3c;
	position: absolute;
	bottom: 0;
	left: 0;
	visibility: hidden;
}

.form-control.error small {
	visibility: visible;
}

.form button {
	background-color: #44ad52;
	border: 2px solid #55ad44;
	border-radius: 4px;
	color: #fff;
	display: block;
	font-family: inherit;
	font-size: 16px;
	padding: 10px;
	margin-top: 20px;
	width: 100%;
}

/* for dropdown in navbar */
.dropdown:hover .dropdown-menu {
	display: block;
}

.form-control span {
	color: red;
	font-weight: bold;
}

#star {
	color: red;
	font-weight: bold;
}
</style>

<style>
#navname {
	color: white;
	float: Right;
	padding-right: 20px;
	font-size: 20px;
}
</style>
</head>

<body>
	<nav class="navbar navbar-expand-lg navbar-dark bg-dark">
		<a class="navbar-brand" href="#">Insurance App</a>
		<button class="navbar-toggler" type="button" data-toggle="collapse"
			data-target="#navbarSupportedContent"
			aria-controls="navbarSupportedContent" aria-expanded="false"
			aria-label="Toggle navigation">
			<span class="navbar-toggler-icon"></span>
		</button>

		<div class="collapse navbar-collapse" id="navbarSupportedContent">
			<ul class="navbar-nav mr-auto">
				<li class="nav-item active"><a class="nav-link"
					href="/admin/homepage">Home</a></li>
				<li class="nav-item active"><a class="nav-link" name="allplan"
					value="planvalue" href="/admin/allplans">All Plans</a></li>
			</ul>
			<div id="navname">Hi Admin</div>

		</div>

		<div class="mx-2">
			<!-- <button class="btn btn-success">Logout</button> -->
			<div class="dropdown">
				<a href="/logout">
					<button class="btn btn-success">Logout</button>
				</a>

			</div>


		</div>

	</nav>

	<br>
		<%
	String msss = (String) session.getAttribute("planpresent");
	%>

	<%
	if (msss != null) {
	%>
	<div id="updatem">
		<h5 style="color: red; text-align: center"><%=msss%></h5>
	</div>
	<%
	}
	session.removeAttribute("planpresent");
	%>
	
	<div class="container">
		<div class="row">
			<div
				class="col-lg-6 col-md-6 col-sm-6 container justify-content-center card">
				<h2 class="text-center">Add Details of Plan</h2>

				<div class="card-body">
					<form action="/admin/allplansAfter" method="POST" id="form"
						class="form" onsubmit="return checkInputs()">



						<!-- ******************************************************************************************************************** -->


						<label for="username">Insurance Type</label><span
							style="color: red; font-weight: bold;"> *</span> <br>
						<div class="form-check form-check-inline">
							<input class="form-check-input" type="radio" name="insuranceType"
								id="inlineRadio1" value="Life" checked> <label
								class="form-check-label" for="inlineRadio1">Life</label>
						</div>
						<div class="form-check form-check-inline">
							<input class="form-check-input" type="radio" name="insuranceType"
								id="inlineRadio2" value="Dental"> <label
								class="form-check-label" for="inlineRadio2">Dental</label>
						</div>
						<div class="form-check form-check-inline">
							<input class="form-check-input" type="radio" name="insuranceType"
								id="inlineRadio3" value="Vision"> <label
								class="form-check-label" for="inlineRadio2">Vision</label>
						</div>
						<br>
						<br>


						<div class="form-control">

							<label for="username">Insurance Name</label> <span> *</span><input
								type="text" onChange="checkComp()" placeholder="Enter name"
								id="compname" name="insuranceCompName" /> <i
								class="fas fa-check-circle"></i> <i
								class="fas fa-exclamation-circle"></i> <small>Error
								message</small>
						</div>
						<div class="form-control">
							<label for="username">Plan Details</label><span> *</span> <input
								type="text" onchange="checkplandet()"
								placeholder="Enter details" id="plandetails"
								name="insPlanDetail" /> <i class="fas fa-check-circle"></i> <i
								class="fas fa-exclamation-circle"></i> <small>Error
								message</small>
						</div>
						<div class="form-control">
							<label for="username">Base Price</label><span> *</span> <input
								type="number" onchange="checkbaseprice()"
								placeholder="Enter base price" id="baseprice" name="basePrice" />
							<i class="fas fa-check-circle"></i> <i
								class="fas fa-exclamation-circle"></i> <small>Error
								message</small>
						</div>
						<div class="form-control">
							<label for="username">Minimum Age</label><span> *</span> <input
								type="number" onchange="checkminage()"
								placeholder="Enter min age" id="minage" name="minAge" /> <i
								class="fas fa-check-circle"></i> <i
								class="fas fa-exclamation-circle"></i> <small>Error
								message</small>
						</div>

						<div class="form-control">
							<label for="username">Min Annual Income</label><span> *</span> <input
								type="number" placeholder="Enter min income" id="minincome"
								name="minAnnualIncome" onchange="checkminincome()" /> <i
								class="fas fa-check-circle"></i> <i
								class="fas fa-exclamation-circle"></i> <small>Error
								message</small>
						</div>

						<div class="form-control">
							<label for="username">Cover Amount</label><span> *</span> <input
								type="number" onchange="checkcoverageAmount()"
								placeholder="Enter Cover amount" id="coverageAmount"
								name="coverageAmount" /> <i class="fas fa-check-circle"></i> <i
								class="fas fa-exclamation-circle"></i> <small>Error
								message</small>
						</div>

						<div class="form-control">
							<label for="username">Maximum Age</label><span> *</span> <input
								type="number" onchange="checkmaxcover()"
								placeholder="Enter max age" id="maxcover" name="coverageAge" />
							<i class="fas fa-check-circle"></i> <i
								class="fas fa-exclamation-circle"></i> <small>Error
								message</small>
						</div>

						<button type="submit" class="btn btn-primary">Add Plan</button>
					</form>

				</div>
			</div>
		</div>
	</div>


	<script>
		const form = document.getElementById('form');

		//for particular field check on TAB

		//Company check
		const compname = document.getElementById('compname');
		function checkComp() {
			const compnameValue = compname.value.trim();
			if (compnameValue === '') {
				setErrorFor(compname, 'Insurance name cannot be blank');

			} else if (compnameValue.length < 3) {
				setErrorFor(compname, 'Not valid');
			} else {
				setSuccessFor(compname);

			}
		}

		//Plan Details check
		const plandetails = document.getElementById('plandetails');
		function checkplandet() {
			const plandetailsValue = plandetails.value.trim();
			if (plandetailsValue === '') {
				setErrorFor(plandetails, 'Details cannot be blank');

			} else if (plandetailsValue.length < 3) {
				setErrorFor(plandetails, 'Not valid');
			} else {
				setSuccessFor(plandetails);

			}
		}

		//Base price check
		const baseprice = document.getElementById('baseprice');
		function checkbaseprice() {
			const basepriceVakue = baseprice.value.trim();

			if (basepriceVakue === '') {
				setErrorFor(baseprice, 'Price cannnot be blank');
			} else if (basepriceVakue < 1) {
				setErrorFor(baseprice, 'Price cannnot be negative');
			}

			else {
				setSuccessFor(baseprice);

			}
		}

		//Minimum age check
		const minage = document.getElementById('minage');
		function checkminage() {
			const minageValue = minage.value.trim();

			if (minageValue === '') {
				setErrorFor(minage, 'Age cannot be blank');
			} else if (minageValue < 0) {
				setErrorFor(minage, ' Cannnot be negative');

			}

			else {
				setSuccessFor(minage);

			}
		}

		//minimum annual income check

		const minincome = document.getElementById('minincome');
		function checkminincome() {
			const minincomeVakue = minincome.value.trim();

			if (minincomeVakue === '') {
				setErrorFor(minincome, 'Income cannot be blank');
			} else if (minincomeVakue < 0) {
				setErrorFor(minincome, ' Cannnot be negative');

			}

			else {
				setSuccessFor(minincome);

			}
		}

		//cover amaount

		const coverageAmount = document.getElementById('coverageAmount');
		function checkcoverageAmount() {
			const coverageAmountVakue = coverageAmount.value.trim();

			if (coverageAmountVakue === '') {
				setErrorFor(coverageAmount, 'Cover amount cannot be blank');
			} else if (coverageAmountVakue < 0) {
				setErrorFor(coverageAmount, ' Cannnot be negative');

			}

			else {
				setSuccessFor(coverageAmount);

			}
		}

		//cover till age

		const maxcover = document.getElementById('maxcover');
		function checkmaxcover() {
			const maxcoverVakue = maxcover.value.trim();

			if (maxcoverVakue === '') {
				setErrorFor(maxcover, 'Max age cannot be blank');
			} else if (maxcoverVakue < 0) {
				setErrorFor(maxcover, ' Cannnot be negative');

			}

			else {
				setSuccessFor(maxcover);

			}
		}

		//************************************************************************

		//for submitting the form all checks will be there

		function checkInputs() {
			// trim to remove the whitespaces

			var flag = 0;

			// //Company check

			const compnameValue = compname.value.trim();
			if (compnameValue === '') {
				setErrorFor(compname, 'Insurance name cannot be blank');
				flag = 0;
			} else if (compnameValue.length < 3) {
				setErrorFor(compname, 'name not valid');
				flag = 0;
			} else {
				setSuccessFor(compname);

			}

			// //Plan Details check

			const plandetailsValue = plandetails.value.trim();
			if (plandetailsValue === '') {
				setErrorFor(plandetails, 'Details cannot be blank');
				flag = 0;
			} else if (plandetailsValue.length < 3) {
				setErrorFor(plandetails, 'name not valid');
				flag = 0;
			} else {
				setSuccessFor(plandetails);
				flag = 1;

			}

			// //Base price check

			const basepriceVakue = baseprice.value.trim();

			if (basepriceVakue === '') {
				setErrorFor(baseprice, 'Price cannot be blank');
				flag = 0;
			} else if (basepriceVakue < 0) {
				setErrorFor(baseprice, ' Cannnot be negative');
				flag = 0;
			}

			else {
				setSuccessFor(baseprice);
				flag = 1;
			}

			// //Minimum age check

			const minageValue = minage.value.trim();

			if (minageValue === '') {
				setErrorFor(minage, 'Age cannot be blank');
				flag = 0;
			} else if (minageValue < 0) {
				setErrorFor(minage, ' Cannnot be negative');
				flag = 0;
			}

			else {
				setSuccessFor(minage);
				flag = 1;
			}

			// //minimum annual income check

			const minincomeVakue = minincome.value.trim();

			if (minincomeVakue === '') {
				setErrorFor(minincome, 'Income cannot be blank');
				flag = 0;
			} else if (minincomeVakue < 0) {
				setErrorFor(minincome, ' Cannnot be negative');
				flag = 0;
			}

			else {
				setSuccessFor(minincome);
				flag = 1;
			}

			// //cover amaount

			const coverageAmountVakue = coverageAmount.value.trim();

			if (coverageAmountVakue === '') {
				setErrorFor(coverageAmount, 'Cover amount cannot be blank');
				flag = 0;
			} else if (coverageAmountVakue < 0) {
				setErrorFor(coverageAmount, ' Cannnot be negative');
				flag = 0;
			}

			else {
				setSuccessFor(coverageAmount);
				flag = 1;
			}

			// //cover till age

			const maxcoverVakue = maxcover.value.trim();

			if (maxcoverVakue === '') {
				setErrorFor(maxcover, 'Max age cannot be blank');
				flag = 0;
			} else if (maxcoverVakue < 0) {
				setErrorFor(maxcover, ' Cannnot be negative');
				flag = 0;
			} else {
				setSuccessFor(maxcover);
				//flag = 1;
			}

			return Boolean(flag);

		}

		function setErrorFor(input, message) {
			const formControl = input.parentElement;
			const small = formControl.querySelector('small');
			formControl.className = 'form-control error';
			small.innerText = message;
		}

		function setSuccessFor(input) {
			const formControl = input.parentElement;
			formControl.className = 'form-control success';
		}
	</script>


	<!-- Optional JavaScript -->
	<!-- jQuery first, then Popper.js, then Bootstrap JS -->
	<script src="https://code.jquery.com/jquery-3.2.1.slim.min.js"
		integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN"
		crossorigin="anonymous"></script>
	<script
		src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js"
		integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q"
		crossorigin="anonymous"></script>
	<script
		src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js"
		integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl"
		crossorigin="anonymous"></script>
</body>

</html>