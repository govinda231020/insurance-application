<%@page import="com.impetus.insuranceapp.model.InsurancePlan"%>
<%@page import="com.impetus.insuranceapp.repository.InsurRepo"%>
<%@page import="com.impetus.insuranceapp.model.UserProfile"%>

<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!doctype html>
<html lang="en">

<head>
<link rel="styleshe et"
	href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css"
	integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm"
	crossorigin="anonymous">

<!-- Required meta tags -->
<meta charset="utf-8">
<meta name="viewport"
	content="width=device-width, initial-scale=1, shrink-to-fit=no">
<script src="https://kit.fontawesome.com/e38070cbc2.js"
	crossorigin="anonymous"></script>
<!-- Bootstrap CSS -->
<link rel="stylesheet"
	href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css"
	integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm"
	crossorigin="anonymous">
<style>
.form {
	padding: 5px 40px;
}

.form-control {
	margin-bottom: 5px;
	padding-bottom: 16px;
	position: relative;
	border-style: none;
}

.form-control label {
	display: inline-block;
	margin-bottom: 5px;
}

.form-control input {
	border: 2px solid #f0f0f0;
	border-radius: 4px;
	display: block;
	font-family: inherit;
	font-size: 14px;
	padding: 10px;
	width: 100%;
}

.form-control input:focus {
	outline: 0;
	border-color: #777;
}

.form-control.success input {
	border-color: #2ecc71;
}

.form-control.error input {
	border-color: #e74c3c;
}

.form-control i {
	visibility: hidden;
	position: absolute;
	top: 50px;
	right: 20px;
}

.form-control.success i.fa-check-circle {
	color: #2ecc71;
	visibility: visible;
}

.form-control.error i.fa-exclamation-circle {
	color: #e74c3c;
	visibility: visible;
}

.form-control small {
	color: #e74c3c;
	position: absolute;
	bottom: 0;
	left: 0;
	visibility: hidden;
}

.form-control.error small {
	visibility: visible;
}

.form button {
	background-color: #8e44ad;
	border: 2px solid #8e44ad;
	border-radius: 4px;
	color: #fff;
	display: block;
	font-family: inherit;
	font-size: 16px;
	padding: 10px;
	margin-top: 20px;
	width: 100%;
}

/* for dropdown in navbar */
.dropdown:hover .dropdown-menu {
	display: block;
}

.form-control span {
	color: red;
	font-weight: bold;
}

#star {
	color: red;
	font-weight: bold;
}
</style>
<style>
/* for dropdown in navbar */
.dropdown:hover .dropdown-menu {
	display: block;
	margin-top: 0;
}

#navname {
	color: white;
	float: Right;
	padding-right: 20px;
	font-size: 20px;
}
</style>

<title>Fill Beneficiary Details</title>
</head>

<body>

	<nav class="navbar navbar-expand-lg navbar-dark bg-dark">
		<a class="navbar-brand" href="#">Insurance App</a>
		<button class="navbar-toggler" type="button" data-toggle="collapse"
			data-target="#navbarSupportedContent"
			aria-controls="navbarSupportedContent" aria-expanded="false"
			aria-label="Toggle navigation">
			<span class="navbar-toggler-icon"></span>
		</button>

		<div class="collapse navbar-collapse" id="navbarSupportedContent">
			<ul class="navbar-nav mr-auto">
				<li class="nav-item active"><a class="nav-link"
					href="/user/homered">Home<span class="sr-only">(current)</span></a></li>
				<div class="dropdown">
					<button class="btn btn-dark dropdown-toggle" type="button"
						id="dropdownMenuButton" data-toggle="dropdown"
						aria-haspopup="true" aria-expanded="false">Insurance
						Plans</button>
					<div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
						<a class="dropdown-item" href="/user/lifeplans">Life Insurance</a>
						<a class="dropdown-item" href="/user/dentalplans">Dental
							Insurance</a> <a class="dropdown-item" href="/user/visionplans">Vision
							Insurance</a>
					</div>
				</div>
			</ul>
			<div id="navname">Hi ${yourname}</div>

		</div>

		<div class="mx-2">
			<!-- <button class="btn btn-success">Logout</button> -->
			<div class="dropdown">
				<button class="btn btn-success dropdown-toggle" type="button"
					data-toggle="dropdown">
					Actions <span class="caret"></span>
				</button>
				<ul class="dropdown-menu">
					<div class="dropbt">
						<li><a href="/user/myprofile" style="color: black"><b>My
									Profile</b></a></li>
						<li><a href="/user/myPolicies" style="color: black"><b>My
									Policies</b></a></li>
						<li><a href="/user/myfamily" style="color: black"><b>My
									Family</b></a></li>
						<li><a href="/user/contact" style="color: black"><b>Contact
									Us</b></a></li>
						<li><a href="/logout" style="color: black"><b>Logout</b></a></li>
					</div>
				</ul>
			</div>

			<!-- <button class="btn btn-danger" data-toggle="modal" data-target="#signupModal">SignUp</button> -->
		</div>

	</nav>
	<br>
	<% String usergen=(String)request.getAttribute("usergender");
	UserProfile usp=(UserProfile)request.getAttribute("userpro");
	%>
	
	<h2 class="text-center">Enter Details</h2>
	
	<form action="/user/getcustompriceind" method="POST" id="form"
						class="form" onsubmit="return checkInputs()">
	
	<div class="container">
					<div class="row">
						<div
							class="col-lg-6 col-md-6 col-sm-6 container justify-content-center card">
							<div class="card-body">



								<%
								Integer myear = (Integer) request.getAttribute("udet") - 1;
								%>

								<div class="form-control">
									<label for="years"><h5>Select Number Of Years For
											Policy</h5></label><span> *</span><br> <input type="number"
										onChange="checkYear()" placeholder="Enter years" id="years"
										step="1" min="1" name="years" /> <small>Error
										message</small>
								</div>



							</div>
						</div>
					</div>
				</div>



				<br>
	
	
	

	<div class="container">
		<div class="row">
			<div class="col-lg-6 col-md-6 col-sm-6 container justify-content-center card">
			<div class="card-body">

					
						 <h5>Beneficiary Details</h5>
                    <div class="form-control">
                        <label for="relation">Relation</label><span> *</span><br> <select
                            class="form-select form-select-lg mb-3" aria-label=".form-select-lg example" id="relation"
                            name="benRelation" onchange="checkrelation()" >
                            <option selected disabled value="">Select Relation</option>
                            <option value="son">Son</option>
                            <option value="daughter">Daughter</option>
                            <option value="father">Father</option>
                            <option value="mother">Mother</option>
                            <option value="husband">Husband</option>
                            <option value="wife">Wife</option>

                            <option value="brother">Brother</option>
                            <option value="sister">Sister</option>
                            <option value="yourself">Yourself</option>
                            <option value="other">Other</option>
                        </select>
                        <div id="errm" style="color:#e74c3c;font-size:0.8em"></div>
                    </div>
						
						
						 <label for="username">Gender</label> <span id="star"> *</span><br>
                    <div class="form-check form-check-inline">
                        <input class="form-check-input" type="radio" name="gender" id="inlineRadio1" value="male"
                            > <label class="form-check-label" for="inlineRadio1">Male</label>
                    </div>
                    <div class="form-check form-check-inline">
                        <input class="form-check-input" type="radio" name="gender" id="inlineRadio2" value="female">
                        <label class="form-check-label" for="inlineRadio2">Female</label>
                    </div>
<!--                     <div class="form-check form-check-inline"> -->
<!--                         <input class="form-check-input" type="radio" name="gender" id="inlineRadio3" value="other"> -->
<!--                         <label class="form-check-label" for="inlineRadio2">Other</label> -->
<!--                     </div> -->
                    <div id="radioerr" style="color:#e74c3c;font-size:0.8em;"></div>
                   <br>
						
						
						<div class="form-control">
							<label for="relfullname">Full Name</label><span> *</span><input
								type="text" onChange="checkfirst()"
								placeholder="Enter full name" id="relfullname" name="benName" />
							<i class="fas fa-check-circle"></i> <i
								class="fas fa-exclamation-circle"></i> <small>Error
								message</small>
						</div>

						<div class="form-control">
							<label for="relemail">Email</label> <span> *</span><input
								type="email" onChange="checkemail()" placeholder="Enter email"
								id="relemail" name="emailAddress" /> <i
								class="fas fa-check-circle"></i> <i
								class="fas fa-exclamation-circle"></i> <small>Error
								message</small>
						</div>

						<div class="form-control">
							<label for="benMobNo">Mobile Number</label> <span> *</span><input
								type="text" onChange="checkmob()"
								placeholder="Enter mobile number" id="relmobile" name="benMobNo" />
							<i class="fas fa-check-circle"></i> <i
								class="fas fa-exclamation-circle"></i> <small>Error
								message</small>
						</div>


						


						<!-- 						<div class="form-control"> -->
						<!-- 							<label for="years"><h5>Select Number Of Years For Policy</h5></label><span> *</span><br>  -->
						<!-- 							<select -->
						<!-- 								class="form-select form-select-lg mb-3" -->
						<!-- 								aria-label=".form-select-lg example" id="years" -->
						<!-- 								name="years" required> -->
						<!-- 								<option>select</option> -->
						<!-- 								<option value="1">1 Years</option> -->
						<!-- 								<option value="2">2 Years</option> -->
						<!-- 								<option value="5">5 Years</option> -->
						<!-- 								<option value="10">10 Years</option> -->
						<!-- 								<option value="15">15 Years</option> -->
						<!-- 							</select> -->
						<!-- 						</div> -->

						

						<div id="cannotbuy"></div>
						<%
						String mss = (String) request.getAttribute("notvalidmsg");
						%>

						<%
						if (mss != null) {
						%>
						<div id="updatem">
							<h6 style="color: red; text-align: center"><%=mss%></h6>
						</div>
						<%
						}
						%>

						<!-- 1.Diabetes 2.Heart Ailments 3.Cataract 4.Tooth Decay 5.Covid-19 6.Cancer -->
						<button type="submit" class="btn btn-primary">Get Price</button>
					</form>

				</div>
			</div>
		</div>
	</div>


	<script>
		const form = document.getElementById('form');
		const username = document.getElementById('relfullname');
		const email = document.getElementById('relemail');
		//const password = document.getElementById('password');
		//const password2 = document.getElementById('password2');

		//const lastname = document.getElementById('lastname');

		const mobile = document.getElementById('relmobile');

		const yob = document.getElementById('years');

		//const address = document.getElementById('address');

		//for particular field check on TAB
		
		
		function checkrelation() {
            var option = document.getElementById("relation").value;


            if(option == "son")
                   {
            	document.getElementById("relfullname").value = "";
             	  document.getElementById("relemail").value = "";
             	 document.getElementById("relmobile").value = "";
             	 
           	  document.getElementById("relfullname").readOnly = false;
       		  document.getElementById("relemail").readOnly = false;
         		  document.getElementById("relmobile").readOnly = false;
                        
                    //document.getElementById("errm").innerHTML = "Hiiiiiiiiiiii Hiiiiiiii";
                        document.getElementById("inlineRadio1").checked = true;
                        document.getElementById("inlineRadio1").disabled = false;
                        document.getElementById("inlineRadio2").disabled = true;
                        document.getElementById("inlineRadio3").disabled = true;
                   }
                        

                if(option == "daughter")
                  {
                	document.getElementById("relfullname").value = "";
               	  document.getElementById("relemail").value = "";
               	 document.getElementById("relmobile").value = "";
               	 
               	document.getElementById("relfullname").readOnly = false;
         		  document.getElementById("relemail").readOnly = false;
           		  document.getElementById("relmobile").readOnly = false;
                        //document.getElementById("enroll1").style.display="none";
                        
                        document.getElementById("inlineRadio2").checked = true;
                        document.getElementById("inlineRadio1").disabled = true;
                        document.getElementById("inlineRadio2").disabled = false;
                        document.getElementById("inlineRadio3").disabled = true;
                    }
                if(option == "father")
                  {
                	document.getElementById("relfullname").value = "";
               	  document.getElementById("relemail").value = "";
               	 document.getElementById("relmobile").value = "";
               	 
               	document.getElementById("relfullname").readOnly = false;
       		  document.getElementById("relemail").readOnly = false;
         		  document.getElementById("relmobile").readOnly = false;
         		  
                        document.getElementById("inlineRadio1").checked = true;
                        document.getElementById("inlineRadio1").disabled = false;
                        document.getElementById("inlineRadio2").disabled = true;
                        document.getElementById("inlineRadio3").disabled = true;
                  }

                  if(option == "mother")
                  {
                	  document.getElementById("relfullname").value = "";
                 	  document.getElementById("relemail").value = "";
                 	 document.getElementById("relmobile").value = "";
                 	 
                 	document.getElementById("relfullname").readOnly = false;
           		  document.getElementById("relemail").readOnly = false;
             		  document.getElementById("relmobile").readOnly = false;
             		  
                        document.getElementById("inlineRadio2").checked = true;
                        document.getElementById("inlineRadio1").disabled = true;
                        document.getElementById("inlineRadio2").disabled = false;
                        document.getElementById("inlineRadio3").disabled = true;
                  }


                  if(option == "husband")
                  {
                	  document.getElementById("relfullname").value = "";
                 	  document.getElementById("relemail").value = "";
                 	 document.getElementById("relmobile").value = "";
                 	 
                 	document.getElementById("relfullname").readOnly = false;
           		  document.getElementById("relemail").readOnly = false;
             		  document.getElementById("relmobile").readOnly = false;
             		  
                 	 
                        document.getElementById("inlineRadio1").checked = true;
                        document.getElementById("inlineRadio1").disabled = false;
                        document.getElementById("inlineRadio2").disabled = true;
                        document.getElementById("inlineRadio3").disabled = true;
                  }


                  if(option == "wife")
                  {
                	  document.getElementById("relfullname").value = "";
                 	  document.getElementById("relemail").value = "";
                 	 document.getElementById("relmobile").value = "";
                 	 
                 	document.getElementById("relfullname").readOnly = false;
           		  document.getElementById("relemail").readOnly = false;
             		  document.getElementById("relmobile").readOnly = false;
             		  
                 	 
                        document.getElementById("inlineRadio2").checked = true;
                        document.getElementById("inlineRadio1").disabled = true;
                        document.getElementById("inlineRadio2").disabled = false;
                        document.getElementById("inlineRadio3").disabled = true;
                  }


                  if(option == "brother")
                  {
                	  document.getElementById("relfullname").value = "";
                 	  document.getElementById("relemail").value = "";
                 	 document.getElementById("relmobile").value = "";
                 	 
                 	document.getElementById("relfullname").readOnly = false;
           		  document.getElementById("relemail").readOnly = false;
             		  document.getElementById("relmobile").readOnly = false;
             		  
                 	 
                        document.getElementById("inlineRadio1").checked = true;
                        document.getElementById("inlineRadio1").disabled = false;
                        document.getElementById("inlineRadio2").disabled = true;
                        document.getElementById("inlineRadio3").disabled = true;
                  }

                  if(option == "sister")
                  {
                	  document.getElementById("relfullname").value = "";
                 	  document.getElementById("relemail").value = "";
                 	 document.getElementById("relmobile").value = "";
                 	 
                 	document.getElementById("relfullname").readOnly = false;
           		  document.getElementById("relemail").readOnly = false;
             		  document.getElementById("relmobile").readOnly = false;
             		  
                 	 
                        document.getElementById("inlineRadio2").checked = true;
                        document.getElementById("inlineRadio1").disabled = true;
                        document.getElementById("inlineRadio2").disabled = false;
                        document.getElementById("inlineRadio3").disabled = true;
                  }

                  if(option == "yourself")
                  {
                	  
                	  
                	
                	  
                	  document.getElementById("relfullname").value = "<%=usp.getFirstName()%>"+" "+"<%=usp.getLastName()%>";
                	  
                	  document.getElementById("relemail").value = "<%=usp.getEmail()%>";
                	 document.getElementById("relmobile").value = "<%=usp.getMobileNo()%>";
                	 
                	 
                	 
                 		  document.getElementById("relfullname").readOnly = true;
               		  document.getElementById("relemail").readOnly = true;
                 		  document.getElementById("relmobile").readOnly = true;
                		  
                      <% if(usergen.equals("male"))
                      { %>
                        document.getElementById("inlineRadio1").checked = true;
                        document.getElementById("inlineRadio1").disabled = false;
                        document.getElementById("inlineRadio2").disabled = true;
                        document.getElementById("inlineRadio3").disabled = true;
                       <%} 
                      else if(usergen.equals("female"))
                      { %>
                      document.getElementById("inlineRadio2").checked = true;
                      document.getElementById("inlineRadio1").disabled = true;
                      document.getElementById("inlineRadio2").disabled = false;
                      document.getElementById("inlineRadio3").disabled = true;
                     <%} 
                      else
                      { %>
                      document.getElementById("inlineRadio3").checked = true;
                      document.getElementById("inlineRadio1").disabled = true;
                      document.getElementById("inlineRadio2").disabled = true;
                      document.getElementById("inlineRadio3").disabled = false;
                     <%} 
                      
                      %>
                      
                      }

                  if(option == "other")
                  {
                	  document.getElementById("relfullname").value = "";
                 	  document.getElementById("relemail").value = "";
                 	 document.getElementById("relmobile").value = "";
                 	 
                 	document.getElementById("relfullname").readOnly = false;
           		  document.getElementById("relemail").readOnly = false;
             		  document.getElementById("relmobile").readOnly = false;
             		  
                 	 
                    //document.getElementById("inlineRadio1").checked = true;
                        document.getElementById("inlineRadio1").checked = false;
                        document.getElementById("inlineRadio2").checked = false;
                        //document.getElementById("inlineRadio3").checked = false;
                        document.getElementById("inlineRadio1").disabled = false;
                        document.getElementById("inlineRadio2").disabled = false;
                        //document.getElementById("inlineRadio3").disabled = false;
                  }
                  
                }
		
		

		function checkfirst() {
			const usernameValue = username.value.trim();
			if (usernameValue === '') {
				setErrorFor(username, 'Full Name cannot be blank');

			} else if (usernameValue.length < 3) {
				setErrorFor(username, 'name not valid');
			} else {
				setSuccessFor(username);

			}
		}

		// 		function checklast() {
		// 			const lastnameValue = lastname.value.trim();
		// 			if (lastnameValue === '') {
		// 				setErrorFor(lastname, 'Lastname cannot be blank');

		// 			} else if (lastnameValue.length < 3) {
		// 				setErrorFor(lastname, 'lastname not valid');
		// 			} else {
		// 				setSuccessFor(lastname);

		// 			}
		// 		}

		function checkemail() {
			const emailValue = email.value.trim();
			if (emailValue === '') {
				setErrorFor(email, 'Email cannot be blank');

			} else if (!isEmail(emailValue)) {
				setErrorFor(email, 'Not a valid email');

			} else {
				setSuccessFor(email);
			}
		}

		// 		function checkPassword1() {
		// 			const passwordValue = password.value.trim();
		// 			if (passwordValue === '') {
		// 				setErrorFor(password, 'Password cannot be blank');
		// 			} else if (passwordValue.length < 5) {
		// 				setErrorFor(password, 'Password not valid');
		// 			} else {
		// 				setSuccessFor(password);

		// 			}
		// 		}
		// 		function checkPassword2() {
		// 			const passwordValue = password.value.trim();
		// 			const password2Value = password2.value.trim();
		// 			if (password2Value === '') {
		// 				setErrorFor(password2, 'Password cannot be blank');

		// 			} else if (passwordValue !== password2Value) {
		// 				setErrorFor(password2, 'Passwords does not match');

		// 			} else {
		// 				setSuccessFor(password2);

		// 			}
		// 		}

		function checkmob() {
			const mobilevalue = mobile.value.trim();

			if (/^\d{10}$/.test(mobilevalue)) {
				setSuccessFor(mobile);

			} else {
				setErrorFor(mobile, 'Please enter valid number!');
			}
		}

		function checkYear() {
			const yobvalue = yob.value.trim();

			if (yobvalue === '') {
				setErrorFor(yob, 'Years cannot be blank');
			}
			
			else if(yobvalue > <%=myear%>){
				setErrorFor(yob, 'Years cannot exceed '+<%=myear%>+' years');
			}
			
			else {
				setSuccessFor(yob);

			}
		}

		// 		function checkaddress() {
		// 			const addressValue = address.value.trim();
		// 			if (addressValue === '') {
		// 				setErrorFor(address, 'Address cannot be blank');

		// 			} else if (addressValue.length < 5) {
		// 				setErrorFor(address, 'address not valid');
		// 			} else {
		// 				setSuccessFor(address);
		// 			}
		// 		}

		//for submitting the form all checks will be there

		function checkInputs() {
			// trim to remove the whitespaces

			var flag = 0;
			const usernameValue = username.value.trim();
			const emailValue = email.value.trim();
			
			
			
			
			
			var gen = document.getElementsByName("gender").value;
            if(document.getElementById("inlineRadio1").checked == false && document.getElementById("inlineRadio1").checked == false && document.getElementById("inlineRadio1").checked == false)
            {
                document.getElementById("radioerr").innerHTML = "Please select gender!";
                flag=0;
            }
            else{
                document.getElementById("radioerr").innerHTML = "";
                flag=1;
            }



    var option = document.getElementById("relation").value;

    if(option === '')
            {
                document.getElementById("errm").innerHTML = "Please select reletion!";
                flag=0;
            }
            else{
                document.getElementById("errm").innerHTML = "";
                
            }



    

			//const passwordValue = password.value.trim();
			//const password2Value = password2.value.trim();

			// 			const lastnameValue = lastname.value.trim();
			// 			if (lastnameValue === '') {
			// 				setErrorFor(lastname, 'Lastname cannot be blank');
			// 				flag = 0;
			// 			} else {
			// 				setSuccessFor(lastname);
			// 				flag = 1;
			// 			}

			// const gender =document.getElementById('inlineRadio1');
			// if(document.getElementById('inlineRadio1').checked || document.getElementById('inlineRadio2').checked) {
			// 	flag=1;
			//   }
			// else {
			// 	document.getElementById("radioerr").innerHTML = "Please select one !" ;
			//         document.getElementById("err").style.color = "red";
			// 		flag=0;
			// }

			if (usernameValue === '') {
				setErrorFor(username, 'Full Name cannot be blank');
				flag = 0;
			} else {
				setSuccessFor(username);
				//flag = 1;
			}

			if (emailValue === '') {
				setErrorFor(email, 'Email cannot be blank');
				flag = 0;
			} else if (!isEmail(emailValue)) {
				setErrorFor(email, 'Not a valid email');
				flag = 0;
			} else {
				setSuccessFor(email);
				//flag = 1;
			}

			// 			if (passwordValue === '') {
			// 				setErrorFor(password, 'Password cannot be blank');
			// 				flag = 0;
			// 			} else {
			// 				setSuccessFor(password);
			// 				flag = 1;
			// 			}

			// 			if (password2Value === '') {
			// 				setErrorFor(password2, 'Password cannot be blank');
			// 				flag = 0;
			// 			} else if (passwordValue !== password2Value) {
			// 				setErrorFor(password2, 'Passwords does not match');
			// 				flag = 0;
			// 			} else {
			// 				setSuccessFor(password2);
			// 				flag = 1;
			// 			}

			const mobilevalue = mobile.value.trim();

			if (/^\d{10}$/.test(mobilevalue)) {
				setSuccessFor(mobile);
				//flag = 1;
			} else {
				setErrorFor(mobile, 'Please enter valid number!');
				flag = 0;
			}

			const yobvalue = yob.value.trim();

			if (yobvalue === '') {
				setErrorFor(yob, 'Years cannot be blank');
				flag = 0;
			}
			else if(yobvalue > <%=myear%>){
				setErrorFor(yob, 'Years cannot exceed '+<%=myear%>+' years');
				flag=0;
			}
			
			else {
				setSuccessFor(yob);
				//flag = 1;
			}

			// 			const addressValue = address.value.trim();
			// 			if (addressValue === '') {
			// 				setErrorFor(address, 'Address cannot be blank');
			// 				flag = 0;
			// 			} else {
			// 				setSuccessFor(address);
			// 				//flag = 1;
			// 			}

			return Boolean(flag);

		}

		function setErrorFor(input, message) {
			const formControl = input.parentElement;
			const small = formControl.querySelector('small');
			formControl.className = 'form-control error';
			small.innerText = message;
		}

		function setSuccessFor(input) {
			const formControl = input.parentElement;
			formControl.className = 'form-control success';
		}

		function isEmail(email) {
			return /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/
					.test(email);
		}
	</script>



	<!-- Optional JavaScript -->
	<!-- jQuery first, then Popper.js, then Bootstrap JS -->
	<script src="https://code.jquery.com/jquery-3.2.1.slim.min.js"
		integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN"
		crossorigin="anonymous"></script>
	<script
		src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js"
		integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q"
		crossorigin="anonymous"></script>
	<script
		src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js"
		integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl"
		crossorigin="anonymous"></script>
</body>

</html>