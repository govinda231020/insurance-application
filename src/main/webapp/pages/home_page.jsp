<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!doctype html>
<html lang="en">

<head>
<!-- Required meta tags -->
<meta charset="utf-8">
<meta name="viewport"
	content="width=device-width, initial-scale=1, shrink-to-fit=no">

<!-- Bootstrap CSS -->
<link rel="stylesheet"
	href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css"
	integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm"
	crossorigin="anonymous">

<title>Home Page</title>

<style>
/* for dropdown in navbar */
.dropdown:hover .dropdown-menu {
	display: block;
	margin-top: 0;
}

#navname{
color:white;
float:Right;
padding-right: 20px;
font-size:20px;
}
</style>


</head>

<body>

	<nav class="navbar navbar-expand-lg navbar-dark bg-dark">
		<a class="navbar-brand" href="#">Insurance App</a>
		<button class="navbar-toggler" type="button" data-toggle="collapse"
			data-target="#navbarSupportedContent"
			aria-controls="navbarSupportedContent" aria-expanded="false"
			aria-label="Toggle navigation">
			<span class="navbar-toggler-icon"></span>
		</button>

		<div class="collapse navbar-collapse" id="navbarSupportedContent">
			<ul class="navbar-nav mr-auto">
				<li class="nav-item active"><a class="nav-link" href="/user/homered">Home<span
						class="sr-only">(current)</span></a></li>
				<div class="dropdown">
					<button class="btn btn-dark dropdown-toggle" type="button"
						id="dropdownMenuButton" data-toggle="dropdown"
						aria-haspopup="true" aria-expanded="false">Insurance
						Plans</button>
					<div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
						<a class="dropdown-item" href="lifeplans">Life Insurance</a> 
						<a
							class="dropdown-item" href="dentalplans">Dental Insurance</a> <a
							class="dropdown-item" href="visionplans">Vision Insurance</a>
					</div>
				</div>
			</ul>
			<div id="navname">Hi ${yourname}</div>

		</div>

		<div class="mx-2">
			<!-- <button class="btn btn-success">Logout</button> -->
			<div class="dropdown">
				<button class="btn btn-success dropdown-toggle" type="button"
					data-toggle="dropdown">
					Actions <span class="caret"></span>
				</button>
				<ul class="dropdown-menu">
					<div class="dropbt">
						<li><a href="/user/myprofile" style="color: black"><b>My Profile</b></a></li>
						<li><a href="/user/myPolicies" style="color: black"><b>My Policies</b></a></li>
						<li><a href="/user/myfamily" style="color: black"><b>My Family</b></a></li>
						<li><a href="/user/contact" style="color: black"><b>Contact Us</b></a></li>
						<li><a href="/logout" style="color: black"><b>Logout</b></a></li>
					</div>
				</ul>
			</div>

			<!-- <button class="btn btn-danger" data-toggle="modal" data-target="#signupModal">SignUp</button> -->
		</div>

	</nav>
	<br>
	<div class="container">
		<div class="row">
			<div class="col-md-4">
				<div class="card mb-4 box-shadow">
					<!-- <img class="card-img-top"
						data-src="holder.js/100px225?theme=thumb&amp;bg=55595c&amp;fg=eceeef&amp;text=Thumbnail"
						alt="Thumbnail [100%x225]"
						style="height: 225px; width: 100%; display: block;"
						src="/InsuranceApp/src/main/webapp/pages/images/lifeinssurance30 (1).jpg"
						data-holder-rendered="true"> -->
					<div class="card-body">
						<h4>Life Insurance</h4>
						<!-- <p>Your beneficiaries could use the money to help cover essential expenses, such as paying a mortgage or college tuition for your children in case you are trouble.</p> -->
						<div class="d-flex justify-content-between align-items-center">
							<div class="btn-group">
								<a href="/user/lifeplans"><button type="button"
										class="btn btn-success" value="lifeins">View Plans</button></a>
							</div>

						</div>
					</div>
				</div>
			</div>
			<div class="col-md-4">
				<div class="card mb-4 box-shadow">
					<!-- <img class="card-img-top"
						data-src="holder.js/100px225?theme=thumb&amp;bg=55595c&amp;fg=eceeef&amp;text=Thumbnail"
						alt="Thumbnail [100%x225]" src="images/Dentalns30 (1) (1).png"
						data-holder-rendered="true"
						style="height: 225px; width: 100%; display: block;"> -->
					<div class="card-body">
						<h4>Dental Insurance</h4>

						<!-- <p>Most dental plans cover preventive visits at little or no additional cost. You're protected from financial risk.</p> -->

						<div class="d-flex justify-content-between align-items-center">
							<div class="btn-group">
								<a href="/user/dentalplans"><button type="button"
										class="btn btn-success" value="dentalins">View Plans</button></a>
							</div>

						</div>
					</div>
				</div>
			</div>
			<div class="col-md-4">
				<div class="card mb-4 box-shadow">
					<!-- <img class="card-img-top"
						data-src="holder.js/100px225?theme=thumb&amp;bg=55595c&amp;fg=eceeef&amp;text=Thumbnail"
						alt="Thumbnail [100%x225]" src="images\Vision.jpg"
						data-holder-rendered="true"
						style="height: 225px; width: 100%; display: block;"> -->
					<div class="card-body">
						<h4>Vision Insurance</h4>

						<div class="d-flex justify-content-between align-items-center">
							<div class="btn-group">
								<a href="/user/visionplans"><button type="button"
										class="btn btn-success" value="visionins">View Plans</button></a>
							</div>

						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<!-- Optional JavaScript -->
	<!-- jQuery first, then Popper.js, then Bootstrap JS -->
	<script src="https://code.jquery.com/jquery-3.2.1.slim.min.js"
		integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN"
		crossorigin="anonymous"></script>
	<script
		src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js"
		integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q"
		crossorigin="anonymous"></script>
	<script
		src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js"
		integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl"
		crossorigin="anonymous"></script>
</body>

</html>