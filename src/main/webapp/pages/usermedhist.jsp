<%@page import="com.impetus.insuranceapp.model.MedicalHistory"%>
<%@page import="org.apache.coyote.RequestGroupInfo"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@page isELIgnored="false"%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>


<!doctype html>
<html lang="en">

<head>
<link rel="stylesheet"
	href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css"
	integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm"
	crossorigin="anonymous">

<!-- Required meta tags -->
<meta charset="utf-8">
<meta name="viewport"
	content="width=device-width, initial-scale=1, shrink-to-fit=no">
<script src="https://kit.fontawesome.com/e38070cbc2.js"
	crossorigin="anonymous"></script>
<!-- Bootstrap CSS -->
<link rel="stylesheet"
	href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css"
	integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm"
	crossorigin="anonymous">
<style>
.form {
	padding: 5px 40px;
}

.form-control {
	margin-bottom: 10px;
	padding-bottom: 10px;
	position: relative;
	border-style: none;
}

.form-control label {
	display: inline-block;
	margin-bottom: 5px;
}

.form-control input {
	border: 2px solid #f0f0f0;
	border-radius: 4px;
	display: block;
	font-family: inherit;
	font-size: 14px;
	padding: 10px;
	width: 100%;
}

.form-control input:focus {
	outline: 0;
	border-color: #777;
}

.form-control.success input {
	border-color: #2ecc71;
}

.form-control.error input {
	border-color: #e74c3c;
}

.form-control i {
	visibility: hidden;
	position: absolute;
	top: 50px;
	right: 20px;
}

.form-control.success i.fa-check-circle {
	color: #2ecc71;
	visibility: visible;
}

.form-control.error i.fa-exclamation-circle {
	color: #e74c3c;
	visibility: visible;
}

.form-control small {
	color: #e74c3c;
	position: absolute;
	bottom: 0;
	left: 0;
	visibility: hidden;
}

.form-control.error small {
	visibility: visible;
}

.form button {
	background-color: #8e44ad;
	border: 2px solid #8e44ad;
	border-radius: 4px;
	color: #fff;
	display: block;
	font-family: inherit;
	font-size: 16px;
	padding: 10px;
	margin-top: 20px;
	width: 100%;
}
#star {
	color: red;
	font-weight: bold;
}

#navname{
color:white;
float:Right;
padding-right: 20px;
font-size:20px;
}
/* for dropdown in navbar */
.dropdown:hover .dropdown-menu {
	display: block;
}
</style>

<title>Update Medical History</title>
</head>

<body>

	<nav class="navbar navbar-expand-lg navbar-dark bg-dark">
		<a class="navbar-brand" href="#">Insurance App</a>
		<button class="navbar-toggler" type="button" data-toggle="collapse"
			data-target="#navbarSupportedContent"
			aria-controls="navbarSupportedContent" aria-expanded="false"
			aria-label="Toggle navigation">
			<span class="navbar-toggler-icon"></span>
		</button>

		<div class="collapse navbar-collapse" id="navbarSupportedContent">
			<ul class="navbar-nav mr-auto">
				<li class="nav-item active"><a class="nav-link" href="/user/homered">Home<span
						class="sr-only">(current)</span></a></li>
				<div class="dropdown">
					<button class="btn btn-dark dropdown-toggle" type="button"
						id="dropdownMenuButton" data-toggle="dropdown"
						aria-haspopup="true" aria-expanded="false">Insurance
						Plans</button>
					<div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
						<a class="dropdown-item" href="/user/lifeplans">Life Insurance</a> 
						<a
							class="dropdown-item" href="/user/dentalplans">Dental Insurance</a> <a
							class="dropdown-item" href="/user/visionplans">Vision Insurance</a>
					</div>
				</div>
			</ul>
			<div id="navname">Hi ${yourname}</div>

		</div>

		<div class="mx-2">
			<!-- <button class="btn btn-success">Logout</button> -->
			<div class="dropdown">
				<button class="btn btn-success dropdown-toggle" type="button"
					data-toggle="dropdown">
					Actions <span class="caret"></span>
				</button>
				<ul class="dropdown-menu">
					<div class="dropbt">
						<li><a href="/user/myprofile" style="color: black"><b>My Profile</b></a></li>
						<li><a href="/user/myPolicies" style="color: black"><b>My Policies</b></a></li>
						<li><a href="/user/myfamily" style="color: black"><b>My Family</b></a></li>
						<li><a href="/user/contact" style="color: black"><b>Contact Us</b></a></li>
						<li><a href="/logout" style="color: black"><b>Logout</b></a></li>
					</div>
				</ul>
			</div>

			<!-- <button class="btn btn-danger" data-toggle="modal" data-target="#signupModal">SignUp</button> -->
		</div>

	</nav>
	<br>
	
		<% String mss=(String)session.getAttribute("message"); %>

<% if(mss != null)
	{
	%>
<div id="updatem"><h3 style="color:green; text-align:center"><%=mss %></h3></div>
<%
}
session.removeAttribute("message");
%>
	
	
	
	
	<%String plsupdate = (String) request.getAttribute("updatemedmessage");%>
	
	<% if(plsupdate != null)
	{
	%>
	<div id="updatem"><h3 style="color:red; text-align:center"><%=plsupdate %></h3></div>
		<%
			}

		%>			
	
	
	
	<% MedicalHistory med=(MedicalHistory)request.getAttribute("medi");
	String ch="checked";
	%>
	
	<% if (med!=null){
		
	%>
	

	<div class="container">
		<div class="row">
			<div
				class="col-lg-6 col-md-6 col-sm-6 container justify-content-center card">
				<h2 class="text-center">Update Medical History</h2>
				<div class="card-body">
				
				
				
					<form action="/user/usermedicalupdate" method="POST" id="form"
						class="form" onsubmit="return checkInputs()">
						<label for="heart">Heart Ailments</label> <span id="star">
							*</span><br>
						<div class="form-check form-check-inline">
							<input class="form-check-input" type="radio" name="heartAilments"
								id="inlineRadio1" value="no" <% if(med.getHeartAilments().equals("no") || med.getHeartAilments()==null){ %> <%=ch%> <%} %>> <label
								class="form-check-label" for="inlineRadio1">No</label>
						</div>
						<div class="form-check form-check-inline">
							<input class="form-check-input" type="radio" name="heartAilments"
								id="inlineRadio2" value="low"  <% if(med.getHeartAilments().equals("low")){ %> <%=ch%> <%} %>   > <label
								class="form-check-label" for="inlineRadio2">Low</label>
						</div>
						<div class="form-check form-check-inline">
							<input class="form-check-input" type="radio" name="heartAilments"
								id="inlineRadio3" value="moderate" <% if(med.getHeartAilments().equals("moderate")){ %> <%=ch%> <%} %>> <label
								class="form-check-label" for="inlineRadio3">Moderate</label>
						</div>
						<div class="form-check form-check-inline">
							<input class="form-check-input" type="radio" name="heartAilments"
								id="inlineRadio4" value="severe"  <% if(med.getHeartAilments().equals("severe")){ %> <%=ch%> <%} %>    > <label
								class="form-check-label" for="inlineRadio4">Severe</label>
						</div>
						<div id="radioerr"></div>
						<br> <label for="diabetes">Diabetes</label> <span id="star">
							*</span><br>
						<div class="form-check form-check-inline">
							<input class="form-check-input" type="radio" name="diabetes"
								id="inlineRadio5" value="no" <% if(med.getDiabetes().equals("no") || med.getDiabetes()==null){ %> <%=ch%> <%} %>> <label
								class="form-check-label" for="inlineRadio5">No</label>
						</div>
						<div class="form-check form-check-inline">
							<input class="form-check-input" type="radio" name="diabetes"
								id="inlineRadio6" value="low"  <% if(med.getDiabetes().equals("low")){ %> <%=ch%> <%} %>> <label
								class="form-check-label" for="inlineRadio6">Low</label>
						</div>
						<div class="form-check form-check-inline"> 
							<input class="form-check-input" type="radio" name="diabetes"
								id="inlineRadio7" value="moderate" <% if(med.getDiabetes().equals("moderate")){ %> <%=ch%> <%} %>> <label
								class="form-check-label" for="inlineRadio7">Moderate</label>
						</div>
						<div class="form-check form-check-inline">
							<input class="form-check-input" type="radio" name="diabetes"
								id="inlineRadio8" value="severe" <% if(med.getDiabetes().equals("severe")){ %> <%=ch%> <%} %>> <label
								class="form-check-label" for="inlineRadio8">Severe</label>
						</div>
						

						<div id="radioerr"></div>
						<br> <label for="tooth">Tooth Decay</label> <span id="star">
							*</span><br>
						<div class="form-check form-check-inline">
							<input class="form-check-input" type="radio" name="toothDecay"
								id="inlineRadio11" value="no" <% if(med.getToothDecay().equals("no") || med.getToothDecay()==null){ %> <%=ch%> <%} %>> <label
								class="form-check-label" for="inlineRadio11">No</label>
						</div>
						<div class="form-check form-check-inline">
							<input class="form-check-input" type="radio" name="toothDecay"
								id="inlineRadio12" value="yes" <% if(med.getToothDecay().equals("yes")){ %> <%=ch%> <%} %>  > <label
								class="form-check-label" for="inlineRadio12">Yes</label>
						</div>

						<div id="radioerr"></div>
						<br> <label for="covid">Covid</label> <span id="star">
							*</span><br>
						<div class="form-check form-check-inline">
							<input class="form-check-input" type="radio" name="covid"
								id="inlineRadio13" value="no" <% if(med.getCovid().equals("no") || med.getCovid()==null){ %> <%=ch%> <%} %>> <label
								class="form-check-label" for="inlineRadio13">No</label>
						</div>
						<div class="form-check form-check-inline">
							<input class="form-check-input" type="radio" name="covid"
								id="inlineRadio14" value="yes"  <% if(med.getCovid().equals("yes")){ %> <%=ch%> <%} %>  > <label
								class="form-check-label" for="inlineRadio14">Yes</label>
						</div>

						<div id="radioerr"></div>
						<br> <label for="cancer">Cancer</label> <span id="star">
							*</span><br>
						<div class="form-check form-check-inline">
							<input class="form-check-input" type="radio" name="cancer"
								id="inlineRadio15" value="no" <% if(med.getCancer().equals("no") || med.getCancer()==null){ %> <%=ch%> <%} %>> <label
								class="form-check-label" for="inlineRadio15">No</label>
						</div>
						<div class="form-check form-check-inline">
							<input class="form-check-input" type="radio" name="cancer"
								id="inlineRadio16" value="low" <% if(med.getCancer().equals("low")){ %> <%=ch%> <%} %>  > <label
								class="form-check-label" for="inlineRadio16">Low</label>
						</div>
						<div class="form-check form-check-inline">
							<input class="form-check-input" type="radio" name="cancer"
								id="inlineRadio17" value="moderate" <% if(med.getCancer().equals("moderate")){ %> <%=ch%> <%} %>  > <label
								class="form-check-label" for="inlineRadio17">Moderate</label>
						</div><div class="form-check form-check-inline">
							<input class="form-check-input" type="radio" name="cancer"
								id="inlineRadio18" value="severe"  <% if(med.getCancer().equals("severe")){ %> <%=ch%> <%} %> > <label
								class="form-check-label" for="inlineRadio18">Severe</label>
						</div>
						<div id="radioerr"></div>
						<br> <label for="cataract">Cataract</label> <span id="star">
							*</span><br>
						<div class="form-check form-check-inline">
							<input class="form-check-input" type="radio" name="cataract"
								id="inlineRadio19" value="no" <% if(med.getCataract().equals("no") || med.getCataract()==null){ %> <%=ch%> <%} %>> <label
								class="form-check-label" for="inlineRadio19">No</label>
						</div>
						<div class="form-check form-check-inline">
							<input class="form-check-input" type="radio" name="cataract"
								id="inlineRadio20" value="yes"  <% if(med.getCataract().equals("yes")){ %> <%=ch%> <%} %> > <label
								class="form-check-label" for="inlineRadio20">Yes</label>
						</div>
						<button type="submit" class="btn btn-primary">Update
							Medical History</button>
					</form>
					
				</div>
			</div>
		</div>
	</div>
	<%} 
	
	else
	{%>
	
	<div class="container">
		<div class="row">
			<div
				class="col-lg-6 col-md-6 col-sm-6 container justify-content-center card">
				<h2 class="text-center">Update Medical History</h2>
				<div class="card-body">
				

					<form action="/user/usermedicalupdate" method="POST" id="form"
						class="form" onsubmit="return checkInputs()">
						<label for="heart">Heart Ailments</label> <span id="star">
							*</span><br>
						<div class="form-check form-check-inline">
							<input class="form-check-input" type="radio" name="heartAilments"
								id="inlineRadio1" value="no" checked> <label
								class="form-check-label" for="inlineRadio1">No</label>
						</div>
						<div class="form-check form-check-inline">
							<input class="form-check-input" type="radio" name="heartAilments"
								id="inlineRadio2" value="low"> <label
								class="form-check-label" for="inlineRadio2">Low</label>
						</div>
						<div class="form-check form-check-inline">
							<input class="form-check-input" type="radio" name="heartAilments"
								id="inlineRadio3" value="moderate"> <label
								class="form-check-label" for="inlineRadio3">Moderate</label>
						</div>
						<div class="form-check form-check-inline">
							<input class="form-check-input" type="radio" name="heartAilments"
								id="inlineRadio4" value="severe"> <label
								class="form-check-label" for="inlineRadio4">Severe</label>
						</div>
						<div id="radioerr"></div>
						<br> <label for="diabetes">Diabetes</label> <span id="star">
							*</span><br>
						<div class="form-check form-check-inline">
							<input class="form-check-input" type="radio" name="diabetes"
								id="inlineRadio5" value="no" checked> <label
								class="form-check-label" for="inlineRadio5">No</label>
						</div>
						<div class="form-check form-check-inline">
							<input class="form-check-input" type="radio" name="diabetes"
								id="inlineRadio6" value="low"> <label
								class="form-check-label" for="inlineRadio6">Low</label>
						</div>
						<div class="form-check form-check-inline"> 
							<input class="form-check-input" type="radio" name="diabetes"
								id="inlineRadio7" value="moderate"> <label
								class="form-check-label" for="inlineRadio7">Moderate</label>
						</div>
						<div class="form-check form-check-inline">
							<input class="form-check-input" type="radio" name="diabetes"
								id="inlineRadio8" value="severe"> <label
								class="form-check-label" for="inlineRadio8">Severe</label>
						</div>
						

						<div id="radioerr"></div>
						<br> <label for="tooth">Tooth Decay</label> <span id="star">
							*</span><br>
						<div class="form-check form-check-inline">
							<input class="form-check-input" type="radio" name="toothDecay"
								id="inlineRadio11" value="no" checked> <label
								class="form-check-label" for="inlineRadio11">No</label>
						</div>
						<div class="form-check form-check-inline">
							<input class="form-check-input" type="radio" name="toothDecay"
								id="inlineRadio12" value="yes"> <label
								class="form-check-label" for="inlineRadio12">Yes</label>
						</div>

						<div id="radioerr"></div>
						<br> <label for="covid">Covid</label> <span id="star">
							*</span><br>
						<div class="form-check form-check-inline">
							<input class="form-check-input" type="radio" name="covid"
								id="inlineRadio13" value="no" checked> <label
								class="form-check-label" for="inlineRadio13">No</label>
						</div>
						<div class="form-check form-check-inline">
							<input class="form-check-input" type="radio" name="covid"
								id="inlineRadio14" value="yes"> <label
								class="form-check-label" for="inlineRadio14">Yes</label>
						</div>

						<div id="radioerr"></div>
						<br> <label for="cancer">Cancer</label> <span id="star">
							*</span><br>
						<div class="form-check form-check-inline">
							<input class="form-check-input" type="radio" name="cancer"
								id="inlineRadio15" value="no" checked> <label
								class="form-check-label" for="inlineRadio15">No</label>
						</div>
						<div class="form-check form-check-inline">
							<input class="form-check-input" type="radio" name="cancer"
								id="inlineRadio16" value="low"> <label
								class="form-check-label" for="inlineRadio16">Low</label>
						</div>
						<div class="form-check form-check-inline">
							<input class="form-check-input" type="radio" name="cancer"
								id="inlineRadio17" value="moderate"> <label
								class="form-check-label" for="inlineRadio17">Moderate</label>
						</div><div class="form-check form-check-inline">
							<input class="form-check-input" type="radio" name="cancer"
								id="inlineRadio18" value="severe"> <label
								class="form-check-label" for="inlineRadio18">Severe</label>
						</div>
						<div id="radioerr"></div>
						<br> <label for="cataract">Cataract</label> <span id="star">
							*</span><br>
						<div class="form-check form-check-inline">
							<input class="form-check-input" type="radio" name="cataract"
								id="inlineRadio19" value="no" checked> <label
								class="form-check-label" for="inlineRadio19">No</label>
						</div>
						<div class="form-check form-check-inline">
							<input class="form-check-input" type="radio" name="cataract"
								id="inlineRadio20" value="yes"> <label
								class="form-check-label" for="inlineRadio20">Yes</label>
						</div>
						<button type="submit" class="btn btn-primary">Update
							Medical History</button>
					</form>
					
				</div>
			</div>
		</div>
	</div>
	
	
	<%} %>
	<br>
	
	


	<script>
		const form = document.getElementById('form');
		const username = document.getElementById('username');
		const email = document.getElementById('email');
		const password = document.getElementById('password');
		const password2 = document.getElementById('password2');

		const lastname = document.getElementById('lastname');

		const mobile = document.getElementById('mobile');

		const yob = document.getElementById('yob');

		const address = document.getElementById('address');

		//for particular field check on TAB

		function checkfirst() {
			const usernameValue = username.value.trim();
			if (usernameValue === '') {
				setErrorFor(username, 'Username cannot be blank');

			} else if (usernameValue.length < 3) {
				setErrorFor(username, 'name not valid');
			} else {
				setSuccessFor(username);

			}
		}

		function checklast() {
			const lastnameValue = lastname.value.trim();
			if (lastnameValue === '') {
				setErrorFor(lastname, 'Lastname cannot be blank');

			} else if (lastnameValue.length < 3) {
				setErrorFor(lastname, 'lastname not valid');
			} else {
				setSuccessFor(lastname);

			}
		}

		function checkemail() {
			const emailValue = email.value.trim();
			if (emailValue === '') {
				setErrorFor(email, 'Email cannot be blank');

			} else if (!isEmail(emailValue)) {
				setErrorFor(email, 'Not a valid email');

			} else {
				setSuccessFor(email);
			}
		}

		function checkPassword1() {
			const passwordValue = password.value.trim();
			if (passwordValue === '') {
				setErrorFor(password, 'Password cannot be blank');
			} else if (passwordValue.length < 5) {
				setErrorFor(password, 'Password not valid');
			} else {
				setSuccessFor(password);

			}
		}
		function checkPassword2() {
			const passwordValue = password.value.trim();
			const password2Value = password2.value.trim();
			if (password2Value === '') {
				setErrorFor(password2, 'Password cannot be blank');

			} else if (passwordValue !== password2Value) {
				setErrorFor(password2, 'Passwords does not match');

			} else {
				setSuccessFor(password2);

			}
		}

		function checkmob() {
			const mobilevalue = mobile.value.trim();

			if (/^\d{10}$/.test(mobilevalue)) {
				setSuccessFor(mobile);

			} else {
				setErrorFor(mobile, 'Please enter valid number!');
			}
		}

		function checkYear() {
			const yobvalue = yob.value.trim();

			if (yobvalue === '') {
				setErrorFor(yob, 'year cannot be blank');

			} else if (yobvalue<1920 || yobvalue>(new Date().getFullYear()) - 5) {
				setErrorFor(yob, 'Enter valid Year');

			} else {
				setSuccessFor(yob);

			}
		}

		function checkaddress() {
			const addressValue = address.value.trim();
			if (addressValue === '') {
				setErrorFor(address, 'Address cannot be blank');

			} else if (addressValue.length < 5) {
				setErrorFor(address, 'address not valid');
			} else {
				setSuccessFor(address);
			}
		}

		//for submitting the form all checks will be there

		function checkInputs() {
			// trim to remove the whitespaces

			var flag = 0;
			const usernameValue = username.value.trim();
			const emailValue = email.value.trim();
			const passwordValue = password.value.trim();
			const password2Value = password2.value.trim();

			const lastnameValue = lastname.value.trim();
			if (lastnameValue === '') {
				setErrorFor(lastname, 'Lastname cannot be blank');
				flag = 0;
			} else {
				setSuccessFor(lastname);
				flag = 1;
			}

			// const gender =document.getElementById('inlineRadio1');
			// if(document.getElementById('inlineRadio1').checked || document.getElementById('inlineRadio2').checked) {
			// 	flag=1;
			//   }
			// else {
			// 	document.getElementById("radioerr").innerHTML = "Please select one !" ;
			//         document.getElementById("err").style.color = "red";
			// 		flag=0;
			// }

			if (usernameValue === '') {
				setErrorFor(username, 'Username cannot be blank');
				flag = 0;
			} else {
				setSuccessFor(username);
				flag = 1;
			}

			if (emailValue === '') {
				setErrorFor(email, 'Email cannot be blank');
				flag = 0;
			} else if (!isEmail(emailValue)) {
				setErrorFor(email, 'Not a valid email');
				flag = 0;
			} else {
				setSuccessFor(email);
				flag = 1;
			}

			if (passwordValue === '') {
				setErrorFor(password, 'Password cannot be blank');
				flag = 0;
			} else {
				setSuccessFor(password);
				flag = 1;
			}

			if (password2Value === '') {
				setErrorFor(password2, 'Password cannot be blank');
				flag = 0;
			} else if (passwordValue !== password2Value) {
				setErrorFor(password2, 'Passwords does not match');
				flag = 0;
			} else {
				setSuccessFor(password2);
				flag = 1;
			}

			const mobilevalue = mobile.value.trim();

			if (/^\d{10}$/.test(mobilevalue)) {
				setSuccessFor(mobile);
				flag = 1;
			} else {
				setErrorFor(mobile, 'Please enter valid number!');
				flag = 0;
			}

			const yobvalue = yob.value.trim();

			if (yobvalue === '') {
				setErrorFor(yob, 'year cannot be blank');
				flag = 0;
			} else if (yobvalue<1960 || yobvalue>(new Date().getFullYear()) - 5) {
				setErrorFor(yob, 'Enter valid Year');
				flag = 0;
			} else {
				setSuccessFor(yob);
				flag = 1;
			}

			const addressValue = address.value.trim();
			if (addressValue === '') {
				setErrorFor(address, 'Address cannot be blank');
				flag = 0;
			} else {
				setSuccessFor(address);
				flag = 1;
			}

			return Boolean(flag);

		}

		function setErrorFor(input, message) {
			const formControl = input.parentElement;
			const small = formControl.querySelector('small');
			formControl.className = 'form-control error';
			small.innerText = message;
		}

		function setSuccessFor(input) {
			const formControl = input.parentElement;
			formControl.className = 'form-control success';
		}

		function isEmail(email) {
			return /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/
					.test(email);
		}
	</script>


	<!-- Optional JavaScript -->
	<!-- jQuery first, then Popper.js, then Bootstrap JS -->
	<script src="https://code.jquery.com/jquery-3.2.1.slim.min.js"
		integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN"
		crossorigin="anonymous"></script>
	<script
		src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js"
		integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q"
		crossorigin="anonymous"></script>
	<script
		src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js"
		integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl"
		crossorigin="anonymous"></script>
</body>

</html>