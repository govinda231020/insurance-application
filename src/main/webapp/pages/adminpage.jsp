<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>

    

<!doctype html>
<html lang="en">

<head>
<!-- Required meta tags -->
<meta charset="utf-8">
<meta name="viewport"
	content="width=device-width, initial-scale=1, shrink-to-fit=no">

<!-- Bootstrap CSS -->
<link rel="stylesheet"
	href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css"
	integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm"
	crossorigin="anonymous">

<title>Life Insurance</title>

<style>
#navname{
color:white;
float:Right;
padding-right: 20px;
font-size:20px;
}
</style>
</head>

<body>
	<nav class="navbar navbar-expand-lg navbar-dark bg-dark">
		<a class="navbar-brand" href="#">Insurance App</a>
		<button class="navbar-toggler" type="button" data-toggle="collapse"
			data-target="#navbarSupportedContent"
			aria-controls="navbarSupportedContent" aria-expanded="false"
			aria-label="Toggle navigation">
			<span class="navbar-toggler-icon"></span>
		</button>

		<div class="collapse navbar-collapse" id="navbarSupportedContent">
			<ul class="navbar-nav mr-auto">
				<li class="nav-item active"><a class="nav-link" href="/admin/homepage">Home</a>
				</li>
				<li class="nav-item active"><a class="nav-link" name="allplan" value="planvalue" href="/admin/allplans">All Plans</a>
				</li>
			</ul>
			<div id="navname">Hi Admin</div>	
			
		</div>
		
		<div class="mx-2">
            <!-- <button class="btn btn-success">Logout</button> -->
            <div class="dropdown">
               <a href="/logout"> <button class="btn btn-success">Logout</button></a>
                  
            </div>

            
        </div>

	</nav>
	
	
	<br>
    <div class="container">
        <div class="row">
            <div class="col-md-4">
                <div class="card mb-4 box-shadow">
                   
                    <div class="card-body">
                        <h4>History Of Policies</h4>
                        <!-- <p>Your beneficiaries could use the money to help cover essential expenses, such as paying a mortgage or college tuition for your children in case you are trouble.</p> -->
                        <div class="d-flex justify-content-between align-items-center">
                            <div class="btn-group">
                         <a href="/admin/Policies"> <button type="button" class="btn btn-success">View History</button></a>
                            </div>
                            
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-4">
                <div class="card mb-4 box-shadow">
                    
                    <div class="card-body">
                        <h4>User Management</h4>
    
                        <!-- <p>Most dental plans cover preventive visits at little or no additional cost. You're protected from financial risk.</p> -->

                        <div class="d-flex justify-content-between align-items-center">
                            <div class="btn-group">
                             <a href="/admin/allUsers">  <button type="button" class="btn btn-success">View Users</button></a> 
                            </div>
                            
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-4">
                <div class="card mb-4 box-shadow">
                    
                    <div class="card-body">
                        <h4>Insurance Plans</h4>
                        
                        <div class="d-flex justify-content-between align-items-center">
                            <div class="btn-group">
                               <a href="/admin/allplans"> <button type="button" class="btn btn-success">View Insurance Plans</button></a>
                            </div>
                            
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

	
		
				
					
			

	<!-- Optional JavaScript -->
	<!-- jQuery first, then Popper.js, then Bootstrap JS -->
	<script src="https://code.jquery.com/jquery-3.2.1.slim.min.js"
		integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN"
		crossorigin="anonymous"></script>
	<script
		src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js"
		integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q"
		crossorigin="anonymous"></script>
	<script
		src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js"
		integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl"
		crossorigin="anonymous"></script>

</body>

</html>
    