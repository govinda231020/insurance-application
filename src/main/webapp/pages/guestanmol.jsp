<%@page import="com.impetus.insuranceapp.model.UserProfile"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>

<!doctype html>
<html lang="en">

<head>
<!-- Required meta tags -->
<meta charset="utf-8">
<meta name="viewport"
	content="width=device-width, initial-scale=1, shrink-to-fit=no">

<!-- Bootstrap CSS -->
<link rel="stylesheet"
	href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css"
	integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm"
	crossorigin="anonymous">

<title>Guest Page</title>
<script src="https://kit.fontawesome.com/e38070cbc2.js"
	crossorigin="anonymous"></script>






<style>
.form {
	padding: 30px 40px;
}

.form-control {
	margin-bottom: 10px;
	padding-bottom: 20px;
	position: relative;
	border-style: none;
}

.form-control label {
	display: inline-block;
	margin-bottom: 5px;
}

.form-control input {
	border: 2px solid #f0f0f0;
	border-radius: 4px;
	display: block;
	font-family: inherit;
	font-size: 14px;
	padding: 10px;
	width: 100%;
}

.form-control input:focus {
	outline: 0;
	border-color: #777;
}

.form-control.success input {
	border-color: #2ecc71;
}

.form-control.error input {
	border-color: #e74c3c;
}

.form-control i {
	visibility: hidden;
	position: absolute;
	top: 50px;
	right: 20px;
}

.form-control.success i.fa-check-circle {
	color: #2ecc71;
	visibility: visible;
}

.form-control.error i.fa-exclamation-circle {
	color: #e74c3c;
	visibility: visible;
}

.form-control small {
	color: #e74c3c;
	position: absolute;
	bottom: 0;
	left: 0;
	visibility: hidden;
}

.form-control.error small {
	visibility: visible;
}

.form button {
	background-color: #8e44ad;
	border: 2px solid #8e44ad;
	border-radius: 4px;
	color: #fff;
	display: block;
	font-family: inherit;
	font-size: 16px;
	padding: 10px;
	margin-top: 20px;
	width: 100%;
}

/* for dropdown in navbar */
.dropdown:hover .dropdown-menu {
	display: block;
	margin-top: 0;
}
</style>


</head>

<body>

	<nav class="navbar navbar-expand-lg navbar-dark bg-dark">
		<a class="navbar-brand" href="#">Insurance App</a>
		<button class="navbar-toggler" type="button" data-toggle="collapse"
			data-target="#navbarSupportedContent"
			aria-controls="navbarSupportedContent" aria-expanded="false"
			aria-label="Toggle navigation">
			<span class="navbar-toggler-icon"></span>
		</button>

		<div class="collapse navbar-collapse" id="navbarSupportedContent">
			<ul class="navbar-nav mr-auto">
				<li class="nav-item active"><a class="nav-link" href="#">Home
						<span class="sr-only">(current)</span>
				</a></li>

				<div class="dropdown">
					<button class="btn btn-dark dropdown-toggle" type="button"
						id="dropdownMenuButton" data-toggle="dropdown"
						aria-haspopup="true" aria-expanded="false">Insurance
						plans</button>
					<div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
						<a class="dropdown-item" href="${pageContext.request.contextPath}/lifeplans">Life Insurance</a> <a
							class="dropdown-item" href="${pageContext.request.contextPath}/dentalplans">Dental Insurance</a> <a
							class="dropdown-item" href="${pageContext.request.contextPath}/visionplans">Vision Insurance</a>
					</div>
				</div>


			</ul>
			<div class="mx-2">	
				<a href="login">
					<button class="btn btn-success">Login</button>
				</a>
				<a href="signup">
					<button class="btn btn-success">SignUp</button>
				</a>
			</div>
		</div>
	</nav>
	<br>
	<!--Login Modal -->
	

	<!-- <br> -->

	<div class="container">
		<div class="row">
			<div class="col-md-4">
				<div class="card mb-4 box-shadow">
					<!-- <img class="card-img-top"
						data-src="holder.js/100px225?theme=thumb&amp;bg=55595c&amp;fg=eceeef&amp;text=Thumbnail"
						alt="Thumbnail [100%x225]"
						style="height: 225px; width: 100%; display: block;"
						src="/InsuranceApp/src/main/webapp/pages/images/lifeinssurance30 (1).jpg"
						data-holder-rendered="true"> -->
					<div class="card-body">
						<h4>Life Insurance</h4>
						<!-- <p>Your beneficiaries could use the money to help cover essential expenses, such as paying a mortgage or college tuition for your children in case you are trouble.</p> -->
						<div class="d-flex justify-content-between align-items-center">
							<div class="btn-group">
								<a href="${pageContext.request.contextPath}/lifeplans"><button type="button"
										class="btn btn-success" value="lifeins">View Plans</button></a>
							</div>

						</div>
					</div>
				</div>
			</div>
			<div class="col-md-4">
				<div class="card mb-4 box-shadow">
					<!-- <img class="card-img-top"
						data-src="holder.js/100px225?theme=thumb&amp;bg=55595c&amp;fg=eceeef&amp;text=Thumbnail"
						alt="Thumbnail [100%x225]" src="images/Dentalns30 (1) (1).png"
						data-holder-rendered="true"
						style="height: 225px; width: 100%; display: block;"> -->
					<div class="card-body">
						<h4>Dental Insurance</h4>

						<!-- <p>Most dental plans cover preventive visits at little or no additional cost. You're protected from financial risk.</p> -->

						<div class="d-flex justify-content-between align-items-center">
							<div class="btn-group">
								<a href="${pageContext.request.contextPath}/dentalplans"><button type="button"
										class="btn btn-success" value="dentalins">View Plans</button></a>
							</div>

						</div>
					</div>
				</div>
			</div>
			<div class="col-md-4">
				<div class="card mb-4 box-shadow">
					<!-- <img class="card-img-top"
						data-src="holder.js/100px225?theme=thumb&amp;bg=55595c&amp;fg=eceeef&amp;text=Thumbnail"
						alt="Thumbnail [100%x225]" src="images\Vision.jpg"
						data-holder-rendered="true"
						style="height: 225px; width: 100%; display: block;"> -->
					<div class="card-body">
						<h4>Vision Insurance</h4>

						<div class="d-flex justify-content-between align-items-center">
							<div class="btn-group">
								<a href="${pageContext.request.contextPath}/visionplans"><button type="button"
										class="btn btn-success" value="visionins">View Plans</button></a>
							</div>

						</div>
					</div>
				</div>
			</div>
		</div>
	</div>

	<!--life Modal-->
	<div class="modal fade" id="lifeModal" tabindex="-1" role="dialog"
		aria-labelledby="lifeModalLabel" aria-hidden="true">
		<div class="modal-dialog" role="document">
			<div class="modal-content">
				<div class="modal-header">
					<h5 class="modal-title" id="lifeModalLabel">Enter Details</h5>
					<button type="button" class="close" data-dismiss="modal"
						aria-label="Close">
						<span aria-hidden="true">&times;</span>
					</button>
				</div>
				<div class="modal-body">
					<form>
						<div class="form-group">
							<label for="exampleInputEmail1">Age</label> <input type="number"
								min="5" class="form-control" id="exampleInputlifeage1"
								aria-describedby="emailHelp" placeholder="Enter Age">
						</div>

						<div class="form-group">
							<label for="exampleInputPassword1">Annual Income</label><br>

							<select class="form-select" aria-label="Default select example"
								id="income" name="Annual Income">
								<option value="below than 1 lakhs"><b>below than 1
										lakhs</b></option>
								<option value="1-3 lakhs">1-3 lakhs</option>
								<option value="3-5 lakhs">3-5 lakhs</option>
								<option value="5-10 lakhs">5-10 lakhs</option>
								<option value="10+ lakhs">10+ lakhs</option>
							</select>

						</div>

						<button type="submit" class="btn btn-primary">Proceed</button>
					</form>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-secondary"
						data-dismiss="modal">Close</button>
				</div>
			</div>
		</div>
	</div>
	<!--dental Modal-->
	<div class="modal fade" id="dentalModal" tabindex="-1" role="dialog"
		aria-labelledby="dentalModalLabel" aria-hidden="true">
		<div class="modal-dialog" role="document">
			<div class="modal-content">
				<div class="modal-header">
					<h5 class="modal-title" id="dentalModalLabel">Enter Details</h5>
					<button type="button" class="close" data-dismiss="modal"
						aria-label="Close">
						<span aria-hidden="true">&times;</span>
					</button>
				</div>
				<div class="modal-body">
					<form>
						<div class="form-group">
							<label for="exampleInputEmail1">Age</label> <input type="number"
								min="5" class="form-control" id="exampleInputlifeage1"
								aria-describedby="emailHelp" placeholder="Enter Age">
						</div>

						<div class="form-group">
							<label for="exampleInputPassword1">Annual Income</label><br>

							<select class="form-select" aria-label="Default select example"
								id="income" name="Annual Income">
								<option value="below than 1 lakhs"><b>below than 1
										lakhs</b></option>
								<option value="1-3 lakhs">1-3 lakhs</option>
								<option value="3-5 lakhs">3-5 lakhs</option>
								<option value="5-10 lakhs">5-10 lakhs</option>
								<option value="10+ lakhs">10+ lakhs</option>
							</select>

						</div>

						<button type="submit" class="btn btn-primary">Proceed</button>
					</form>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-secondary"
						data-dismiss="modal">Close</button>
				</div>
			</div>
		</div>
	</div>
	<!--vision Modal-->
	<div class="modal fade" id="visionModal" tabindex="-1" role="dialog"
		aria-labelledby="visionModalLabel" aria-hidden="true">
		<div class="modal-dialog" role="document">
			<div class="modal-content">
				<div class="modal-header">
					<h5 class="modal-title" id="visionModalLabel">Enter Details</h5>
					<button type="button" class="close" data-dismiss="modal"
						aria-label="Close">
						<span aria-hidden="true">&times;</span>
					</button>
				</div>
				<div class="modal-body">
					<form>
						<div class="form-group">
							<label for="exampleInputEmail1">Age</label> <input type="number"
								min="5" class="form-control" id="exampleInputlifeage1"
								aria-describedby="emailHelp" placeholder="Enter Age">
						</div>

						<div class="form-group">
							<label for="exampleInputPassword1">Annual Income</label><br>

							<select class="form-select" aria-label="Default select example"
								id="income" name="Annual Income">
								<option value="below than 1 lakhs"><b>below than 1
										lakhs</b></option>
								<option value="1-3 lakhs">1-3 lakhs</option>
								<option value="3-5 lakhs">3-5 lakhs</option>
								<option value="5-10 lakhs">5-10 lakhs</option>
								<option value="10+ lakhs">10+ lakhs</option>
							</select>

						</div>

						<button type="submit" class="btn btn-primary">Proceed</button>
					</form>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-secondary"
						data-dismiss="modal">Close</button>
				</div>
			</div>
		</div>
	</div>
	<!-- Optional JavaScript -->
	<!-- jQuery first, then Popper.js, then Bootstrap JS -->
	<script src="https://code.jquery.com/jquery-3.2.1.slim.min.js"
		integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN"
		crossorigin="anonymous"></script>
	<script
		src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js"
		integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q"
		crossorigin="anonymous"></script>
	<script
		src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js"
		integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl"
		crossorigin="anonymous"></script>


	<!-- <script src="LoginModal.js"></script> -->

	<!-- <script>
		const form = document.getElementById('form');
		const username = document.getElementById('username');
		const email = document.getElementById('email');
		const password = document.getElementById('password');
		const password2 = document.getElementById('password2');

		const lastname = document.getElementById('lastname');

		const mobile = document.getElementById('mobile');

		const yob = document.getElementById('yob');

		const address = document.getElementById('address');

		//for particular field check on TAB

		function checkfirst() {
			const usernameValue = username.value.trim();
			if (usernameValue === '') {
				setErrorFor(username, 'Username cannot be blank');

			} else if (usernameValue.length < 3) {
				setErrorFor(username, 'name not valid');
			} else {
				setSuccessFor(username);

			}
		}

		function checklast() {
			const lastnameValue = lastname.value.trim();
			if (lastnameValue === '') {
				setErrorFor(lastname, 'Lastname cannot be blank');

			} else if (lastnameValue.length < 3) {
				setErrorFor(lastname, 'lastname not valid');
			} else {
				setSuccessFor(lastname);

			}
		}

		function checkemail() {
			const emailValue = email.value.trim();
			if (emailValue === '') {
				setErrorFor(email, 'Email cannot be blank');

			} else if (!isEmail(emailValue)) {
				setErrorFor(email, 'Not a valid email');

			} else {
				setSuccessFor(email);
			}
		}

		function checkPassword1() {
			const passwordValue = password.value.trim();
			if (passwordValue === '') {
				setErrorFor(password, 'Password cannot be blank');
			} else if (passwordValue.length < 5) {
				setErrorFor(password, 'Password not valid');
			} else {
				setSuccessFor(password);

			}
		}
		function checkPassword2() {
			const passwordValue = password.value.trim();
			const password2Value = password2.value.trim();
			if (password2Value === '') {
				setErrorFor(password2, 'Password cannot be blank');

			} else if (passwordValue !== password2Value) {
				setErrorFor(password2, 'Passwords does not match');

			} else {
				setSuccessFor(password2);

			}
		}

		function checkmob() {
			const mobilevalue = mobile.value.trim();

			if (/^\d{10}$/.test(mobilevalue)) {
				setSuccessFor(mobile);

			} else {
				setErrorFor(mobile, 'Please enter valid number!');
			}
		}

		function checkYear() {
			const yobvalue = yob.value.trim();

			if (yobvalue === '') {
				setErrorFor(yob, 'year cannot be blank');

			} else if (yobvalue<1920 || yobvalue>(new Date().getFullYear()) - 5) {
				setErrorFor(yob, 'Enter valid Year');

			} else {
				setSuccessFor(yob);

			}
		}

		function checkaddress() {
			const addressValue = address.value.trim();
			if (addressValue === '') {
				setErrorFor(address, 'Address cannot be blank');

			} else if (addressValue.length < 5) {
				setErrorFor(address, 'address not valid');
			} else {
				setSuccessFor(address);
			}
		}

		//for submitting the form all checks will be there

		function checkInputs() {
			// trim to remove the whitespaces

			var flag = 0;
			const usernameValue = username.value.trim();
			const emailValue = email.value.trim();
			const passwordValue = password.value.trim();
			const password2Value = password2.value.trim();

			const lastnameValue = lastname.value.trim();
			if (lastnameValue === '') {
				setErrorFor(lastname, 'Lastname cannot be blank');
				flag = 0;
			} else {
				setSuccessFor(lastname);
				flag = 1;
			}

			// const gender =document.getElementById('inlineRadio1');
			// if(document.getElementById('inlineRadio1').checked || document.getElementById('inlineRadio2').checked) {
			// 	flag=1;
			//   }
			// else {
			// 	document.getElementById("radioerr").innerHTML = "Please select one !" ;
			//         document.getElementById("err").style.color = "red";
			// 		flag=0;
			// }

			if (usernameValue === '') {
				setErrorFor(username, 'Username cannot be blank');
				flag = 0;
			} else {
				setSuccessFor(username);
				flag = 1;
			}

			if (emailValue === '') {
				setErrorFor(email, 'Email cannot be blank');
				flag = 0;
			} else if (!isEmail(emailValue)) {
				setErrorFor(email, 'Not a valid email');
				flag = 0;
			} else {
				setSuccessFor(email);
				flag = 1;
			}

			if (passwordValue === '') {
				setErrorFor(password, 'Password cannot be blank');
				flag = 0;
			} else {
				setSuccessFor(password);
				flag = 1;
			}

			if (password2Value === '') {
				setErrorFor(password2, 'Password cannot be blank');
				flag = 0;
			} else if (passwordValue !== password2Value) {
				setErrorFor(password2, 'Passwords does not match');
				flag = 0;
			} else {
				setSuccessFor(password2);
				flag = 1;
			}

			const mobilevalue = mobile.value.trim();

			if (/^\d{10}$/.test(mobilevalue)) {
				setSuccessFor(mobile);
				flag = 1;
			} else {
				setErrorFor(mobile, 'Please enter valid number!');
				flag = 0;
			}

			const yobvalue = yob.value.trim();

			if (yobvalue === '') {
				setErrorFor(yob, 'year cannot be blank');
				flag = 0;
			} else if (yobvalue<1960 || yobvalue>(new Date().getFullYear()) - 5) {
				setErrorFor(yob, 'Enter valid Year');
				flag = 0;
			} else {
				setSuccessFor(yob);
				flag = 1;
			}

			const addressValue = address.value.trim();
			if (addressValue === '') {
				setErrorFor(address, 'Address cannot be blank');
				flag = 0;
			} else {
				setSuccessFor(address);
				flag = 1;
			}

			return Boolean(flag);

		}

		function setErrorFor(input, message) {
			const formControl = input.parentElement;
			const small = formControl.querySelector('small');
			formControl.className = 'form-control error';
			small.innerText = message;
		}

		function setSuccessFor(input) {
			const formControl = input.parentElement;
			formControl.className = 'form-control success';
		}

		function isEmail(email) {
			return /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/
					.test(email);
		}
	</script>
 -->







	<!-- <script>

const form = document.getElementById('form');
const username = document.getElementById('username');
const email = document.getElementById('email');
const password = document.getElementById('password');
const password2 = document.getElementById('password2');


const lastname = document.getElementById('lastname');

const mobile=document.getElementById('mobile');

const yob=document.getElementById('yob');

const address=document.getElementById('address');

function checkInputs() {
	// trim to remove the whitespaces
    var flag=0;
	const usernameValue = username.value.trim();
	const emailValue = email.value.trim();
	const passwordValue = password.value.trim();
	const password2Value = password2.value.trim();

const lastnameValue=lastname.value.trim();
if(lastnameValue === '') {
    setErrorFor(lastname, 'Lastname cannot be blank');
    flag=0;
} else {
    setSuccessFor(lastname);
    flag=1;
}








// const gender =document.getElementById('inlineRadio1');
// if(document.getElementById('inlineRadio1').checked || document.getElementById('inlineRadio2').checked) {
// 	flag=1;
//   }
// else {
// 	document.getElementById("radioerr").innerHTML = "Please select one !" ;
//         document.getElementById("err").style.color = "red";
// 		flag=0;
// }






	
	if(usernameValue === '') {
		setErrorFor(username, 'Username cannot be blank');
        flag=0;
	} else {
		setSuccessFor(username);
        flag=1;
	}


	
	if(emailValue === '') {
		setErrorFor(email, 'Email cannot be blank');
        flag= 0;
	} else if (!isEmail(emailValue)) {
		setErrorFor(email, 'Not a valid email');
        flag= 0;
	} else {
		setSuccessFor(email);
        flag= 1;
	}
	
	if(passwordValue === '') {
		setErrorFor(password, 'Password cannot be blank');
        flag= 0;
	} else {
		setSuccessFor(password);
        flag= 1;
	}
	
	if(password2Value === '') {
		setErrorFor(password2, 'Password cannot be blank');
        flag= 0;
	} else if(passwordValue !== password2Value) {
		setErrorFor(password2, 'Passwords does not match');
        flag= 0;
	} else{
		setSuccessFor(password2);
        flag= 1;
	}


	const mobilevalue=mobile.value.trim();

if (/^\d{10}$/.test(mobilevalue)) {
    setSuccessFor(mobile);
    flag=1;
}
else{
	setErrorFor(mobile, 'Please enter valid number!');
    flag=0;
}




const yobvalue=yob.value.trim();

if(yobvalue ==='' ){
	setErrorFor(yob, 'year cannot be blank');
    flag=0;
}
else if(yobvalue<1960 || yobvalue> (new Date().getFullYear())-5)
{
	setErrorFor(yob, 'Enter valid Year');
    flag=0;
}
else{
	setSuccessFor(yob);
    flag=1;
}


const addressValue=address.value.trim();
if(addressValue === '') {
	setErrorFor(address, 'Address cannot be blank');
	flag=0;
} else {
	setSuccessFor(address);
	flag=1;
}



return Boolean(flag);


}

function setErrorFor(input, message) {
	const formControl = input.parentElement;
	const small = formControl.querySelector('small');
	formControl.className = 'form-control error';
	small.innerText = message;
}

function setSuccessFor(input) {
	const formControl = input.parentElement;
	formControl.className = 'form-control success';
}
	
function isEmail(email) {
	return /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/.test(email);
} -->




	<!-- // const ff=0;

// form.addEventListener('submit', e => {
// 	e.preventDefault();
	
// 	checkInputs();
// 	if (ff===1)
// 	{
// 		(this).submit();
// 	}
// }); -->







	<!-- 
        // </script> -->




	<!-- Form validation code JavaScript -->

	<!-- <script>
    function loginFunction() {
      // Get the value of the input field with id="numb"
      let x = document.getElementById("numb").value;
      // If x is Not a Number or less than one or greater than 10
      let text;
      if (isNaN(x) || x < 1 || x > 10) {
        text = "Input not valid";
      } else {
        text = "Input OK";
      }
      document.getElementById("demo").innerHTML = text;
    }
    </script> -->




</body>

</html>