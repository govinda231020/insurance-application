package com.impetus.insuranceapp.filter;

import java.io.IOException;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.Registration;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.stereotype.Component;

import com.impetus.insuranceapp.model.UserProfile;

@Component
public class FilterProcessUser implements Filter
{

	@Override
	public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain)
			throws IOException, ServletException 
	{
		HttpServletRequest req =(HttpServletRequest)request;
		HttpServletResponse res = (HttpServletResponse)response;
		
		HttpSession session = req.getSession();
		
		UserProfile login = (UserProfile)session.getAttribute("login");
		
		if(login == null) 
		{
			res.sendRedirect("/login");
		}
		else 
		{
			String userType = login.getUserType();
			
			if(!(userType.equalsIgnoreCase("user")))
			{
				res.sendRedirect("/login");
			}
			else
			{
				chain.doFilter(request, response);
			}
		}
		
		// TODO Auto-generated method stub
		
	}

}
