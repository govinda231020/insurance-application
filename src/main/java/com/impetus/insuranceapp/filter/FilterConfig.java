package com.impetus.insuranceapp.filter;

import org.springframework.boot.web.servlet.FilterRegistrationBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class FilterConfig {
	
	@Bean
	public FilterRegistrationBean<FilterProcessUser> registrationBeanUser()
	{
		FilterRegistrationBean<FilterProcessUser> registrationBean = new FilterRegistrationBean<FilterProcessUser>();
		
		registrationBean.setFilter(new FilterProcessUser());
		registrationBean.addUrlPatterns("/user/*");
		
		return registrationBean;
	}
	
	@Bean
	public FilterRegistrationBean<FilterProcessAdmin> registrationBeanAdmin()
	{
		FilterRegistrationBean<FilterProcessAdmin> registrationBean = new FilterRegistrationBean<FilterProcessAdmin>();
		
		registrationBean.setFilter(new FilterProcessAdmin());
		registrationBean.addUrlPatterns("/admin/*");
		
		return registrationBean;
	}
	
	@Bean
	public FilterRegistrationBean<FilterProcessUnderwriter> registrationBeanUnderwriter()
	{
		FilterRegistrationBean<FilterProcessUnderwriter> registrationBean = new FilterRegistrationBean<FilterProcessUnderwriter>();
		
		registrationBean.setFilter(new FilterProcessUnderwriter());
		registrationBean.addUrlPatterns("/underwriter/*");
		
		return registrationBean;
	}
	
}
