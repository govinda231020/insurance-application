package com.impetus.insuranceapp.filter;

import java.io.IOException;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.impetus.insuranceapp.model.UserProfile;

public class FilterProcessUnderwriter implements Filter
{

	@Override
	public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain)
			throws IOException, ServletException {
		// TODO Auto-generated method stub
		HttpServletRequest req =(HttpServletRequest)request;
		HttpServletResponse res = (HttpServletResponse)response;
		
		HttpSession session = req.getSession();
		
		UserProfile login = (UserProfile)session.getAttribute("login");
		
		if(login == null) 
		{
			res.sendRedirect("/login");
		}
		else 
		{
			String userType = login.getUserType();
			
			if(!(userType.equalsIgnoreCase("uwriter")))
			{
				res.sendRedirect("/login");
			}
			else
			{
				chain.doFilter(request, response);
			}
		}
		
		// TODO Auto-generated method stub

		
	}

}
