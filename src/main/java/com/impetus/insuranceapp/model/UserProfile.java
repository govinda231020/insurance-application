package com.impetus.insuranceapp.model;

import java.util.List;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

import org.springframework.beans.factory.annotation.Value;


@Entity
@Table(name = "UserProfile",
	   uniqueConstraints = 
   		{@UniqueConstraint(name = "unique_email",
   		columnNames = {"email"})})
public class UserProfile 
{
	@Id
	@SequenceGenerator(name="id_sequence",sequenceName = "id_sequence",allocationSize = 1)
	@GeneratedValue(strategy = GenerationType.SEQUENCE,generator="id_sequence")
	private Long userId;
//	
//	@OneToMany(cascade = CascadeType.ALL)
//	@JoinColumn(name="user_id",referencedColumnName = "userId")
//	private Set<DepDetails> depDetails;
	
	
	@Column(name="first_name",nullable = false,length=15)
	private String firstName;
	
	@Column(name="last_name",nullable = false,length=15)
	private String lastName;
	
	@Column(name="email",nullable=false,length=30)
	private String email;
	
	@Column(name="password",nullable=false,updatable = false,length=100)
	private String password;
	
	@Column(name="gender",nullable=false,length=10)
	private String gender;
	
	@Column(name="yob",nullable=false)
	private Integer yob;
	
	@Column(name="mobile_no",nullable=false)
	private Long mobileNo;
	
	@Column(name="address",nullable=false,length=20)
	private String address;
	
	@Column(name="annual_income",length=20)
	private String annualIncome;
	
	@Column(name="is_active")
	private boolean isActive =true;
	
	
	@Column(name="user_type",length=10)
	private String userType = "USER";
	
//	@OneToOne(mappedBy = "userProfile")
//	MedicalHistory medicalHistory;

	
	
	
	public UserProfile() 
	{
		
	}
	
	public UserProfile(String firstName, String lastName, String email, String password, String gender,
			Integer yob, Long mobileNo, String address, String annualIncome) {
		super();
		this.firstName = firstName;
		this.lastName = lastName;
		this.email = email;
		this.password = password;
		this.gender = gender;
		this.yob = yob;
		this.mobileNo = mobileNo;
		this.address = address;
		this.annualIncome = annualIncome;

	}
	
	
	//Define Getter and Setter since @ModelAttribute Uses the setter
	/**
	 * @return the userId
	 */
	public Long getUserId() {
		return userId;
	}
	/**
	 * @param userId the userId to set
	 */
	public void setUserId(Long userId) {	
		this.userId = userId;
	}
	/**
	 * @return the firstName
	 */
	public String getFirstName() {
		return firstName;
	}
	/**
	 * @param firstName the firstName to set
	 */
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}
	/**
	 * @return the lastName
	 */
	public String getLastName() {
		return lastName;
	}
	/**
	 * @param lastName the lastName to set
	 */
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}
	/**
	 * @return the email
	 */
	public String getEmail() {
		return email;
	}
	/**
	 * @param email the email to set
	 */
	public void setEmail(String email) {
		this.email = email;
	}
	/**
	 * @return the password
	 */
	public String getPassword() {
		return password;
	}
	/**
	 * @param password the password to set
	 */
	public void setPassword(String password) {
		this.password = password;
	}
	/**
	 * @return the gender
	 */
	public String getGender() {
		return gender;
	}
	/**
	 * @param gender the gender to set
	 */
	public void setGender(String gender) {
		this.gender = gender;
	}
	/**
	 * @return the age
	 */
	public Integer getYob() {
		return yob;
	}
	/**
	 * @param yob the age to set
	 */
	public void setYob(Integer yob) {
		this.yob = yob;
	}
	/**
	 * @return the mobileNo
	 */
	public Long getMobileNo() {
		return mobileNo;
	}
	/**
	 * @param mobileNo the mobileNo to set
	 */
	public void setMobileNo(Long mobileNo) {
		this.mobileNo = mobileNo;
	}
	/**
	 * @return the address
	 */
	public String getAddress() {
		return address;
	}
	/**
	 * @param address the address to set
	 */
	public void setAddress(String address) {
		this.address = address;
	}
	/**
	 * @return the annualIncome
	 */
	public String getAnnualIncome() {
		return annualIncome;
	}
	/**
	 * @param annualIncome the annualIncome to set
	 */
	public void setAnnualIncome(String annualIncome) {
		this.annualIncome = annualIncome;
	}
	/**
	 * @return the isActive
	 */
	public boolean isActive() {
		return isActive;
	}
	/**
	 * @param isActive the isActive to set
	 */
	public void setActive(boolean isActive) {
		this.isActive = isActive;
	}
	/**
	 * @return the userType
	 */
	public String getUserType() {
		return userType;
	}
	/**
	 * @param userType the userType to set
	 */
	public void setUserType(String userType) {
		this.userType = userType;
	}

	

	
	
	
	
}
