package com.impetus.insuranceapp.model;

import java.io.Serializable;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;


@Entity
@Table(name="medical_history")
public class MedicalHistory 
{
//	@Id	
	//By This way Uni - directional from Medical History to User Profile Mapping will get created.
//	@OneToOne(cascade = CascadeType.ALL,fetch = FetchType.EAGER) //User Profile will get auto matically fetched whenever we fetch medical history.
	
	@Id
	@SequenceGenerator(name="med_sequence",sequenceName = "med_sequence",allocationSize = 1)
	@GeneratedValue(strategy = GenerationType.SEQUENCE,generator="med_sequence")
	@Column(name="med_id")
	private Long medId;
	
	@OneToOne
	@JoinColumn(
			name = "user_id",
			referencedColumnName = "userId"
			)
	private UserProfile userProfile;
	
	@OneToOne
	@JoinColumn(name="dep_id",referencedColumnName = "depId")
	private DepDetails depDetail;
	
	/**
	 * @return the depDetail
	 */
	public DepDetails getDepDetail() {
		return depDetail;
	}

	/**
	 * @param depDetail the depDetail to set
	 */
	public void setDepDetail(DepDetails depDetail) {
		this.depDetail = depDetail;
	}

	@Column(name="diabetes",length=10)
	private String diabetes;
	
	@Column(name="heart_ailments",length=10)
	private String heartAilments;
	
	@Column(name="tooth_decay",length=10)
	private String toothDecay;
	
	@Column(name="covid",length=10)
	private String covid;
	
	@Column(name="cancer",length=10)
	private String cancer;
	
	@Column(name="cataract",length=10)
	private String cataract;
	
	
	public MedicalHistory() 	
	{
		
	}
	
	public MedicalHistory(UserProfile userProfile, String diabetes, String heartAilments, String toothDecay,
			String covid, String cancer,String cataract,DepDetails depDetail) {
		super();
		this.userProfile = userProfile;
		this.diabetes = diabetes;
		this.heartAilments = heartAilments;
		this.toothDecay = toothDecay;
		this.covid = covid;
		this.cancer = cancer;
		this.cataract = cataract;
		this.depDetail = depDetail;
	}

	/**
	 * @return the medId
	 */
	public Long getMedId() {
		return medId;
	}

	/**
	 * @param medId the medId to set
	 */
	public void setMedId(Long medId) {
		this.medId = medId;
	}

	/**
	 * @return the cataract
	 */
	public String getCataract() {
		return cataract;
	}

	/**
	 * @param cataract the cataract to set
	 */
	public void setCataract(String cataract) {
		this.cataract = cataract;
	}

	/**
	 * @return the userProfile
	 */
	public UserProfile getUserProfile() {
		return userProfile;
	}

	/**
	 * @param userProfile the userProfile to set
	 */
	public void setUserProfile(UserProfile userProfile) {
		this.userProfile = userProfile;
	}

	/**
	 * @return the diabetes
	 */
	public String getDiabetes() 
	{
		return diabetes;
	}

	/**
	 * @param diabetes the diabetes to set
	 */
	public void setDiabetes(String diabetes) 
	{
		this.diabetes = diabetes;
	}

	/**
	 * @return the heartAilments
	 */
	public String getHeartAilments() 
	{
		return heartAilments;
	}

	/**
	 * @param heartAilments the heartAilments to set
	 */
	public void setHeartAilments(String heartAilments) {
		this.heartAilments = heartAilments;
	}

	/**
	 * @return the toothDecay
	 */
	public String getToothDecay() {
		return toothDecay;
	}

	/**
	 * @param toothDecay the toothDecay to set
	 */
	public void setToothDecay(String toothDecay) {
		this.toothDecay = toothDecay;
	}

	/**
	 * @return the covid
	 */
	public String getCovid() {
		return covid;
	}

	/**
	 * @param covid the covid to set
	 */
	public void setCovid(String covid) {
		this.covid = covid;
	}

	/**
	 * @return the cancer
	 */
	public String getCancer() {
		return cancer;
	}

	/**
	 * @param cancer the cancer to set
	 */
	public void setCancer(String cancer) {
		this.cancer = cancer;
	}

	
	
	
	
}
