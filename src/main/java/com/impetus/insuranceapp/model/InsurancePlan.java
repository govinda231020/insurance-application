package com.impetus.insuranceapp.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Entity
@Table(name="InsurancePlan")
public class InsurancePlan 
{
	@Id
	@SequenceGenerator(name="ins_sequence",sequenceName = "ins_sequence",allocationSize = 1)
	@GeneratedValue(strategy = GenerationType.SEQUENCE,generator="ins_sequence")
	private Long insuranceId;
	
	@Column(nullable=false,length=10)
	private String insuranceType;
	
	@Column(nullable=false,length=100)
	private String insuranceCompName;
	
	@Column(nullable=false,length=250)
	private String insPlanDetail;
	
	@Column(nullable=false)
	private Long basePrice;
	
	@Column(nullable=false)
	private int minAge;
	
//	private int maxAge;
	@Column(nullable=false)
	private Long minAnnualIncome;
	
	@Column(nullable=false)
	private Long coverageAmount;
	
	@Column(nullable=false)
	private int coverageAge;
	
	@Column(name="is_activeplan")
	private boolean isActiveplan =true;
	
	

	public InsurancePlan(String insuranceType, String insuranceCompName, String insPlanDetail,
			Long basePrice, int minAge, Long minAnnualIncome, Long coverageAmount, int coverageAge,
			boolean isActiveplan) {
		super();
		this.insuranceType = insuranceType;
		this.insuranceCompName = insuranceCompName;
		this.insPlanDetail = insPlanDetail;
		this.basePrice = basePrice;
		this.minAge = minAge;
		this.minAnnualIncome = minAnnualIncome;
		this.coverageAmount = coverageAmount;
		this.coverageAge = coverageAge;
		this.isActiveplan = isActiveplan;
	}

	

	public boolean isActiveplan() {
		return isActiveplan;
	}



	public void setActiveplan(boolean isActiveplan) {
		this.isActiveplan = isActiveplan;
	}



	public InsurancePlan() {
		super();
	}

	/**
	 * @return the insuranceId
	 */
	public Long getInsuranceId() {
		return insuranceId;
	}

	/**
	 * @param insuranceId the insuranceId to set
	 */
	public void setInsuranceId(Long insuranceId) {
		this.insuranceId = insuranceId;
	}

	/**
	 * @return the insuranceType
	 */
	public String getInsuranceType() {
		return insuranceType;
	}

	/**
	 * @param insuranceType the insuranceType to set
	 */
	public void setInsuranceType(String insuranceType) {
		this.insuranceType = insuranceType;
	}

	/**
	 * @return the insuranceCompName
	 */
	public String getInsuranceCompName() {
		return insuranceCompName;
	}

	/**
	 * @param insuranceCompName the insuranceCompName to set
	 */
	public void setInsuranceCompName(String insuranceCompName) {
		this.insuranceCompName = insuranceCompName;
	}

	/**
	 * @return the insPlanDetail
	 */
	public String getInsPlanDetail() {
		return insPlanDetail;
	}

	/**
	 * @param insPlanDetail the insPlanDetail to set
	 */
	public void setInsPlanDetail(String insPlanDetail) {
		this.insPlanDetail = insPlanDetail;
	}

	/**
	 * @return the basePrice
	 */
	public Long getBasePrice() {
		return basePrice;
	}

	/**
	 * @param basePrice the basePrice to set
	 */
	public void setBasePrice(Long basePrice) {
		this.basePrice = basePrice;
	}

	/**
	 * @return the minAge
	 */
	public int getMinAge() {
		return minAge;
	}

	/**
	 * @param minAge the minAge to set
	 */
	public void setMinAge(int minAge) {
		this.minAge = minAge;
	}

	/**
	 * @return the minAnnualIncome
	 */
	public Long getMinAnnualIncome() {
		return minAnnualIncome;
	}

	/**
	 * @param minAnnualIncome the minAnnualIncome to set
	 */
	public void setMinAnnualIncome(Long minAnnualIncome) {
		this.minAnnualIncome = minAnnualIncome;
	}

	/**
	 * @return the coverageAmount
	 */
	public Long getCoverageAmount() {
		return coverageAmount;
	}

	/**
	 * @param coverageAmount the coverageAmount to set
	 */
	public void setCoverageAmount(Long coverageAmount) {
		this.coverageAmount = coverageAmount;
	}

	/**
	 * @return the coverageAge
	 */
	public int getCoverageAge() {
		return coverageAge;
	}

	/**
	 * @param coverageAge the coverageAge to set
	 */
	public void setCoverageAge(int coverageAge) {
		this.coverageAge = coverageAge;
	}
	
	
	
	
}
