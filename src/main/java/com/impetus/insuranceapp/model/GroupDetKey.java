package com.impetus.insuranceapp.model;

import java.io.Serializable;

import javax.persistence.Embeddable;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;


@Embeddable
public class GroupDetKey implements Serializable
{
	@ManyToOne
	@JoinColumn(name="policy_id",referencedColumnName = "policyId")
	PolicyDetail policydet; 
	
	@ManyToOne
	@JoinColumn(name="dep_id",referencedColumnName = "depId")
	DepDetails depdet;

	public GroupDetKey(PolicyDetail policydet, DepDetails depdet) {
		super();
		this.policydet = policydet;
		this.depdet = depdet;
	}

	public GroupDetKey() {
		super();
	}

	/**
	 * @return the policydet
	 */
	public PolicyDetail getPolicydet() {
		return policydet;
	}

	/**
	 * @param policydet the policydet to set
	 */
	public void setPolicydet(PolicyDetail policydet) {
		this.policydet = policydet;
	}

	/**
	 * @return the depdet
	 */
	public DepDetails getDepdet() {
		return depdet;
	}

	/**
	 * @param depdet the depdet to set
	 */
	public void setDepdet(DepDetails depdet) {
		this.depdet = depdet;
	}

	@Override
	public int hashCode() {
		// TODO Auto-generated method stub
		return super.hashCode();
	}

	@Override
	public boolean equals(Object obj) {
		// TODO Auto-generated method stub
		return super.equals(obj);
	}
	
		
}
