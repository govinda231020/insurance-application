package com.impetus.insuranceapp.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;

import org.springframework.beans.factory.annotation.Value;


@Entity
public class PolicyDetail 
{
	@Id
	@SequenceGenerator(name="policy_seq",sequenceName = "policy_seq",allocationSize = 1)
	@GeneratedValue(strategy = GenerationType.SEQUENCE,generator="policy_seq")
	Long policyId;
	
	@ManyToOne
	@JoinColumn(name="user_id",referencedColumnName = "userId")
	UserProfile user;
	
	@ManyToOne
	@JoinColumn(name="insurance_id",referencedColumnName = "insuranceId")
	InsurancePlan insurance;
	
	@Column(name="policy_status",length=15)
	@Value("${some.key:Pending}")
	private String policyStatus;
	
	@Column(name="medical_history",length=10)
	private String medicalHistory;
	
	@Column(name="start_date",length=20)
	private String date;
	
	@Column(name="group_present")
	private boolean isGroupPresent;
	
	@Column(name="policy_cost")
	private Integer policyCost;
	
	@Column(name="approval",length=20)
	private String approval;
	
	@Column(name="policy_end_date",length=20)
	private String policyEndDate;

	


	public PolicyDetail(UserProfile user, InsurancePlan insurance, String policyStatus,
			String medicalHistory, String date, boolean isGroupPresent, Integer policyCost) {
		super();
		this.policyId = policyId;
		this.user = user;
		this.insurance = insurance;
		this.policyStatus = policyStatus;
		this.medicalHistory = medicalHistory;
		this.date = date;
		this.isGroupPresent = isGroupPresent;
		this.policyCost = policyCost;
	}

	public PolicyDetail() {
		super();
	}

	/**
	 * @return the policyId
	 */
	public Long getPolicyId() {
		return policyId;
	}

	/**
	 * @param policyId the policyId to set
	 */
	public void setPolicyId(Long policyId) {
		this.policyId = policyId;
	}

	/**
	 * @return the user
	 */
	public UserProfile getUser() {
		return user;
	}

	/**
	 * @param user the user to set
	 */
	public void setUser(UserProfile user) {
		this.user = user;
	}

	/**
	 * @return the insurance
	 */
	public InsurancePlan getInsurance() {
		return insurance;
	}

	/**
	 * @param insurance the insurance to set
	 */
	public void setInsurance(InsurancePlan insurance) {
		this.insurance = insurance;
	}

	/**
	 * @return the policyStatus
	 */
	public String getPolicyStatus() {
		return policyStatus;
	}

	/**
	 * @param policyStatus the policyStatus to set
	 */
	public void setPolicyStatus(String policyStatus) {
		this.policyStatus = policyStatus;
	}

	/**
	 * @return the medicalHistory
	 */
	public String getMedicalHistory() {
		return medicalHistory;
	}

	/**
	 * @param medicalHistory the medicalHistory to set
	 */
	public void setMedicalHistory(String medicalHistory) {
		this.medicalHistory = medicalHistory;
	}

	/**
	 * @return the date
	 */
	public String getDate() {
		return date;
	}

	/**
	 * @param date the date to set
	 */
	public void setDate(String date) {
		this.date = date;
	}

	/**
	 * @return the isGroupPresent
	 */
	public boolean getIsGroupPresent() {
		return isGroupPresent;
	}

	/**
	 * @param isGroupPresent the isGroupPresent to set
	 */
	public void setIsGroupPresent(boolean isGroupPresent) {
		this.isGroupPresent = isGroupPresent;
	}

	/**
	 * @return the policyCost
	 */
	public Integer getPolicyCost() {
		return policyCost;
	}

	/**
	 * @param policyCost the policyCost to set
	 */
	public void setPolicyCost(Integer policyCost) {
		this.policyCost = policyCost;
	}

	public String getApproval() {
		return approval;
	}

	public void setApproval(String approval) {
		this.approval = approval;
	}
	/**
	 * @return the policyEndDate
	 */
	public String getPolicyEndDate() {
		return policyEndDate;
	}

	/**
	 * @param policyEndDate the policyEndDate to set
	 */
	public void setPolicyEndDate(String policyEndDate) {
		this.policyEndDate = policyEndDate;
	}

	/**
	 * @param isGroupPresent the isGroupPresent to set
	 */
	public void setGroupPresent(boolean isGroupPresent) {
		this.isGroupPresent = isGroupPresent;
	}



	
	
}
