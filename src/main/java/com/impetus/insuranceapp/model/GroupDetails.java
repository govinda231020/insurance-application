package com.impetus.insuranceapp.model;

import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.IdClass;

@Entity
public class GroupDetails 
{
	@EmbeddedId
	GroupDetKey Id;

	public GroupDetails(GroupDetKey id) {
		super();
		Id = id;
	}

	/**
	 * @return the id
	 */
	public GroupDetKey getId() {
		return Id;
	}

	/**
	 * @param id the id to set
	 */
	public void setId(GroupDetKey id) {
		Id = id;
	}

	public GroupDetails() {
		super();
	}
	
}
