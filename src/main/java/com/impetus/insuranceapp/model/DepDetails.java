package com.impetus.insuranceapp.model;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import org.hibernate.annotations.ManyToAny;

@Entity
@Table(name="dependent_details")

public class DepDetails 
{
	@ManyToOne
	@JoinColumn(name="user_id",referencedColumnName = "userId")
	UserProfile userProfile;
	
	@Id
	@SequenceGenerator(name="dep_id_seq",sequenceName = "dep_id_seq",allocationSize = 1)
	@GeneratedValue(strategy = GenerationType.SEQUENCE,generator="dep_id_seq")
	private Long depId;
	

	private String depFullName;
	
	private String depRelation; 
	
	private Integer depyob;
	
	private String gender;

	public DepDetails(UserProfile userProfile, String depFullName, String depRelation, Integer depyob,
			String gender) {
		super();
		this.userProfile = userProfile;
		this.depFullName = depFullName;
		this.depRelation = depRelation;
		this.depyob = depyob;
		this.gender = gender;
	}

	

	public String getGender() {
		return gender;
	}



	public void setGender(String gender) {
		this.gender = gender;
	}



	public DepDetails() {
		super();
	}

	/**			
	 * @return the userProfile
	 */
	public UserProfile getUserProfile() {
		return userProfile;
	}

	/**
	 * @param userProfile the userProfile to set
	 */
	public void setUserProfile(UserProfile userProfile) {
		this.userProfile = userProfile;
	}

	/**
	 * @return the depId
	 */
	public Long getDepId() {
		return depId;
	}

	/**
	 * @param depId the depId to set
	 */
	public void setDepId(Long depId) {
		this.depId = depId;
	}

	/**
	 * @return the depFullName
	 */
	public String getDepFullName() {
		return depFullName;
	}

	/**
	 * @param depFullName the depFullName to set
	 */
	public void setDepFullName(String depFullName) {
		this.depFullName = depFullName;
	}

	/**
	 * @return the depRelation
	 */
	public String getDepRelation() {
		return depRelation;
	}

	/**
	 * @param depRelation the depRelation to set
	 */
	public void setDepRelation(String depRelation) {
		this.depRelation = depRelation;
	}

	/**
	 * @return the depyob
	 */
	public Integer getDepyob() {
		return depyob;
	}

	/**
	 * @param depyob the depyob to set
	 */
	public void setDepyob(Integer depyob) {
		this.depyob = depyob;
	}

	
	
	
}
