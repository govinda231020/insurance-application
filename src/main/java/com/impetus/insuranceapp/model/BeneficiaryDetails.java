package com.impetus.insuranceapp.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.SequenceGenerator;

@Entity
public class BeneficiaryDetails 
{
	/**
	 * 
	 */
	
	@Id
	@SequenceGenerator(name="ben_sequence",sequenceName = "ben_sequence",allocationSize = 1)
	@GeneratedValue(strategy = GenerationType.SEQUENCE,generator="ben_sequence")
	@Column(name="ben_id")
	private Long benId;
	

	@OneToOne
	@JoinColumn(name="policy_id",referencedColumnName = "policyId")
	PolicyDetail policyDet;
	 
	/**
	 * @return the userId
	 */
	public Long getUserId() {
		return benId;
	}



	/**
	 * @param userId the userId to set
	 */
	public void setUserId(Long benId) {
		this.benId = benId;
	}

	private String benRelation;
	
	private String benName;
	
	private String emailAddress;
	
	private Long benMobNo;
	
	private String gender;

	public BeneficiaryDetails() {
		super();
	}
	
	
	
	public BeneficiaryDetails(PolicyDetail policyDet, String benRelation, String benName,
			String emailAddress, Long benMobNo, String gender) {
		super();
		this.policyDet = policyDet;
		this.benRelation = benRelation;
		this.benName = benName;
		this.emailAddress = emailAddress;
		this.benMobNo = benMobNo;
		this.gender = gender;
	}




	public String getGender() {
		return gender;
	}



	public void setGender(String gender) {
		this.gender = gender;
	}



	/**
	 * @return the policyDet
	 */
	public PolicyDetail getPolicyDet() {
		return policyDet;
	}

	/**
	 * @param policyDet the policyDet to set
	 */
	public void setPolicyDet(PolicyDetail policyDet) {
		this.policyDet = policyDet;
	}

	/**
	 * @return the benRelation
	 */
	public String getBenRelation() {
		return benRelation;
	}

	/**
	 * @param benRelation the benRelation to set
	 */
	public void setBenRelation(String benRelation) {
		this.benRelation = benRelation;
	}

	/**
	 * @return the benName
	 */
	public String getBenName() {
		return benName;
	}

	/**
	 * @param benName the benName to set
	 */
	public void setBenName(String benName) {
		this.benName = benName;
	}

	/**
	 * @return the emailAddress
	 */
	public String getEmailAddress() {
		return emailAddress;
	}

	/**
	 * @param emailAddress the emailAddress to set
	 */
	public void setEmailAddress(String emailAddress) {
		this.emailAddress = emailAddress;
	}

	/**
	 * @return the benMobNo
	 */
	public Long getBenMobNo() {
		return benMobNo;
	}

	/**
	 * @param benMobNo the benMobNo to set
	 */
	public void setBenMobNo(Long benMobNo) {
		this.benMobNo = benMobNo;
	}

	
	
	
}
