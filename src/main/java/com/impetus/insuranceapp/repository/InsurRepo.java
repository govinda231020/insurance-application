package com.impetus.insuranceapp.repository;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.impetus.insuranceapp.model.InsurancePlan;
import com.impetus.insuranceapp.model.PolicyDetail;

@Repository
public interface InsurRepo extends JpaRepository<InsurancePlan, Long> 
{
	@Modifying
	@Transactional
	@Query("update InsurancePlan u set u.insuranceCompName=?1,u.insPlanDetail=?2,u.basePrice=?3,u.minAge=?4,u.minAnnualIncome=?5, u.coverageAmount=?6, u.coverageAge=?7 where u.insuranceId=?8")
	public Integer updateInsurance(String insuranceCompName,String insPlanDetail,Long basePrice,int minAge,Long minAnnualIncome,Long coverageAmount,int coverageAge, Long Iid);


	@Query("select s from InsurancePlan s where s.insuranceType =:e AND isActiveplan=true")
	public java.util.List<InsurancePlan> getInsuranceByType(@Param("e") String s);


	
	@Modifying
	@Transactional
	@Query("update InsurancePlan u set u.isActiveplan=?1 where u.insuranceId=?2")
	public Integer adminupdateStatus(Boolean b,Long insid);
	
	
	@Query(value="SELECT * FROM insurance_plan where is_activeplan=true",nativeQuery=true)
	public List<InsurancePlan> getallActivePlans();
	
	@Query(value="SELECT * FROM insurance_plan where insurance_comp_name=?1",nativeQuery=true)
	public InsurancePlan getByNameCheck(String insName);
	
	@Query(value="SELECT * FROM insurance_plan where insurance_type=?1",nativeQuery=true)
	public InsurancePlan getByTypeCheck(String insType);
	
}
