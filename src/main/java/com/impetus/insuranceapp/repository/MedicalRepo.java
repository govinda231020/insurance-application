package com.impetus.insuranceapp.repository;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;

import com.impetus.insuranceapp.model.MedicalHistory;
import com.impetus.insuranceapp.model.UserProfile;

public interface MedicalRepo extends JpaRepository<MedicalHistory, Long> 
{
	@Query(value="SELECT * FROM medical_history WHERE user_id=?1",nativeQuery = true)
	List<MedicalHistory> findByUserCustom(Long user_id);

	
	@Modifying
	@Transactional
//	@Query("update MedicalHistory u set u.heartAilments=?1,u.diabetes=?2,u.cataract=?3,u.toothDecay=?4,u.covid=?5,u.cancer=?6 where u.userProfile=?7")
	@Query(value="UPDATE medical_history SET heart_ailments=?1,diabetes=?2,cataract=?3,tooth_decay=?4,covid=?5,cancer=?6 WHERE user_id=?7",nativeQuery=true)
	public Integer updateMedical(String heart,String diabetes,String cataract,String tooth,String covid,String cancer,Long userid);
	
	
	
	@Query(value="SELECT * FROM medical_history where dep_id=?1",nativeQuery=true)
	MedicalHistory findByDepCustom(Long dep_id);
	
	@Modifying
	@Transactional
	@Query(value="UPDATE medical_history SET heart_ailments=?1,diabetes=?2,cataract=?3,tooth_decay=?4,covid=?5,cancer=?6 WHERE dep_id=?7",nativeQuery=true)
	public Integer updateDep(String heart,String diabetes,String cataract,String tooth,String covid,String cancer,Long depid);

}
