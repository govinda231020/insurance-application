package com.impetus.insuranceapp.repository;



import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.impetus.insuranceapp.model.BeneficiaryDetails;

import com.impetus.insuranceapp.model.PolicyDetail;



public interface BeneficiaryRepo extends JpaRepository<BeneficiaryDetails,PolicyDetail>
{
	@Query(value="SELECT * FROM beneficiary_details WHERE policy_id=?1",nativeQuery = true)
     BeneficiaryDetails findBeniByPolyId(Long Poly_id);
}
