package com.impetus.insuranceapp.repository;

import java.security.Policy;
import java.util.List;
import java.util.Map;

import javax.transaction.Transactional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;

import com.impetus.insuranceapp.model.GroupDetails;
import com.impetus.insuranceapp.model.PolicyDetail;

public interface PolicyRepo extends JpaRepository<PolicyDetail, Long> 
{
	@Query(value="SELECT * FROM policy_detail WHERE user_id=?1",nativeQuery = true)
	List<PolicyDetail> findPoliciesByUserCustom(Long user_id);
	
	@Query(value="SELECT * FROM policy_detail WHERE policy_status in ('Approved','Rejected') AND approval='Manual'",nativeQuery = true)
	List<PolicyDetail> findManualStatusPolicies();
	
	@Query(value="SELECT * FROM policy_detail WHERE policy_status=?1",nativeQuery = true)
	List<PolicyDetail> findPoliciesForManual(String policy_status);
	
	
	@Query(value="SELECT * FROM policy_detail WHERE policy_status='Pending'",nativeQuery=true)
	public List<PolicyDetail> getPendingReq();
	
	@Modifying
	@Transactional
	@Query("update PolicyDetail u set u.policyStatus=?1 where u.policyId=?2")
	public Integer underupdateStatus(String b,Long pid);
	
	
	@Query(value="SELECT * FROM policy_detail WHERE insurance_id=?1",nativeQuery = true)
	List<PolicyDetail> findPolicyByInsId(Long insurance_id);
	
	@Query(value="SELECT insurance_id FROM policy_detail GROUP BY insurance_id",nativeQuery=true)
	public List<Integer> getCountOfUserByInsurance();
	

	@Query(value="SELECT count(insurance_id) FROM policy_detail WHERE insurance_id = ?1",nativeQuery=true)
	public Integer getCountById(Long insId);
	
	
}
