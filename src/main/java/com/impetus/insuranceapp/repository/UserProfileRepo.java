package com.impetus.insuranceapp.repository;

import javax.transaction.Transactional;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
//import org.springframework.data.jpa.repository.Query;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.impetus.insuranceapp.model.UserProfile;



public interface UserProfileRepo extends JpaRepository<UserProfile, Long>
{
	
	
	@Query("select s from UserProfile s where s.email =:e")
	public UserProfile getUserByEmail(@Param("e") String s);
	
	@Modifying
	@Transactional
	@Query("update UserProfile u set u.firstName=?1,u.lastName=?2,u.mobileNo=?3,u.address=?4,u.annualIncome=?5 where u.email=?6")
	public Integer updateUser(String firstName,String lastName,Long mobileNo,String address,String annualInc,String email);
	
	@Modifying
	@Transactional
	@Query("update UserProfile u set u.isActive=?1 where u.userId=?2")
	public Integer adminupdateStatus(Boolean b,Long uid);
	
	
	@Query("select s from UserProfile s where s.userType =:e")
	public List<UserProfile> getUserByType(@Param("e") String s);
	
}
