package com.impetus.insuranceapp.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.impetus.insuranceapp.model.GroupDetKey;
import com.impetus.insuranceapp.model.GroupDetails;

public interface GroupRepo extends JpaRepository<GroupDetails,GroupDetKey>
{
	@Query(value="SELECT * FROM group_details WHERE policy_id=?1",nativeQuery = true)
	List<GroupDetails> findgroupByPolicy(Long policy_id);
	
	@Query(value="SELECT * FROM group_details WHERE dep_id=?1",nativeQuery = true)
	List<GroupDetails> findPolicyByDept(Long dep_id);
}
