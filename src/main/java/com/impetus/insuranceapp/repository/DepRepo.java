package com.impetus.insuranceapp.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.impetus.insuranceapp.model.DepDetails;
import com.impetus.insuranceapp.model.UserProfile;

public interface DepRepo extends JpaRepository<DepDetails, Long> 
{
	@Query(value="SELECT * FROM dependent_details WHERE user_id=?1",nativeQuery = true)
	List<DepDetails> findCustom(Long user_id);
}
