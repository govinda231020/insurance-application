package com.impetus.insuranceapp;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.web.servlet.support.SpringBootServletInitializer;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import com.impetus.insuranceapp.service.EmailSenderService;


@EnableAutoConfiguration
@SpringBootApplication
@EnableScheduling
@RestController
public class InsuranceAppApplication extends SpringBootServletInitializer{

	
	@Autowired
	private EmailSenderService service;
	public static void main(String[] args) {
		SpringApplication.run(InsuranceAppApplication.class, args);
	}
	@Override
	protected SpringApplicationBuilder configure(SpringApplicationBuilder builder) {
		// TODO Auto-generated method stub
		return builder.sources(InsuranceAppApplication.class);
	}
	
	@GetMapping("/greeting")
	public String greet() 
	{
		return "Hey finally it is running...";
	}

	
	/**
	 * In order to trigger a mail if underwriter not responds to a pending request.
	 */
//	@EventListener(ApplicationReadyEvent.class)
	@Scheduled(fixedRate = 4*60*60*1000) //send it after every 4 Hours
//	@Scheduled(fixedRate = 5000) //Send it after every 5 seconds if any pending requests are present {For Testing}
	public void triggerMail()
	{
		service.sendSimpleEmail("anmolg9876@gmail.com", "Hey,Underwriter this is to inform you please take action for pending requests...", "Requests are pending");
	}
}
