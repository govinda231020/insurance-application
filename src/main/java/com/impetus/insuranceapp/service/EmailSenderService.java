package com.impetus.insuranceapp.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.impetus.insuranceapp.repository.PolicyRepo;

import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;



@Service
public class EmailSenderService {
	
	@Autowired
	PolicyRepo policyRepo;
	

	public EmailSenderService() {
	}

	@Autowired
	private JavaMailSender mailSender;
	
	public void sendSimpleEmail(String toEmail,String body,String subject)
	{
		if(!(policyRepo.getPendingReq().isEmpty()))
		{
			SimpleMailMessage message = new SimpleMailMessage();
			message.setFrom("saxenagovinda@gmail.com");
			message.setTo(toEmail);
			message.setText(body);
			message.setSubject(subject);
			
			mailSender.send(message);
		}
	}
	
	
	
}
