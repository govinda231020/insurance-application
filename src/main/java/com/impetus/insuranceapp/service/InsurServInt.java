package com.impetus.insuranceapp.service;

import java.util.List;

import org.springframework.data.jpa.repository.Query;

import com.impetus.insuranceapp.model.InsurancePlan;

public interface InsurServInt 
{
	List<InsurancePlan> findAll();
	
//	@Query(value = "select Obj from InsurancePlan Obj where Obj.insurance_type=?1")
//	List<InsurancePlan> findAllByType(String insType);
}
