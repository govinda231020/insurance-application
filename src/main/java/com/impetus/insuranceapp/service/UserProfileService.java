package com.impetus.insuranceapp.service;


import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.impetus.insuranceapp.model.UserProfile;
import com.impetus.insuranceapp.web.dto.UserProfileDto;

public interface UserProfileService 
{
	UserProfile save(UserProfileDto userProfileDTO);
	
	public UserProfile getUserByEmail(@Param("e") String s);
	
}
