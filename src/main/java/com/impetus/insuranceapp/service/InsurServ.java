package com.impetus.insuranceapp.service;

import java.util.List;

import org.springframework.stereotype.Service;

import com.impetus.insuranceapp.model.InsurancePlan;
import com.impetus.insuranceapp.repository.InsurRepo;

@Service
public class InsurServ implements InsurServInt
{
	InsurRepo insurRepo;
	
	
	public InsurServ(InsurRepo insurRepo) {
		super();
		this.insurRepo = insurRepo;
	}


	@Override
	public List<InsurancePlan> findAll() 
	{
		// TODO Auto-generated method stub
		return insurRepo.findAll();
	}

	
	
	public InsurancePlan save(InsurancePlan insplan) {
		return insurRepo.save(insplan);
	}
	
	public void delete(Long id)
	{
		insurRepo.deleteById(id);
	}
	
	
	
public List<InsurancePlan> findAllByType(String s){
		
		return insurRepo.getInsuranceByType(s);
	}
//
//	@Override
//	public List<InsurancePlan> findAllByType(String insType) {
//		// TODO Auto-generated method stub
//		return null;
//	}
	
}
