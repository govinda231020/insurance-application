package com.impetus.insuranceapp.service;

import org.springframework.stereotype.Service;

import com.impetus.insuranceapp.model.UserProfile;
import com.impetus.insuranceapp.repository.UserProfileRepo;
import com.impetus.insuranceapp.web.dto.UserProfileDto;

@Service
public class UserProfileServiceImple implements UserProfileService
{
	UserProfileRepo userProfileRepo;
	
	
	public UserProfileServiceImple(UserProfileRepo userProfileRepo) {
		super();
		this.userProfileRepo = userProfileRepo;
	}
	

	@Override
	public UserProfile save(UserProfileDto userProfileDto) 
	{
		UserProfile userProfile = new UserProfile(userProfileDto.getFirstName(),userProfileDto.getLastName(),userProfileDto.getEmail(),userProfileDto.getPassword(),userProfileDto.getGender(),userProfileDto.getYob(),userProfileDto.getMobileNo(),userProfileDto.getAddress(),userProfileDto.getAnnualIncome());
		// TODO Auto-generated method stub
		return userProfileRepo.save(userProfile);
	}


	@Override
	public UserProfile getUserByEmail(String email) 
	{
		// TODO Auto-generated method stub
		return userProfileRepo.getUserByEmail(email);
	}
	
	

//
//	public String getPasswordByEmail(LoginDto loginDto) {
//		String emailEntered = loginDto.getEmail();
//		// TODO Auto-generated method stub
//		return userProfileRepo.getUserByEmail(emailEntered);
//	}
	
}
