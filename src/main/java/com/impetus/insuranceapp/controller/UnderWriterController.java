package com.impetus.insuranceapp.controller;

import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import com.impetus.insuranceapp.model.GroupDetails;
import com.impetus.insuranceapp.model.MedicalHistory;
import com.impetus.insuranceapp.model.PolicyDetail;
import com.impetus.insuranceapp.model.UserProfile;
import com.impetus.insuranceapp.repository.BeneficiaryRepo;
import com.impetus.insuranceapp.repository.DepRepo;
import com.impetus.insuranceapp.repository.GroupRepo;
import com.impetus.insuranceapp.repository.InsurRepo;
import com.impetus.insuranceapp.repository.MedicalRepo;
import com.impetus.insuranceapp.repository.PolicyRepo;
import com.impetus.insuranceapp.repository.UserProfileRepo;

@Controller
public class UnderWriterController {

	private PolicyRepo polyrepo;
	private UserProfileRepo userrepo;
	private InsurRepo insplan;
	private GroupRepo grpdet;
	private MedicalRepo med;
	private DepRepo deprepo;
	private BeneficiaryRepo benRepo;
	
	public UnderWriterController(PolicyRepo polyrepo, UserProfileRepo userrepo, InsurRepo insplan, GroupRepo grpdet,MedicalRepo med,DepRepo deprepo,BeneficiaryRepo benRepo) {
		super();
		this.polyrepo = polyrepo;
		this.userrepo = userrepo;
		this.insplan = insplan;
		this.grpdet = grpdet;
		this.med=med;
		this.deprepo=deprepo;
		this.benRepo=benRepo;
	}

	/**
	 * This methods is there for giving us underwriter's Home page
	 * @return underwriter's home page
	 */
	@RequestMapping("/underwriter/homepage")
	public String userAdminPage() 
	{	
		return "underwriterHome";
	}
	
	/**
	 * This Method Process and displays pending policies present in database
	 * @param m for sending all pending requests to page
	 * @return underwriter's pending requests page
	 */
	@RequestMapping("/underwriter/pending")
	public String userAdminPendingPage(Model m) 
	{	
		m.addAttribute("m",polyrepo.findPoliciesForManual("pending"));	
		return "underwriterPending";
	}
	
	
	/** 
	 * This Method is there to approve the pending requests 
	 * @param but to change status to approved when approve get clicked
	 * @param m for again finding the pending policies 
	 * @return underwriter's pending requests page
	 */
	@RequestMapping("/underwriter/approved")
	public String underwriterActionApprove(@RequestParam("button") String but,Model m) 
	{
	
		Long pid =Long.parseLong(but);
		polyrepo.underupdateStatus("Approved", pid);
		m.addAttribute("m",polyrepo.findPoliciesForManual("pending"));
		String s="Policy id "+pid+" approved";
		m.addAttribute("statuschangesmsg", s);
		return "underwriterPending";
//		return "redirect:/underwriter/homepage";
	}
	/** 
	 * This method rejects pending requests 
	 * @param but to update status to reject whenever rejected button gets clicked
	 * @param m for again finding the pending policies
	 * @return underwriter's pending page
	 */
	@RequestMapping("/underwriter/reject")
	public String underwriterRejectApprove(@RequestParam("button") String but,Model m) 
	{
		
		Long pid =Long.parseLong(but);
		polyrepo.underupdateStatus("Rejected", pid);
		m.addAttribute("m",polyrepo.findPoliciesForManual("pending"));
		String s="Policy id "+pid+" rejected";
		m.addAttribute("statuschangesmsg", s);
		return "underwriterPending";
	}
	
	/** 
	 * this method redirects to complete information of policy
	 * @param but for getting the policy id 
	 * @param model for sending various informations to page 
	 * @return underwriter's complete policy details page.
	 */
	@RequestMapping("/underwriter/allPolyDet")
	public String underwriterPolyDet(@RequestParam("insid") String but,Model model) 
	{
		Long pid =Long.parseLong(but);
		PolicyDetail pod=polyrepo.getById(pid);
		UserProfile user=userrepo.getById(pod.getUser().getUserId());
		model.addAttribute("pod", pod);
		model.addAttribute("user", user); 
		model.addAttribute("ins",insplan.getById(pod.getInsurance().getInsuranceId()));
		/*Adding code for seperate medical history for all policies*/
		
		String medicalStr = pod.getMedicalHistory();
		
		MedicalHistory policyMedicalHist = new MedicalHistory();
		
			/*Start Code for heart*/
		if(medicalStr.charAt(0) == '0')
		{
			policyMedicalHist.setHeartAilments("No");
		}
		else if(medicalStr.charAt(0) == '2')
		{
			policyMedicalHist.setHeartAilments("Low");
		}
		else if(medicalStr.charAt(0) == '3')
		{
			policyMedicalHist.setHeartAilments("Moderate");
		}
		else if(medicalStr.charAt(0) == '4')
		{
			policyMedicalHist.setHeartAilments("Severe");
		}
			/*End of Code for heart*/
			/*Start of Code for Diabetes*/
		if(medicalStr.charAt(1) == '0')
		{
			policyMedicalHist.setDiabetes("No");
		}
		else if(medicalStr.charAt(1) == '2')
		{
			policyMedicalHist.setDiabetes("Low");
		}
		else if(medicalStr.charAt(1) == '3')
		{
			policyMedicalHist.setDiabetes("Moderate");
		}
		else if(medicalStr.charAt(1) == '4')
		{
			policyMedicalHist.setDiabetes("Severe");
		}
			/*End of Code for Diabetes*/
		
			/*Start of code for tooth decay*/
		if(medicalStr.charAt(2) == '0')
		{
			policyMedicalHist.setToothDecay("No");
		}
		else if(medicalStr.charAt(2) == '1')
		{
			policyMedicalHist.setToothDecay("Yes");
		}
			/*End of code for tooth decay*/ 
			/*Start of code for covid*/
		if(medicalStr.charAt(3) == '0')
		{
			policyMedicalHist.setCovid("No");
		}
		else if(medicalStr.charAt(3) == '1')
		{
			policyMedicalHist.setCovid("Yes");
		}
			/*End of code for Covid*/
			/*Start of code for cancer*/
		if(medicalStr.charAt(4) == '0')
		{
			policyMedicalHist.setCancer("No");
		}
		else if(medicalStr.charAt(4) == '2')
		{
			policyMedicalHist.setCancer("Low");
		}
		else if(medicalStr.charAt(4) == '3')
		{
			policyMedicalHist.setCancer("Moderate");
		}
		else if(medicalStr.charAt(4) == '4')
		{
			policyMedicalHist.setCancer("Severe");
		}
			/*Start of Code for Cataract*/
		if(medicalStr.charAt(5) == '0')
		{
			policyMedicalHist.setCataract("No");
		}
		else if(medicalStr.charAt(5) == '1')
		{
			policyMedicalHist.setCataract("Yes");
		}
			/*End of code for cataract*/
		
		
		
		
		
		model.addAttribute("medhis", policyMedicalHist);
		
		
		List<GroupDetails> grp=grpdet.findgroupByPolicy(pid);
	ArrayList<Long> arr=new ArrayList<>();
		for(GroupDetails g:grp)
		{
			arr.add(g.getId().getDepdet().getDepId());
		}
		
		model.addAttribute("arr",arr);
		model.addAttribute("grp", grp);
		model.addAttribute("deprepo", deprepo);
		model.addAttribute("ben", benRepo.findBeniByPolyId(pod.getPolicyId()));
		
		return "underwriterMoreDet";
	}
	
	
	/** 
	 * This method gives manually approved/rejected policies information
	 * @param m for sending all manually approved/rejected policies
	 * @return underwriter's manually updated policy page
	 */
	@RequestMapping("/underwriter/manualrequest")
	public String underwritermanual(Model m) 
	{
		m.addAttribute("m",polyrepo.findManualStatusPolicies());
		return "UnderwriterManualPage";
	}
	
}
