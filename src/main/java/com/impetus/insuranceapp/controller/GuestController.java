package com.impetus.insuranceapp.controller;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.impetus.insuranceapp.repository.InsurRepo;
import com.impetus.insuranceapp.service.InsurServ;

@Controller
public class GuestController {

	
	InsurServ insureServ;
	InsurRepo insRepo;
	
	/**
	 * This method is there for contructing a group
	 * @param insureServ for setting insurance service 
	 * @param insRepo for setting insurance repository
	 */
	
	public GuestController(InsurServ insureServ,InsurRepo insRepo) {
		
		this.insRepo = insRepo;
		this.insureServ = insureServ;
	}

	
	 
	/**
	 * This Method is there to display life insurance plans
	 * @param model for sending Life insurance plans 
	 * @return guest insurance plans page.
	 */
	@RequestMapping(value = "/lifeplans", method = RequestMethod.GET)
	public String listLifePlans(Model model) {
		
		model.addAttribute("insplans",insureServ.findAllByType("Life")); 
		return "guestInsPlans";
	}
	
	/**
	 * This Method is there to display dental insurance plans
	 * @param model for sending Dental insurance plans
	 * @return guest insurance plans page.
	 */
	@RequestMapping(value = "/dentalplans", method = RequestMethod.GET)
	public String listVisionPlans(Model model) {
		
		
		
		model.addAttribute("insplans",insureServ.findAllByType("Dental"));
		return "guestInsPlans";
	}
	
	/** 
	 * This Method is there to display Vision insurance plans
	 * @param model for sending Vision insurance plans
	 * @return guest insurance plans page.
	 */
	@RequestMapping(value = "/visionplans", method = RequestMethod.GET)
	public String listDentalPlans(Model model) {
		
		model.addAttribute("insplans",insureServ.findAllByType("Vision"));
		return "guestInsPlans";
	}
	
	
	
}
