
package com.impetus.insuranceapp.controller;

import java.io.IOException;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.impetus.insuranceapp.model.BeneficiaryDetails;
import com.impetus.insuranceapp.model.DepDetails;
import com.impetus.insuranceapp.model.GroupDetKey;
import com.impetus.insuranceapp.model.GroupDetails;
import com.impetus.insuranceapp.model.MedicalHistory;
import com.impetus.insuranceapp.model.PolicyDetail;
import com.impetus.insuranceapp.model.UserProfile;
import com.impetus.insuranceapp.repository.BeneficiaryRepo;
import com.impetus.insuranceapp.repository.DepRepo;
import com.impetus.insuranceapp.repository.GroupRepo;
import com.impetus.insuranceapp.repository.InsurRepo;
import com.impetus.insuranceapp.repository.MedicalRepo;
import com.impetus.insuranceapp.repository.PolicyRepo;
import com.impetus.insuranceapp.repository.UserProfileRepo;
import com.impetus.insuranceapp.service.InsurServ;
import com.impetus.insuranceapp.service.UserProfileService;
import com.impetus.insuranceapp.web.dto.UserProfileDto;

@Controller
public class UserController {
	private UserProfileRepo userProfileRepo;
	private DepRepo depRepo;
	private MedicalRepo medRepo;
	private UserProfileService userProfileService;
	private PolicyDetail policyDet = new PolicyDetail();
	private GroupDetails grpDet = new GroupDetails();
	private GroupDetKey grpKey = new GroupDetKey();
	private UserProfile user;
	private String emailId;
	private String age;
	InsurServ insureServ;
	InsurRepo insRepo;
	PolicyRepo policyRepo;
	BeneficiaryRepo benRepo;
	GroupRepo grpRepo;
	
	
	
	
	
	

	
	
	
	@Value("${value.getlife}")
	private String val;
	
	
	
	
	
	

	public UserController(UserProfileRepo userProfileRepo, UserProfileService userProfileService,DepRepo depRepo,MedicalRepo medRepo,InsurServ insureServ,InsurRepo insRepo,PolicyRepo policyRepo,BeneficiaryRepo benRepo,GroupRepo grpRepo) {
		super();
		this.userProfileRepo = userProfileRepo;
		this.depRepo = depRepo;
		this.medRepo = medRepo;
		this.userProfileService = userProfileService;
		this.insRepo = insRepo;
		this.insureServ = insureServ;
		this.policyRepo = policyRepo;
		this.benRepo = benRepo;
		this.grpRepo = grpRepo;
//		this.policyDet = policyDet; 
	}
	
	
	/**
	 * This Method returns the guest page
	 * @return guest page
	 */
	/*Guest Page */
	@RequestMapping("/guest")
	public String userGuestPage() {
		
		return "guestanmol";
	}
	
	/**
	 * This page redirects to sign up page
	 * @return Sign Up Page
	 */
	/* Sign Up Page */
	@RequestMapping("/signup")
	public String signupRedirect() {
		return "signup";
	}

	/** 
	 * This method checks all server side validation which are there in sign up and if all ok then saves info and register user
	 * @param userProfileDto for setting all info of user
	 * @param dob for recording user's dob
	 * @param bindingResult for checking for error(Server Side Validations)
	 * @param m for giving different kind of messages 
	 * @return login page on successfull registration otherwise sign up page
	 */
	/* Home Page After Sign Up */
	@RequestMapping(value = "/home",method = RequestMethod.POST)
	public String userHomePage(@Valid @ModelAttribute("guestinvalid") UserProfileDto userProfileDto,@RequestParam("dob") String dob,BindingResult bindingResult,Model m)
	{
		
		emailId = userProfileDto.getEmail();
		

			
		if(bindingResult.hasErrors())
		{
			return "signup";
		}
		else if(userProfileRepo.getUserByEmail(emailId) != null)
		{
			m.addAttribute("emailexist", "Email Already Exist !!");
			return "signup";
		}
	
		else
		{	
//		mm/dd/yyyy
		Integer yob = Integer.parseInt(dob.substring(0,4));
		userProfileDto.setYob(yob);
		userProfileService.save(userProfileDto);
		m.addAttribute("successmsg", "Registered Successfully Please Log In !");
		return "login";
		}
	}

	
	/**
	 * This method leads us to log in page
	 * @return login page
	 */
	/*Login Page*/
	@RequestMapping("/login")
	public String loginRedirect() {
		return "login";
	}

	/** 
	 * This page checks for valid email and password and if user blocked and type of user(ADMIN,USER,UWRITER) and appropriately redirects to respective pages.
	 * @param email for getting from email field of form
	 * @param password for getting from password field of form
	 * @param model for displaying messages and sending objects to jsp page 
	 * @param session for setting the name for all page 
	 * @return different page according to type of user and login page if errors are there 
	 */
	/* ReDirection After Login Page */
	@RequestMapping(value = "/homelogin", method = RequestMethod.POST)
	public String userHomeViaLogin(@RequestParam("email") String email, @RequestParam("password") String password,Model model,HttpSession session)
	{
		
		
		
		
		if (userProfileRepo.getUserByEmail(email) == null ) 
		{
			model.addAttribute("message", "Invalid Credentials !!");//user not exists.
			return "login";
		} 
		
		else if(userProfileRepo.getUserByEmail(email).isActive()==false)
		{
			model.addAttribute("message", "User is not active !");//user not exists.
			return "login";
		}
		
		else 
		{
			
			if (userProfileRepo.getUserByEmail(email).getPassword().equals(password) && userProfileRepo.getUserByEmail(email).getUserType().equalsIgnoreCase("ADMIN") ) 
			{
				session.setAttribute("login",userProfileRepo.getUserByEmail(email));
				
				return "redirect:admin/homepage";
			}
			
			if (userProfileRepo.getUserByEmail(email).getPassword().equals(password) && userProfileRepo.getUserByEmail(email).getUserType().equalsIgnoreCase("UWRITER") )
			{
				session.setAttribute("login",userProfileRepo.getUserByEmail(email));
				
				return "redirect:underwriter/homepage";
			}
			
			
			
			
			
			if (userProfileRepo.getUserByEmail(email).getPassword().equals(password)) 
			{
				user=userProfileRepo.getUserByEmail(email);
				emailId = email;
				//model.addAttribute("yourname",user.getFirstName());
				session.setAttribute("yourname",user.getFirstName() );
				
				session.setAttribute("login",userProfileRepo.getUserByEmail(email));
				
				return "redirect:user/homered";
			}
			else 
			{
				model.addAttribute("Passmessage", "Invalid Credentials !!");
				return "login";
			}
		}
	}
	
	/** 
	 * This method ends session and log out the user,Restrict to get user back to previous page
	 * @param rq for Request of http
	 * @param rs for Response of http
	 * @throws IOException for throwing IOEXCEPTIONS
	 * @throws ServletException for throwing SERVLETEXCEPTIONS
	 */
	/*Logout Controller*/
	@GetMapping("/logout")
	public void logout(HttpServletRequest rq, HttpServletResponse rs) throws IOException, ServletException {
	try
	{
		HttpSession session = rq.getSession(false);
		session.removeAttribute("login");
		session.removeAttribute("yourname");
		session.setAttribute("login", null);
		session.invalidate();
		rs.sendRedirect("/login");
	}
	catch (Exception exp) 
	{
		RequestDispatcher dd = rq.getRequestDispatcher("/guest");
		dd.forward(rq, rs);
	}



	}
	
	/**
	 * This method redirects us to home page of user
	 * @param m for getting name of user 
	 * @return Home Page of user 
	 */
	/*Nav Bar Home Page Button linked to */
	@RequestMapping("/user/homered")
	public String homelogin(Model m)  
	{ 
		m.addAttribute("yourname",user.getFirstName());
		return "home_page";
	}
	
	/** 
	 * This method leads us to my family page of user
	 * @param model gives all present dependent for respective user
	 * @return myfamily page of User
	 */
	/* My Family Page */
	@RequestMapping(value="/user/myfamily")
	public String myFamilyPage(Model model) 
	{
		model.addAttribute("dep",depRepo.findCustom(user.getUserId())); 
		return "myfamily";
	}
	
	/** 
	 * This method leads us to add member details taking page
	 * @return user add member page
	 */
	/* Add member in family page*/
	@RequestMapping("/user/useraddmember")
	public String AddfamMember() 
	{
		return "UserAddMember";
	}
	
	/**
	 * This method saves detail of member user entered and redirects back to my family page 
	 * @param depDetails gives us complete details of dependent
	 * @param depDob takes dependents DOB
	 * @param med takes dependent medical history 
	 * @param m gives message
	 * @return redirection to my family page of user
	 */
	/*Add member functionality & redirect back to my family*/
	@RequestMapping(value="/user/membersAfterAdd",method = RequestMethod.POST)
	public String saveMember(@ModelAttribute DepDetails depDetails,@RequestParam("depdob") String depDob,@ModelAttribute MedicalHistory med,Model m) 
	{
		depDetails.setUserProfile(user);
		Integer depYob = Integer.parseInt(depDob.substring(0, 4));
		depDetails.setDepyob(depYob);
		depRepo.save(depDetails);
		med.setDepDetail(depDetails);
		medRepo.save(med);
		m.addAttribute("dep",depRepo.findCustom(user.getUserId()));
		return "myfamily";
//		return mv;
	}
	
	
	
	
	
	/** 
	 * This method displays the myprofile page of user where the fields are not editable
	 * @return my profile page of user 
	 */
	/* My profile page */
	@RequestMapping(value = "/user/myprofile")
	public ModelAndView myProfilePage() 
	{
		ModelAndView mv = new ModelAndView();

		
		mv.addObject("ronly", "readonly");
		mv.addObject("userprofile", userProfileRepo.getUserByEmail(emailId));
		mv.setViewName("myprofile");
		mv.addObject("yourname",user.getFirstName());
		return mv;
	}
	
	
	/** 
	 * This method leads to editing fields of my profile
	 * @return my profile Editable page of user
	 */
	/* My profile page */
	@RequestMapping(value = "/user/myprofileEdit")
	public ModelAndView myProfilePageEdit() 
	{
		ModelAndView mv = new ModelAndView();
		mv.addObject("ronly", "readonly");
		mv.addObject("userprofile", userProfileRepo.getUserByEmail(emailId));
		mv.addObject("yourname",user.getFirstName());
		mv.setViewName("myprofileEdit");
		return mv;
	}
	
	
	/**
	 * This method redirect back to non editable page of my profile 
	 * @param firstName new firstname of user
	 * @param lastName new lastname of user 
	 * @param mobileNo new mobile no of user 
	 * @param address new address of user
	 * @param annualIncome new annual income of user
	 * @param model for displaying message
	 * @param redirectAttrs for setting attr in redirection
	 * @param session for setting some attributes on messages
	 * @return redirects to my profile page of user
	 */
	/* Profile Update */
	@RequestMapping(value = "/user/myprofileupdate", method = RequestMethod.POST)
	public String updateProfile(@RequestParam("firstName") String firstName, @RequestParam("lastName") String lastName,
			@RequestParam("mobileNo") Long mobileNo, @RequestParam("address") String address,
			@RequestParam("annualIncome") String annualIncome ,Model model,RedirectAttributes redirectAttrs, HttpSession session) 
{
Integer val = userProfileRepo.updateUser(firstName, lastName, mobileNo, address, annualIncome, emailId);

if (val != null)
{
} 
else 
{
}
//userProfileService.save(userProfileDto);
user=userProfileRepo.getUserByEmail(emailId);
//ModelAndView mv=new ModelAndView();
//model.addAttribute("ronly","readonly");
model.addAttribute("yourname",user.getFirstName());
session.removeAttribute("youname");
session.setAttribute("yourname",user.getFirstName() );


//model.addAttribute("alertmsg","Details updated successfully");
//redirectAttrs.addFlashAttribute("alertmsg","Details updated successfully");
//redirectAttrs.addAttribute("alertmsg","Details updated successfully");
//model.addAttribute("alertmsg","Details updated successfully");
session.setAttribute("alertmsg","Details updated successfully");


return "redirect:/user/myprofile";

}
	
	

	

	@RequestMapping("/user/getpriceorgrp")
	public String priceOrGroup(@RequestParam("PolicyType") String insbtn,Model m) 
	{
		
//		********minimum and maximum age check  ******* 
		
		
		
		if(insbtn.equalsIgnoreCase("Individual"))
		{
			
			if((LocalDate.now().getYear()-user.getYob()) >= insRepo.getById(insuranceId).getCoverageAge() ||  (LocalDate.now().getYear()-user.getYob())<insRepo.getById(insuranceId).getMinAge())
			{
				m.addAttribute("ins",insRepo.getById(insuranceId));
				m.addAttribute("notvalidmsg","This policy is not valid as Your age is more than maximum cover age of plan or less than minimum age required.");
				return "InsPlansDet";
			}
			
			 
			
			
			int myear=insRepo.getById(insuranceId).getCoverageAge()-(LocalDate.now().getYear()-user.getYob());
			m.addAttribute("udet",myear);
			m.addAttribute("usergender",user.getGender());
			m.addAttribute("userpro", user);
			return "details";
		}
		else 
		{
			
			if((LocalDate.now().getYear()-user.getYob()) >= insRepo.getById(insuranceId).getCoverageAge() ||  (LocalDate.now().getYear()-user.getYob())<insRepo.getById(insuranceId).getMinAge())
			{
				m.addAttribute("ins",insRepo.getById(insuranceId));
				m.addAttribute("notvalidmsg","This policy is not valid as Your age is more than maximum cover age of plan or less than minimum age required.");
				return "InsPlansDet";
			}
			
			
			
			int myear=insRepo.getById(insuranceId).getCoverageAge()-(LocalDate.now().getYear()-user.getYob());
			m.addAttribute("udet",myear);
			m.addAttribute("usergender",user.getGender());
			m.addAttribute("userpro", user);
			m.addAttribute("dependents",depRepo.findCustom(user.getUserId()));
			return "getgroup";
		}
	}
	
	
	/* Insurance Plan Page */
	@RequestMapping(value = "/user/plans", method = RequestMethod.GET)
	public String listAction(Model model) {
		model.addAttribute("insplans",insureServ.findAll());
		return "insplans";
	}
	
	
	@RequestMapping(value="/user/getdetails")
	public String getInsDetails()
	{
		return "details";
	}
	
	Long insuranceId;
	@RequestMapping(value="/user/insplansdet")
	public String getInsPlanDetails(@RequestParam("insid") String idn,Model m) 
	{
		Long id = Long.parseLong(idn);
		insuranceId = id;
		m.addAttribute("ins",insRepo.getById(id));
		return "InsPlansDet";
	}
	
	Long price = (long) 0;
	Integer dep;
	BeneficiaryDetails bendetails;
	ArrayList<Long> depIncluded=null;
	Integer yearsPolicyActive = 1;
	
	@RequestMapping("/user/getcustompricegroup")
	public String customPrice(@RequestParam(value="included", required=false) ArrayList<Long> inc ,@ModelAttribute BeneficiaryDetails bendet,@RequestParam("years") Integer years,Model m,HttpSession session) 
	{
		
		if(inc == null)
		{
			session.setAttribute("adddepmsg", "Please select dependents!!");
			m.addAttribute("dependents",depRepo.findCustom(user.getUserId()));
			int myear=insRepo.getById(insuranceId).getCoverageAge()-(LocalDate.now().getYear()-user.getYob());
			m.addAttribute("udet",myear);
			m.addAttribute("userpro", user);
			m.addAttribute("usergender",user.getGender());
			dep = 0;
			return "getgroup";
		}
		else
		{
			depIncluded = inc;
			bendetails = bendet;
			dep = inc.size();
		}
		
		price = insRepo.getById(insuranceId).getBasePrice();
//		price = price+((price*dep*10)/100);
		Integer age = insRepo.getById(insuranceId).getMinAge();
		Integer userAge = 2021-user.getYob();
		
		age = userAge-age; 
		price = price+((price*(dep*10+age))/100);
		yearsPolicyActive = years;
		m.addAttribute("deprep",depRepo);
		m.addAttribute("user",user);
		m.addAttribute("dep",inc.size());
		m.addAttribute("dependents", inc);
		m.addAttribute("cusprice", price);
		m.addAttribute("mem", inc.size());
		m.addAttribute("years",years);
		m.addAttribute("ins", insRepo.getById(insuranceId));
		m.addAttribute("ben",bendet);
		
		
		List<MedicalHistory> medical = medRepo.findByUserCustom(user.getUserId());
		if(medical.size() == 0)
		{
			m.addAttribute("updatemedmessage","Please update your medical history!");
			return "usermedhist";
		}
		
		
		return "customprice";
	}
	
	/* Update User Medical History & Capture it in Coded String Format */
	String policymed = "";
	@RequestMapping("/user/updatemedhist") 
	public String viewUpdateMedHist(Model m) 
	{
		List<MedicalHistory> med=medRepo.findByUserCustom(user.getUserId());
//		m.addAttribute("message", "Updated Successfully...");
//		if(med !=null)
		MedicalHistory me=null;
		if(med.size()>0)
		{
		me=med.get(0);
		}
		m.addAttribute("medi", me);
		return "usermedhist";
	}
	
	MedicalHistory med;
	/* Updation of medical history in profile section*/
	/* Updation of medical history in profile section*/
	@RequestMapping("/user/usermedicalupdate")
	public String updateMedHist(@ModelAttribute MedicalHistory med,Model m,HttpSession session)
	{
	med.setUserProfile(user);

	ArrayList<MedicalHistory> l = (ArrayList<MedicalHistory>)medRepo.findByUserCustom(user.getUserId());

	if(l.isEmpty())
	{
	medRepo.save(med);
	}
	else
	{
	medRepo.updateMedical(med.getHeartAilments(), med.getDiabetes(), med.getCataract(), med.getToothDecay(), med.getCovid(),med.getCancer(), user.getUserId());
	}

	


//m.addAttribute("med",med);
	session.setAttribute("message","Medical History Updated Successfully");
	m.addAttribute("message","Medical History Updated Successfully");
	m.addAttribute("yourname",user.getFirstName());
	
	return "redirect:/user/updatemedhist";
	}
//	@RequestMapping("/usermedicalupdate")
//	public String updateMedHist(@ModelAttribute MedicalHistory medical,Model m) 
//	{
//		med.setUserProfile(user);		
//		
//		medRepo.save(med);
////		ArrayList<MedicalHistory> l = (ArrayList<MedicalHistory>)medRepo.findByUserCustom(user.getUserId());
//		
////		if(l == null || l.isEmpty()) 
////		{
////			medRepo.save(med);
////		}
////		else
////		{
////			medRepo.updateMedical(med.getHeartAilments(), med.getDiabetes(), med.getCataract(), med.getToothDecay(), med.getCovid(),med.getCancer(), user.getUserId());
////		}
////		m.addAttribute("message","Medical History Updated Successfully");
//		return "home_page";
//	}
//	

	/*Get Custom Price For Individual Taking Beneficiary Information */
	BeneficiaryDetails indbenDetails = new BeneficiaryDetails();
	@RequestMapping("/user/getcustompriceind")
	public String getCustomPriceInd(@ModelAttribute("ben") BeneficiaryDetails benDet,@RequestParam("years") Integer years,Model m) 
	{
		
		//***** Insurance validation check ********//
		
		if((LocalDate.now().getYear()-user.getYob())+years >= insRepo.getById(insuranceId).getCoverageAge() )
		{
			m.addAttribute("notvalidmsg","Tenure entered for this policy is not valid since it exceeds max age.");
			m.addAttribute("usergender",user.getGender());
			return "details";
		}
		
		 
		
		price = insRepo.getById(insuranceId).getBasePrice();
		dep = 0;
		Integer age = insRepo.getById(insuranceId).getMinAge();
		Integer userAge = 2021-user.getYob();
		indbenDetails = benDet;
		age = userAge-age; 
		price = price+((price*(dep*10+age))/100);
		yearsPolicyActive = years;
		m.addAttribute("user", user);
		m.addAttribute("ins", insRepo.getById(insuranceId));
		m.addAttribute("ben", indbenDetails);
		m.addAttribute("dependents",depIncluded);
		m.addAttribute("cusprice", price); 
		m.addAttribute("mem", 0);
		m.addAttribute("years",years);
		
		List<MedicalHistory> medical = medRepo.findByUserCustom(user.getUserId());
		if(medical.size() == 0)
		{
			m.addAttribute("updatemedmessage","Please update your medical history!");
			return "usermedhist";
		}
		return "customprice";
	}
	
	@RequestMapping(value = "/user/myPolicies", method = RequestMethod.GET)
	public String myPolicies(Model model) {
		model.addAttribute("m",policyRepo.findPoliciesByUserCustom(user.getUserId()));
		return "userMyPolicy";
	}
	
	/* Buy Now Button */
	@RequestMapping(value = "/user/buy")
	public String buy(Model m) 
	{
		
		policyDet = new PolicyDetail();
		List<MedicalHistory> medical = medRepo.findByUserCustom(user.getUserId());
		if(medical.size() == 0)
		{
			m.addAttribute("updatemedmessage","Please update your medical history!");
			return "usermedhist";
		}
		else 
		{
//			m.addAttribute("message","");
			
			MedicalHistory med = medical.get(0);
			
			
			/*Start of code for medical string*/
				/*Code for heart*/
			policymed = "";
			if((med.getHeartAilments()).equalsIgnoreCase("no"))
			{
				policymed += "0";
			}
			else if((med.getHeartAilments()).equalsIgnoreCase("low"))
			{
				policymed += "2";
			}
			else if((med.getHeartAilments()).equalsIgnoreCase("moderate"))
			{
				policymed += "3";
			}
			else if((med.getHeartAilments()).equalsIgnoreCase("severe"))
			{
				policymed += "4";
			}
				/*End of Code for heart*/
				/*Start of Code for Diabetes*/
			if((med.getDiabetes()).equalsIgnoreCase("no"))
			{
				policymed += "0";
			}
			else if((med.getDiabetes()).equalsIgnoreCase("low"))
			{
				policymed += "2";
			}
			else if((med.getDiabetes()).equalsIgnoreCase("moderate"))
			{
				policymed += "3";
			}
			else if((med.getDiabetes()).equalsIgnoreCase("severe"))
			{
				policymed += "4";
			}
				/*End of Code for Diabetes*/
			
				/*Start of code for tooth decay*/
			if((med.getToothDecay()).equalsIgnoreCase("no"))
			{
				policymed += "0";
			}
			else if((med.getToothDecay()).equalsIgnoreCase("yes"))
			{
				policymed += "1";
			}
				/*End of code for tooth decay*/ 
				/*Start of code for covid*/
			if(((med.getCovid()).equalsIgnoreCase("no")))
			{
				policymed += "0";
			}
			else if((med.getCovid()).equalsIgnoreCase("yes"))
			{
				policymed += "1";
			}
				/*End of code for Covid*/
				/*Start of code for cancer*/
			if((med.getCancer()).equalsIgnoreCase("no"))
			{
				policymed += "0";
			}
			else if((med.getCancer()).equalsIgnoreCase("low"))
			{
				policymed += "2";
			}
			else if((med.getCancer()).equalsIgnoreCase("moderate"))
			{
				policymed += "3";
			}
			else if((med.getCancer()).equalsIgnoreCase("severe"))
			{
				policymed += "4";
			}
				/*Start of Code for Cataract*/
			if((med.getCataract()).equalsIgnoreCase("no"))
			{
				policymed += "0";
			}
			else if((med.getCataract()).equalsIgnoreCase("yes"))
			{
				policymed += "1";
			}
				/*End of code for cataract*/
			
			
			
	//		#Heart,Dia,Cat,Tooth,Covid,Cancer
			
			//Rules -> 
			int medicalHist = Integer.parseInt(policymed);
			int temp = medicalHist;
			
			int cataract = temp%10;
			temp = temp/10;
			
			int cancer = temp%10;
			temp = temp/10; 
			
			int covid = temp%10;
			temp = temp/10;
			
			int toothDecay = temp%10;
			temp = temp/10;
			
			int diabetes = temp%10;
			temp = temp/10;
			
			int heartAilments = temp%10;
			
			
//			Integer medValues = Integer.parseInt(val);
//			ArrayList<Integer> medicals = ArrayList<Integer>();
//			
//			while(medValues)
//			{
//				medical.add(medValues%10);
//				medValues = 
//			}
			String s = "anmol";
			s.charAt(0);
			
//			Integer i = Integer.parseInt(String.valueOf(val.charAt(0)));
			//#H D Can T Cat Cov (Val format)
			int givHeart = Character.getNumericValue(val.charAt(0));
			
			int givDiabetes = Character.getNumericValue(val.charAt(1));
			
			int givCancer = Character.getNumericValue(val.charAt(2));

			int givTooth = Character.getNumericValue(val.charAt(3));

			int givCataract = Character.getNumericValue(val.charAt(4));

			int givCovid = Character.getNumericValue(val.charAt(5));
			
			
			String insType = (insRepo.getById(insuranceId)).getInsuranceType();
			
			if((heartAilments >= givHeart || diabetes >= givDiabetes || cancer >= givCancer) || (insType.equalsIgnoreCase("Dental") && toothDecay == givTooth) || (insType.equalsIgnoreCase("Vision") && cataract==givCataract))
			{
				//Manual Approval
				policyDet.setPolicyStatus("Pending");
				policyDet.setApproval("Manual");
			}
			else
			{
				//Auto Approval
				policyDet.setPolicyStatus("Approved");
				policyDet.setApproval("Auto");
			}
			
//          Rules Defined Previously		
//			if(policymed.equals(val))
//			{
//				//Manual Approval
//				policyDet.setPolicyStatus("Pending");
//				policyDet.setApproval("Manual");
//			}
//			else
//			{	
//				//Auto Approval
//				policyDet.setPolicyStatus("Approved");
//				policyDet.setApproval("Auto");
//			}
			
			policyDet.setUser(user);
			policyDet.setInsurance(insRepo.getById(insuranceId));
			policyDet.setMedicalHistory(policymed);
			policyDet.setDate(LocalDate.now().toString());
			policyDet.setPolicyCost(Integer.parseInt(price.toString()));
			
			if(depIncluded != null)
			{
				if(depIncluded.size() > 0)
				{
					policyDet.setIsGroupPresent(true);
				}
				else
				{
					policyDet.setIsGroupPresent(false);
				}
			}
			String endDate = LocalDate.now().plusYears(yearsPolicyActive).toString();
			policyDet.setPolicyEndDate(endDate);
			PolicyDetail pobj=policyRepo.save(policyDet);
			m.addAttribute("polystatus", pobj.getPolicyStatus());
			
			
			if(depIncluded != null ) 
			{	
				bendetails.setPolicyDet(policyDet);
				benRepo.save(bendetails);
			}
			else
			{
				indbenDetails.setPolicyDet(policyDet);
				benRepo.save(indbenDetails);
			}
			
			if(depIncluded != null)
			{
				if(depIncluded.size() != 0) 
				{	
					
					for(Long i:depIncluded)
					{
						grpKey.setPolicydet(policyDet);
						grpKey.setDepdet(depRepo.getById(i));
						grpDet.setId(grpKey);
						grpRepo.save(grpDet);	
					}	
					
				}
			}
			
			depIncluded = null;
			bendetails = null;
			indbenDetails = null;
			
			
			
			return "SuccessPage";
		}
	}
	
	Long dependentId;
	@RequestMapping(value="/user/depUpdateDetail")
	public String depUpdateDetail(@RequestParam("button") Long depid,Model m) 
	{
  		dependentId = depid;
		
		MedicalHistory med=medRepo.findByDepCustom(depid);
//		m.addAttribute("message", "Updated Successfully...");
//		if(med !=null)
		//MedicalHistory me=null;
//		if(med.size()>0)
//		{
//		me=med.get(0);
//		}
		m.addAttribute("medi", med);
		
		
		return "depupdatedetail";
	}
	
	@RequestMapping(value="/user/depmedicalupdate")
	public String depMedicalUpdate(@ModelAttribute("medical") MedicalHistory med,Model m,HttpSession session)
	{
		//med.setUserProfile(user);
		//med.setDepDetail(depRepo.getById(dependentId));
		//medRepo.save(med);
		medRepo.updateDep(med.getHeartAilments(), med.getDiabetes(), med.getCataract(), med.getToothDecay(), med.getCovid(),med.getCancer(), dependentId);
		session.setAttribute("updatedepmsg", depRepo.getById(dependentId).getDepFullName());
		
		return "redirect:/user/myfamily";
	}
	
	@RequestMapping(value="/user/depDeleteDetail")
	public String depDeleteDetail(@RequestParam("button") String depidn,HttpSession session)
	{
		Long depid = Long.parseLong(depidn);
		if(grpRepo.findPolicyByDept(depid).size()!=0)
		{
			String s=depRepo.getById(depid).getDepFullName()+" cannot be deleted because dependent already associated with a policy";
			session.setAttribute("deletedepmsg", s);
			return "redirect:/user/myfamily";
			 
		}
		else {
		MedicalHistory m = medRepo.findByDepCustom(depid);
		String s=depRepo.getById(depid).getDepFullName()+" deleted";
		session.setAttribute("deletedepmsg", s);
		medRepo.deleteById(m.getMedId());
		depRepo.deleteById(depid);	
		

		return "redirect:/user/myfamily";
		}
	}  
	
	
	
	
	@RequestMapping("/user/allPolyDet")
	public String underwriterPolyDet(@RequestParam("insid") String but,Model model) 
	{
		Long pid =Long.parseLong(but);
		PolicyDetail pod=policyRepo.getById(pid);
		UserProfile user=userProfileRepo.getById(pod.getUser().getUserId());
		model.addAttribute("pod", pod);
		model.addAttribute("user", user);
		model.addAttribute("ins",insRepo.getById(pod.getInsurance().getInsuranceId()));
		
		/*Adding code for seperate medical history for all policies*/
		
		String medicalStr = pod.getMedicalHistory();
		
		MedicalHistory policyMedicalHist = new MedicalHistory();
		
			/*Start Code for heart*/
		if(medicalStr.charAt(0) == '0')
		{
			policyMedicalHist.setHeartAilments("No");
		}
		else if(medicalStr.charAt(0) == '2')
		{
			policyMedicalHist.setHeartAilments("Low");
		}
		else if(medicalStr.charAt(0) == '3')
		{
			policyMedicalHist.setHeartAilments("Moderate");
		}
		else if(medicalStr.charAt(0) == '4')
		{
			policyMedicalHist.setHeartAilments("Severe");
		}
			/*End of Code for heart*/
			/*Start of Code for Diabetes*/
		if(medicalStr.charAt(1) == '0')
		{
			policyMedicalHist.setDiabetes("No");
		}
		else if(medicalStr.charAt(1) == '2')
		{
			policyMedicalHist.setDiabetes("Low");
		}
		else if(medicalStr.charAt(1) == '3')
		{
			policyMedicalHist.setDiabetes("Moderate");
		}
		else if(medicalStr.charAt(1) == '4')
		{
			policyMedicalHist.setDiabetes("Severe");
		}
			/*End of Code for Diabetes*/
		
			/*Start of code for tooth decay*/
		if(medicalStr.charAt(2) == '0')
		{
			policyMedicalHist.setToothDecay("No");
		}
		else if(medicalStr.charAt(2) == '1')
		{
			policyMedicalHist.setToothDecay("Yes");
		}
			/*End of code for tooth decay*/ 
			/*Start of code for covid*/
		if(medicalStr.charAt(3) == '0')
		{
			policyMedicalHist.setCovid("No");
		}
		else if(medicalStr.charAt(3) == '1')
		{
			policyMedicalHist.setCovid("Yes");
		}
			/*End of code for Covid*/
			/*Start of code for cancer*/
		if(medicalStr.charAt(4) == '0')
		{
			policyMedicalHist.setCancer("No");
		}
		else if(medicalStr.charAt(4) == '2')
		{
			policyMedicalHist.setCancer("Low");
		}
		else if(medicalStr.charAt(4) == '3')
		{
			policyMedicalHist.setCancer("Moderate");
		}
		else if(medicalStr.charAt(4) == '4')
		{
			policyMedicalHist.setCancer("Severe");
		}
			/*Start of Code for Cataract*/
		if(medicalStr.charAt(5) == '0')
		{
			policyMedicalHist.setCataract("No");
		}
		else if(medicalStr.charAt(5) == '1')
		{
			policyMedicalHist.setCataract("Yes");
		}
			/*End of code for cataract*/
		
		
		
		
		
		model.addAttribute("medhis", policyMedicalHist);
		
		List<GroupDetails> grp=grpRepo.findgroupByPolicy(pid);
	ArrayList<Long> arr=new ArrayList<>();
		for(GroupDetails g:grp)
		{
			arr.add(g.getId().getDepdet().getDepId());
		}
		 
		model.addAttribute("arr",arr);
		model.addAttribute("grp", grp);
		model.addAttribute("deprepo", depRepo);
		model.addAttribute("ben", benRepo.findBeniByPolyId(pod.getPolicyId()));
		return "userPolicyMoreDet";
	}
	
	@RequestMapping("/user/contact")
	public String contactUsPage() 
	{
		return "UserContactUs"; 
	}
	
	
	
}