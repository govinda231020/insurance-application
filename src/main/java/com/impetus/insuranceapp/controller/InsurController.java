package com.impetus.insuranceapp.controller;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import com.impetus.insuranceapp.model.InsurancePlan;
import com.impetus.insuranceapp.repository.InsurRepo;
import com.impetus.insuranceapp.service.InsurServ;

@Controller
public class InsurController 
{
	InsurServ insureServ;
	InsurRepo insRepo;
	
	/**
	 * This method is there for contructing a Insurance Controller
	 * @param insureServ for setting insurance service 
	 * @param insRepo for setting insurance repository
	 */
	
	public InsurController(InsurServ insureServ,InsurRepo insRepo) {
		super();
		this.insRepo = insRepo;
		this.insureServ = insureServ;
	}

	
	
	/**
	 * This Method is there to display life insurance plans
	 * @param model for sending Life insurance plans
	 * @return user's insurance plans page
	 */
	@RequestMapping(value = "/user/lifeplans", method = RequestMethod.GET)
	public String listLifePlans(Model model) {
		
		model.addAttribute("insplans",insureServ.findAllByType("Life"));
		return "insplans";
	} 
	
	/**
	 * This Method is there to display dental insurance plans
	 * @param model for sending Dental insurance plans
	 * @return user's insurance plans page
	 */
	@RequestMapping(value = "/user/dentalplans", method = RequestMethod.GET)
	public String listVisionPlans(Model model) {
		
		
		
		model.addAttribute("insplans",insureServ.findAllByType("Dental"));
		return "insplans";
	}
	
	/**
	 * This Method is there to display vision insurance plans
	 * @param model user's insurance plans page
	 * @return user's insurance plans page
	 */
	@RequestMapping(value = "/user/visionplans", method = RequestMethod.GET)
	public String listDentalPlans(Model model) {
		
		model.addAttribute("insplans",insureServ.findAllByType("Vision"));
		return "insplans";
	}
	
	
//	@RequestMapping(value="/getdetails")
//	public String getInsDetails()
//	{
//		return "details";
//	}

//	@RequestMapping(value = "/plans", method = RequestMethod.GET)
//	public String listAction(Model model) {
//		model.addAttribute("insplans",insureServ.findAll());
//		return "insplans";
//	}
//	

//	@RequestMapping(value = "/plans", method = RequestMethod.GET)
//	public String listAction(Model model) {
//		model.addAttribute("insplans",insureServ.findAll());
//		return "insplans";
//	}
	
	
//
//	@RequestMapping(value = "/plans", method = RequestMethod.GET)
//	public String listAction(Model model) {
//		model.addAttribute("insplans",insureServ.findAll());
//		return "insplans";
//	}
//	
//	@RequestMapping(value="/getdetails")
//	public String getInsDetails()
//	{
//		return "details";
//	}
//	
//
//	@RequestMapping(value="insplansdet")
//	public String getInsPlanDetails(@RequestParam("insid") Long id,Model m) 
//	{
//		m.addAttribute("ins",insRepo.getById(id));
//		return "InsPlansDet";
//	}
//	
		
}




