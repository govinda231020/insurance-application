package com.impetus.insuranceapp.controller;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpSession;
import javax.xml.ws.http.HTTPBinding;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import com.impetus.insuranceapp.model.GroupDetails;
import com.impetus.insuranceapp.model.InsurancePlan;
import com.impetus.insuranceapp.model.MedicalHistory;
import com.impetus.insuranceapp.model.PolicyDetail;
import com.impetus.insuranceapp.model.UserProfile;
import com.impetus.insuranceapp.repository.BeneficiaryRepo;
import com.impetus.insuranceapp.repository.DepRepo;
import com.impetus.insuranceapp.repository.GroupRepo;
import com.impetus.insuranceapp.repository.InsurRepo;
import com.impetus.insuranceapp.repository.MedicalRepo;
import com.impetus.insuranceapp.repository.PolicyRepo;
import com.impetus.insuranceapp.repository.UserProfileRepo;
import com.impetus.insuranceapp.service.InsurServ;

@Controller
public class AdminController {

	InsurRepo insurRepo;
	InsurServ insureserv;
	UserProfileRepo userrepo;
	PolicyRepo polyrepo;

	private GroupRepo grpdet;
	private MedicalRepo med;
	private DepRepo deprepo;
	BeneficiaryRepo benRepo;
	
	
	/**
	 * This Method is for contruct required objects for admin
	 * @param insurRepo 
	 * @param insureserv
	 * @param userrepo
	 * @param polyrepo
	 * @param grpdet
	 * @param med
	 * @param deprepo
	 * @param benRepo
	 */
	public AdminController(InsurRepo insurRepo, InsurServ insureserv, UserProfileRepo userrepo, PolicyRepo polyrepo,
			GroupRepo grpdet, MedicalRepo med, DepRepo deprepo,BeneficiaryRepo benRepo) {
		super();
		this.insurRepo = insurRepo;
		this.insureserv = insureserv;
		this.userrepo = userrepo;
		this.polyrepo = polyrepo;
		this.grpdet = grpdet;
		this.med = med;
		this.deprepo = deprepo;
		this.benRepo=benRepo;
	}


	
	/**
	 * This method is for giving admin's Home Page
	 * @return admin's home page
	 */
	@RequestMapping("/admin/homepage")
	public String userAdminPage() 
	{
		return "adminpage";
	}
	
//	@RequestMapping("/adminhomepage")
//	public String adminHome() 
//	{
//		return "redirect:admin";
//	}
	
	/**
	 * 
	 * @param model takes model as argument for sending it to jsp
	 * @return admin's all user page
	 */
	@RequestMapping("/admin/allUsers")
	public String AdminAllUsersPage(Model model) 
	{
		
		model.addAttribute("users",userrepo.getUserByType("USER"));
		return "adminallUsers";
	}
	
	
	/**
	 * 
	 * @param m takes model as argument for sending it to jsp
	 * @return admin's policy history page
	 */
	@RequestMapping("/admin/Policies")
	public String adminPolicies(Model m) 
	{
		
		m.addAttribute("m",polyrepo.findAll());	
		m.addAttribute("countmap",polyrepo.getCountOfUserByInsurance());
		m.addAttribute("insRepo",insurRepo);
		m.addAttribute("policyRepo",polyrepo);
		return "adminPolicyHistory";
	}
	
	@RequestMapping("/admin/allPolyLinked")
	public String adminAllLinked(@RequestParam("insid")Long insId,Model m) 
	{
		m.addAttribute("m",polyrepo.findPolicyByInsId(insId));	
		m.addAttribute("userRepo",userrepo);
		return "adminAllPolicyLinked";
	}
	
	
	/**
	 * 
	 * @param uid captures user id from click
	 * @return admin's all users page
	 */
	@RequestMapping(value="/admin/EnableUser", method=RequestMethod.POST)
	public String adminEnable(@RequestParam("button") String uid, HttpSession session)
	{
		Long id=Long.parseLong(uid);
		UserProfile user=userrepo.getById(id);
		if(user.isActive())
		{
			return "redirect:/admin/allUsers";
		}
		else
		{
			String emsg= "User with email "+user.getEmail()+" Activated";
			session.setAttribute("enablemsg",emsg );
			userrepo.adminupdateStatus(true, id);
		}
		
		//userrepo.adminupdateStatus(null, null);
		
		return "redirect:/admin/allUsers";
	}
	
	/**
	 * 
	 * @param uid captures user id from click
	 * @return admin's all user page
	 */
	@RequestMapping(value="/admin/DisableUser", method=RequestMethod.POST)
	public String adminDisable(@RequestParam("button") String uid, HttpSession session)
	{
		Long id=Long.parseLong(uid);
		UserProfile user=userrepo.getById(id);
		if(user.isActive())
		{
			String emsg= "User with email "+user.getEmail()+" Deactivated";
			session.setAttribute("disablemsg",emsg );
			userrepo.adminupdateStatus(false, id);
			
		}
		else
		{
			return "redirect:/admin/allUsers";
		}
		
		//userrepo.adminupdateStatus(null, null);
		
		return "redirect:/admin/allUsers";
	}
	
	/** 
	 * 
	 * @param model takes model as argument for sending it to jsp
	 * @return admin's all plans page
	 */
	
	
	//Diactivating a insurance Plan
	
	@RequestMapping(value="/admin/Deactivateplan", method=RequestMethod.POST)
	public String adminDeactivateplan(@RequestParam("button") String insid ,HttpSession session)
	{
		Long id=Long.parseLong(insid);
		InsurancePlan insurance=insurRepo.getById(id);
		if(insurance.isActiveplan())
		{
			String emsg= "Insurance with id  "+insurance.getInsuranceId()+" "+insurance.getInsuranceCompName()+" Deactivated";
			session.setAttribute("disablemsg",emsg );
			insurRepo.adminupdateStatus(false, id);
			
		}
		else
		{
			String emsg= "Insurance with id  "+insurance.getInsuranceId()+" "+insurance.getInsuranceCompName()+" Activated";
			session.setAttribute("enablemsg",emsg );
			insurRepo.adminupdateStatus(true, id);
		}
		
		//userrepo.adminupdateStatus(null, null);
		
		return "redirect:/admin/allplans";
	}
	
	
	
	
	@RequestMapping("/admin/allplans")
	public String AdminAllPlansPage(Model model) 
	{
		ArrayList<InsurancePlan> insplans=(ArrayList<InsurancePlan>) insureserv.findAll();
		model.addAttribute("insplans",insplans);
		return "AdminAllPlans";
	}
	
	/**
	 * 
	 * @return admin's add plan page
	 */
	@RequestMapping("/admin/AddPlan")
	public String AdminAddPlan() 
	{
		return "adminAddPlan";
	}
	
	/**
	 * 
	 * @param insureplan recives insuranceplan's object from previous page
	 * @return admin's all plans page
	 */
	@RequestMapping(value = "/admin/allplansAfter",method = RequestMethod.POST)
	public String allplansAfterAdd(InsurancePlan insureplan,HttpSession session)
	{
		if((insurRepo.getByNameCheck(insureplan.getInsuranceCompName())!=null) && (insurRepo.getByTypeCheck(insureplan.getInsuranceType())!=null))
		{
			session.setAttribute("planpresent","This plan is already present");
			return "redirect:/admin/AddPlan";
		}
		else
		{
			insureserv.save(insureplan);
			return "redirect:/admin/allplans";
		}
	}
	
	/** 
	 * 
	 * @param insuranceCompName for updation
	 * @param insPlanDetail for updation
	 * @param basePrice for updation
	 * @param minAge for updation
	 * @param minAnnualIncome for updation
	 * @param coverageAmount for updation
	 * @param coverageAge for updation
	 * @param Iid for updation
	 * @param session for message
	 * @return redirect to admin's all plans page
	 */
	@RequestMapping(value = "/admin/AfterUpdate",method = RequestMethod.POST)
	public String adminAfterUpdate(@RequestParam("insuranceCompName") String insuranceCompName,
			@RequestParam("insPlanDetail") String insPlanDetail,
			@RequestParam("basePrice") Long basePrice,
			@RequestParam("minAge") int minAge,
			@RequestParam("minAnnualIncome") Long minAnnualIncome,
			@RequestParam("coverageAmount") Long coverageAmount, 
			@RequestParam("coverageAge") int coverageAge , @RequestParam("submitupdate") Long Iid , HttpSession session)
	{
	
		
		Integer i=insurRepo.updateInsurance(insuranceCompName, insPlanDetail, basePrice, minAge, minAnnualIncome, coverageAmount, coverageAge, Iid);
	String s="Insurance with id - "+Iid+" "+ insuranceCompName+" Updated Successfully";
		session.setAttribute("insupdatemsg", s);
			return "redirect:/admin/allplans";
	}
	
	/**
	 * 
	 * @param button for capture Insurance id when button get's clicked
	 * @param model for displaying messages on jsp page
	 * @return admin's all plans page
	 */
	@RequestMapping(value = "/admin/DeletePlan",method = RequestMethod.POST)
	public String plansAfterDelete(@RequestParam("button") String button,Model m)
	{
		Long id=Long.parseLong(button);
		
		
		if(polyrepo.findPolicyByInsId(id).size()!=0)
		{
			String s="Insurance "+insurRepo.getById(id).getInsuranceCompName()+" cannot be deleted because it is already associated with a Policy";
			
			m.addAttribute("insDeletemsg", s);
			m.addAttribute("insplans",insureserv.findAll());
			return "AdminAllPlans";
			
		}
	
		insureserv.delete(id);
		
	return "redirect:/admin/allplans";
	}
	
	/**
	 * 
	 * @param button for capturing Insurance id when button get's clicked
	 * @return admin's update plan page
	 */
	@RequestMapping(value = "/admin/UpdatePlan",method = RequestMethod.POST)
	public ModelAndView plansAfterUpdate(@RequestParam("button") String button )
	{
		Long id=Long.parseLong(button);
		
		ModelAndView mv = new ModelAndView();
		mv.addObject("myobj",insurRepo.getById(id));
		mv.setViewName("adminUpdatePlan");
		return mv;
	}
	
	/**
	 * 
	 * @param but for capturing Policy id when button get's clicked
	 * @param model for sending messages on jsp pages
	 * @return admin's more Policy Details page. 
	 */
	@RequestMapping("/admin/allPolyDet")
	public String underwriterPolyDet(@RequestParam("insid") String but,Model model) 
	{
		Long pid =Long.parseLong(but);
		PolicyDetail pod=polyrepo.getById(pid);
		UserProfile user=userrepo.getById(pod.getUser().getUserId());
		model.addAttribute("pod", pod);
		model.addAttribute("user", user);
		model.addAttribute("ins",insurRepo.getById(pod.getInsurance().getInsuranceId()));
		/*Adding code for seperate medical history for all policies*/
		
		String medicalStr = pod.getMedicalHistory();
		
		MedicalHistory policyMedicalHist = new MedicalHistory();
		
			/*Start Code for heart*/
		if(medicalStr.charAt(0) == '0')
		{
			policyMedicalHist.setHeartAilments("No");
		}
		else if(medicalStr.charAt(0) == '2')
		{
			policyMedicalHist.setHeartAilments("Low");
		}
		else if(medicalStr.charAt(0) == '3')
		{
			policyMedicalHist.setHeartAilments("Moderate");
		}
		else if(medicalStr.charAt(0) == '4')
		{
			policyMedicalHist.setHeartAilments("Severe");
		}
			/*End of Code for heart*/
			/*Start of Code for Diabetes*/
		if(medicalStr.charAt(1) == '0')
		{
			policyMedicalHist.setDiabetes("No");
		}
		else if(medicalStr.charAt(1) == '2')
		{
			policyMedicalHist.setDiabetes("Low");
		}
		else if(medicalStr.charAt(1) == '3')
		{
			policyMedicalHist.setDiabetes("Moderate");
		}
		else if(medicalStr.charAt(1) == '4')
		{
			policyMedicalHist.setDiabetes("Severe");
		}
			/*End of Code for Diabetes*/
		
			/*Start of code for tooth decay*/
		if(medicalStr.charAt(2) == '0')
		{
			policyMedicalHist.setToothDecay("No");
		}
		else if(medicalStr.charAt(2) == '1')
		{
			policyMedicalHist.setToothDecay("Yes");
		}
			/*End of code for tooth decay*/ 
			/*Start of code for covid*/
		if(medicalStr.charAt(3) == '0')
		{
			policyMedicalHist.setCovid("No");
		}
		else if(medicalStr.charAt(3) == '1')
		{
			policyMedicalHist.setCovid("Yes");
		}
			/*End of code for Covid*/
			/*Start of code for cancer*/
		if(medicalStr.charAt(4) == '0')
		{
			policyMedicalHist.setCancer("No");
		}
		else if(medicalStr.charAt(4) == '2')
		{
			policyMedicalHist.setCancer("Low");
		}
		else if(medicalStr.charAt(4) == '3')
		{
			policyMedicalHist.setCancer("Moderate");
		}
		else if(medicalStr.charAt(4) == '4')
		{
			policyMedicalHist.setCancer("Severe");
		}
			/*Start of Code for Cataract*/
		if(medicalStr.charAt(5) == '0')
		{
			policyMedicalHist.setCataract("No");
		}
		else if(medicalStr.charAt(5) == '1')
		{
			policyMedicalHist.setCataract("Yes");
		}
			/*End of code for cataract*/
		
		
		
		
		
		model.addAttribute("medhis", policyMedicalHist);
		 
		List<GroupDetails> grp=grpdet.findgroupByPolicy(pid);
	ArrayList<Long> arr=new ArrayList<>();
		for(GroupDetails g:grp)
		{
			arr.add(g.getId().getDepdet().getDepId());
		}
		
		model.addAttribute("arr",arr);
		model.addAttribute("grp", grp);
		model.addAttribute("deprepo", deprepo);
		model.addAttribute("ben", benRepo.findBeniByPolyId(pod.getPolicyId()));
		return "adminMoreDet";
	}
	
}
	

