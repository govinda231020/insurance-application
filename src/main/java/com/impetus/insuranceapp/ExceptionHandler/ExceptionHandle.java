package com.impetus.insuranceapp.ExceptionHandler;



import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.servlet.ModelAndView;

@ControllerAdvice
	public class ExceptionHandle {

	@ResponseStatus(value=HttpStatus.INTERNAL_SERVER_ERROR)
	@ExceptionHandler(value = NullPointerException.class)
	public ModelAndView nullPointer()
	{
	ModelAndView m=new ModelAndView();
	m.addObject("errorMessage","Something went wrong please login");
	m.setViewName("error");
	return m;
	}

	@ResponseStatus(value=HttpStatus.INTERNAL_SERVER_ERROR)
	@ExceptionHandler(value = NumberFormatException.class)
	public ModelAndView numberFormatException()
	{
	ModelAndView m=new ModelAndView();
	m.addObject("errorMessage","Something went wrong please login");
	m.setViewName("error");
	return m;
	}

	@ResponseStatus(value=HttpStatus.BAD_REQUEST)
	@ExceptionHandler(value = Exception.class)
	public ModelAndView pageNotFoundException()
	{
	ModelAndView m=new ModelAndView();
	m.addObject("errorMessage","Something went wrong please login");
	m.setViewName("error");
	return m;
	}

	}
