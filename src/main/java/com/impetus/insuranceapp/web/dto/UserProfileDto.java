package com.impetus.insuranceapp.web.dto;

import javax.validation.constraints.Digits;
import javax.validation.constraints.Email;
import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;

//import org.hibernate.annotations.BatchSize;
//import javax.*;
//import javax.validation.constraints.*;

public class UserProfileDto {

	@Size(message = "first name must be 3 to 10 characters long", min = 3, max = 15)
	private String firstName;

	@Size(message = "last name must be 3 to 10 characters long", min = 3, max = 15)
	private String lastName;

	@Email(message = "please enter a valid Email!")
	private String email;

	@Size(message = "password must have to be 6 characters long", min = 6)
	private String password;

	@NotNull(message = "Please select a gender")
	private String gender;

	
	private Integer yob;

//	@Size(message="Mobile number should be valid",min=10,max=10)
	private Long mobileNo;
	

	@Size(message = "Please enter a valid address!", min = 3, max = 150)
	private String address;

	private String annualIncome;

	public UserProfileDto(String firstName, String lastName, String email, String password, String gender,Integer yob,
			Long mobileNo, String address, String annualIncome) {

		this.firstName = firstName;
		this.lastName = lastName;
		this.email = email;
		this.password = password;
		this.gender = gender;
		this.yob = yob;
		this.mobileNo = mobileNo;
		this.address = address;
		this.annualIncome = annualIncome;
	}

	public UserProfileDto() {

	}

	/**
	 * @return the firstName
	 */
	public String getFirstName() {
		return firstName;
	}

	/**
	 * @param firstName the firstName to set
	 */
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	/**
	 * @return the lastName
	 */
	public String getLastName() {
		return lastName;
	}

	/**
	 * @param lastName the lastName to set
	 */
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	/**
	 * @return the email
	 */
	public String getEmail() {
		return email;
	}

	/**
	 * @param email the email to set
	 */
	public void setEmail(String email) {
		this.email = email;
	}

	/**
	 * @return the password
	 */
	public String getPassword() {
		return password;
	}

	/**
	 * @param password the password to set
	 */
	public void setPassword(String password) {
		this.password = password;
	}

	/**
	 * @return the gender
	 */
	public String getGender() {
		return gender;
	}

	/**
	 * @param gender the gender to set
	 */
	public void setGender(String gender) {
		this.gender = gender;
	}

	/**
	 * @return the age
	 */
	public Integer getYob() {
		return yob;
	}

	/**
	 * @param yob the age to set
	 */
	public void setYob(Integer yob) {
		this.yob = yob;
	}

	/**
	 * @return the mobileNo
	 */
	public Long getMobileNo() {
		return mobileNo;
	}

	/**
	 * @param mobileNo the mobileNo to set
	 */
	public void setMobileNo(Long mobileNo) {
		this.mobileNo = mobileNo;
	}

	/**
	 * @return the address
	 */
	public String getAddress() {
		return address;
	}

	/**
	 * @param address the address to set
	 */
	public void setAddress(String address) {
		this.address = address;
	}

	/**
	 * @return the annualIncome
	 */
	public String getAnnualIncome() {
		return annualIncome;
	}

	/**
	 * @param annualIncome the annualIncome to set
	 */
	public void setAnnualIncome(String annualIncome) {
		this.annualIncome = annualIncome;
	}

}
